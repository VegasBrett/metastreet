using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class AKeweGameManager : MonoBehaviourPunCallbacks
{
    [SerializeField]
    GameObject playerPrefab;

    // Start is called before the first frame update
    void Start()
    {
        if(PhotonNetwork.IsConnected)
        {
            if(playerPrefab != null)
            {
                int randomPoint = Random.Range(-20, 20);
                float xEntryPoint = 629.0f +randomPoint;
                randomPoint = Random.Range(-20, 20);
                float yEntryPoint = 39.6f;
                float zEntryPoint = 2005.0f +randomPoint;
                PhotonNetwork.Instantiate(playerPrefab.name, new Vector3(xEntryPoint,yEntryPoint,zEntryPoint),
                Quaternion.identity );
            }

        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public override void OnJoinedRoom()
    {
        //base.OnJoinedRoom();
        Debug.Log(PhotonNetwork.NickName + " joined to " + PhotonNetwork.CurrentRoom.Name);
        PhotonNetwork.LoadLevel("GameScene");
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
       // base.OnPlayerEnteredRoom(newPlayer);
        Debug.Log(newPlayer.NickName + " joined room "+ PhotonNetwork.CurrentRoom.Name +" players in room "+ PhotonNetwork.CurrentRoom.PlayerCount);
    }
}
