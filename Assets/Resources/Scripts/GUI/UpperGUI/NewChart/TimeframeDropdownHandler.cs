using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeframeDropdownHandler : MonoBehaviour
{
//    static string sSelectedTimeFrame = "";
    public Dropdown dropdown;

    void Start(){
        var dropdown = transform.GetComponent<Dropdown>();

         PopulateList();

        dropdown.onValueChanged.AddListener(delegate { 
            DropdownItemSelected(dropdown);
        });
        dropdown.captionText.text = TradingPlatform.eTimeFrame.M1.ToString();
    }

    void PopulateList()
    {
        string[] eTimeframes = Enum.GetNames(typeof(TradingPlatform.eTimeFrame));
        List<string> lTimeframes = new List<string>(eTimeframes);
        dropdown.AddOptions(lTimeframes);
    }
    void DropdownItemSelected(Dropdown dropdown)
    {
        int index = dropdown.value;

        string sTimeframe = dropdown.options[index].text;
        //Debug.Log("Selected sTimeframe "+sTimeframe +" index "+index);
        NewChartHandler.setTimeframe(sTimeframe);
    }

}
