using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Alveo3_Client;
using System;

public class NewChartHandler : MonoBehaviour
{
    [SerializeField]
    GameObject NewChartGUI;

    public GameObject NumberBarsInputField;
    public Text NumberOfBarsText;
    public Text NumberBarsOutput;

    private static string sChartSymbol = "";
    //private static Alveo3Client.eTimeFrame eChartTimeframe;
    private static string sChartTimeframe;
    static string sSelectedSymbol = "";
    public static int iNumberOfBars = 100;
    private TradingPlatform TBT;

    public void NewChartButtonValues(){
//Debug.Log("NewChartButtonValues");
        TBT = new TradingPlatform();
        //TBT.vActivateNewChartGui();
    }

    public static void setSymbol(string sSymbol){
        sChartSymbol = "EUR/USD";
        if(sSymbol.Length> 3){
            sChartSymbol = sSymbol;
        }
    }
    public static void setTimeframe(String sTimeframe){
        //Debug.Log("setTimeframe sTimeframe "+sTimeframe);
        sChartTimeframe = sTimeframe;
    }

    public int vSetNumberOfBars()
    {
        int iInt=0;
        try{
            string sNumberOfBars = NumberOfBarsText.text;
            iInt = Int32.Parse(sNumberOfBars);
            NumberOfBarsText.text = sNumberOfBars;
            NumberBarsOutput.text = sNumberOfBars;
            NewChartHandler.iNumberOfBars = iInt;
            Debug.Log("Number of bars "+iInt);
            //NumberOfBarsText.text = sNumberOfBars;
        }catch(Exception ex){
            Debug.Log("NewChartHandler vSetNumberOfBars\n"+ ex.ToString());
        }
        if(iInt < 100) iInt = 100;
        if(iInt > 5000) iInt = 5000;
        return iInt;
    }
    public void vStartExecution(){
        //Debug.Log("Execute button pressed");
        iNumberOfBars = vSetNumberOfBars();
        bool bIsSubscribed = bInSubscribedList(sChartSymbol);
        if(!bIsSubscribed)
        {
            //Add this symbol to the list of symbols that will get quotes
            Processor.vSendSubscribeMessage(sChartSymbol);
        }
        //Create a new instance of
        //Debug.Log("vStartExecution "+sChartSymbol+" "+sChartTimeframe+" "+iNumberOfBars);
        Processor.vOpenNewChart(sChartSymbol,sChartTimeframe,iNumberOfBars);
        NewChartGUI.SetActive(false);
        //TBT = new TradingPlatform();
//Debug.Log("NewChartHandler vStartExecution Call vActivateOdersButton");
        //TBT.vActivateOdersButton();
        MainCallbacks MainCB = new MainCallbacks();
        MainCB.NewChartButtonCallback();
    }

    bool bInSubscribedList(string sSymbol){
        foreach (var sSymbols in TradingPlatform.lSubscribedSymbols)
        {
            if(sSymbols == sSymbol){
                return true;
            }
        }
        return false;
    }
}
