using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SymbolDropdownHandler : MonoBehaviour
{
//    static string sSelectedSymbol = "";
    public Dropdown dropdown;
    public Text SymbolLabel;
    void Start(){
        var dropdown = transform.GetComponent<Dropdown>();

        dropdown.options.Clear();
        List<string> Symbols = new List<string>();
        for(int i=0; i<TradingPlatform.lActiveSymbols.Count; i++){
            Symbols.Add(TradingPlatform.lActiveSymbols[i]);
        }
        PopulateList(Symbols);
        //Fill symbols in the dropdown
        try{
            dropdown.onValueChanged.AddListener(delegate { 
                DropdownItemSelected(dropdown);
            dropdown.captionText.text = Symbols[0];
        });
        }catch{
            Debug.Log("Invalid dropdown delegate");
        }
    }

    void PopulateList(List<string> lString)
    {
        //Debug.Log("lString "+lString);
        dropdown.AddOptions(lString);
    }
    void DropdownItemSelected(Dropdown dropdown)
    {
        int index = dropdown.value;

        string sSymbol = dropdown.options[index].text;
        //Debug.Log("Selected symbol "+sSymbol);
        //TextBox.text = sSymbol;
        dropdown.captionText.text = dropdown.options[index].text;
        NewChartHandler.setSymbol(sSymbol);
        //SymbolLabel.text = sSymbol;
    }
}
