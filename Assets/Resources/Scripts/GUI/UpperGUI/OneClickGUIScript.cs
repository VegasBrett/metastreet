using UnityEngine;
using System;
using UnityEngine.UI;

public class OneClickGUIScript : MonoBehaviour
{
    private static bool bOneClickEnabled = false;
    private static float fTP = 0.0f;
    private static float fSL = 0.0f;
    private static float fLotSize = 0.01f;

    // Start is called before the first frame update
 
    public GameObject TP_Text;
    public GameObject SL_Text;
    public GameObject Lots_Text;
    public GameObject OneClickToggle;

    public void vTPPlusButton(){
        fTP += 0.1f;
        vUpdateTPTextField();
    }
    public void vTriggerTpMinus(){
        fTP -= 0.1f;
        if(fTP < 0.0) fTP = 0.0f;
        vUpdateTPTextField();
    }
    private void vUpdateTPTextField(){
        TP_Text.GetComponent<Text>().text = fTP.ToString();
    }

    public void vSLPlusButton(){
        fSL += 0.1f;
        vUpdateSLTextField();
    }
    public void vSLMinusButton(){
        fSL -= 0.1f;
        if(fSL < 0.0) fSL = 0.0f;
        vUpdateSLTextField();
    }
    private void vUpdateSLTextField(){
        SL_Text.GetComponent<Text>().text = fSL.ToString();
    }
    public void vLotsPlusButton(){
        fLotSize += 0.01f;
        vUpdateLotsTextField();
    }
    public void vLotsMinusButton(){
        fLotSize -= 0.01f;
        if(fLotSize < 0.01) fLotSize = 0.01f;
        vUpdateLotsTextField();
    }

    private void vUpdateLotsTextField(){
        //The extra digit can cause the need to round to 2 digits, there is no round for float
        float f = fLotSize;
        fLotSize = (float)Math.Round(f * 100f) / 100f;
        Lots_Text.GetComponent<Text>().text = fLotSize.ToString();
    }

    public void vOneClickToggle(){
        bOneClickEnabled = OneClickToggle.GetComponent<Toggle>().isOn;
        //bOneClickEnabled = !bOneClickEnabled;
    }
    public bool bGetOneClick(){
        return bOneClickEnabled;
    }
    public float fGetSL(){
        return fSL;
    }
    public float fGetTP(){
        return fTP;
    }
    public float fGetLots(){
        return fLotSize;
    }
}

