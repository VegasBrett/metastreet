using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainCallbacks : MonoBehaviour
{
    public static MainCallbacks instance {get; set;}

    [SerializeField]
    GameObject NewChartGUI;
    [SerializeField]
    GameObject NewOrderGUI;
    [SerializeField]
    GameObject HistoricOrderImage;
    [SerializeField]
    GameObject OpenOrderImage;

    bool bNewChartGUI = false;
    static public bool bActiveOpenOrdersButton = false;
    static public bool bActiveOrdersHistoryButton = false;
   public Text ErrorBox; 
    private void Awake(){
        instance = this;
        //OpenOrderImage.SetActive(false);
        //HistoricOrderImage.SetActive(false);
    }
/*
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
*/
    public void NewChartButtonCallback(){
        //Debug.Log("New Chart Button selected");
        try{
            bNewChartGUI = !bNewChartGUI;
            NewChartGUI.SetActive(bNewChartGUI);
        } catch(Exception ex){
            Debug.Log("Exception MainCallbacks NewChartButtonCallback bNewChartGUI"+bNewChartGUI);
        }
    }

    public void WatchListButtonCallback(){
        Debug.Log("Watch List Button selected");
    }

    public void HelpButtonCallback(){
        Debug.Log("Help Button selected");
    }

    public void FileButtonCallback(){
        Debug.Log("File Button selected");
    }

    public void NewOrderButtonCallback(){
        //Debug.Log("New Order Button selected");
        NewOrderGUI.SetActive(true);
    }

    public void vErrorMessageBox(string sErrorMessage){
        ErrorBox.text = sErrorMessage;
    }

    public void OrdersButtonCallback(){
        //Debug.Log("Orders Button selected");
        bActiveOpenOrdersButton = !bActiveOpenOrdersButton;
        HistoricOrderImage.SetActive(false);
        OpenOrderImage.SetActive(false);
        if(bActiveOpenOrdersButton){
            OpenOrderImage.SetActive(true);
        }
        TradingPlatform.vShowOpenOrders();
    }

    public void OrderHistoryButtonCallback(){
        //Debug.Log("Order History Button selected");
        bActiveOrdersHistoryButton = !bActiveOrdersHistoryButton;
        OpenOrderImage.SetActive(false);
        HistoricOrderImage.SetActive(false);
        //If button pushed = true
        if(bActiveOrdersHistoryButton){
            HistoricOrderImage.SetActive(true);
        }
        TradingPlatform.vShowOrderHistory();
    }
    public void ExitButtonCallback(){
        //Debug.Log("Exit Button Selected");
        Alveo3_Client.Processor.vSendExitMessage();
    }


}
