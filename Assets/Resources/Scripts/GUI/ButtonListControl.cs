using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonListControl : MonoBehaviour
{
    [SerializeField]
    private GameObject buttonTemplate;

    [SerializeField]
    private int[] inArray;

    private List<GameObject> buttons;

    void GenerateList(){

        
        if(buttons.Count> 0)
        {
            foreach(GameObject button in buttons)
            {
                Destroy(button.gameObject);
            }
            buttons.Clear();
        }

        foreach(int i in inArray)
        {
            GameObject button = Instantiate(buttonTemplate) as GameObject;
            button.gameObject.SetActive(true);

            button.GetComponent<ButtonListButton>().SetText("Button #"+i);

            button.transform.SetParent(buttonTemplate.transform.parent,false);
        }
    }
    public void ButtonClicked(string myTextString)
    {
        Debug.Log(myTextString);
    }

}
