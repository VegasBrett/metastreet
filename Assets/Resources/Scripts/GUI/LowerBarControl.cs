using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LowerBarControl : MonoBehaviour
{
    LowerBarControl instance;

    //static public GameObject LowerData;
    public  Text CurrentTime;
    public Text GOBalance;
    public Text GOUnrealizedPips;
    public Text GOOpenLots;

    public Text GOMaxLots;
    public Text GOPercentProfit;

    void Awake(){
        instance = this;        
    }

    public  void vUpdateTime(){
        try{
            //Debug.Log("Enter vUpdateTime");

            //Update balance
            try{
                double dCurrentBalance = MoneyManagement.dGetCurrentBalance();
                string sBalanceValue = string.Format("{0:N2}",dCurrentBalance);
                //GOBalance.text = sBalanceValue;
            }catch(Exception ex){
                Debug.Log("LowerBarControl vUpdateTime GOBalance.text"+ex);
            }
            try{
            DateTime dtNow = DateTime.Now;              
            string sTimeValue = "GMT: " +  string.Format("{0:00}/{1:00}/{2} {3:00}:{4:00}:{5:00}",dtNow.Month,dtNow.Day,dtNow.Year,dtNow.Hour, dtNow.Minute,dtNow.Second);
            //CurrentTime.GetComponent<Text>().text = sTimeValue;
            }catch(Exception ex){
                Debug.Log("LowerBarControl vUpdateTimeCurrentTime "+ex);
            }

            try{
                string sUnrealizedValue = string.Format("{0:N1}",MoneyManagement.vGetUnrealizedPips());
                //GOUnrealizedPips.text = sUnrealizedValue;
            }catch(Exception ex){
                Debug.Log("LowerBarControl GOUnrealizedPips.text "+ex);
            }
            string sOpenLotsValue = string.Format("{0:N2}",MoneyManagement.dGetOpenLots());
            //GOOpenLots.text = sOpenLotsValue;

            string sMaxLotsValue = string.Format("/{0:N2}",MoneyManagement.dGetMaxPosition());
            //GOMaxLots.text = sMaxLotsValue;

            string sPercentProfitToday = string.Format("/{0:N2}",MoneyManagement.dGetMaxPosition());;
            //GOPercentProfit.text = sPercentProfitToday;
//Debug.Log(sTimeValue+" "+sUnrealizedValue+" "+sMaxLotsValue);
        }catch(Exception ex){
            Debug.Log("LowerBarControl vUpdateTime "+ex);
        }
    }
}
