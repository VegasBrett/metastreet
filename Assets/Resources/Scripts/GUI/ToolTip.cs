using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToolTip : MonoBehaviour
{
    [SerializeField]
    private Camera uiCamera;

    private Text tooltipText;
    private RectTransform backgroundRectTransform;
    //private static Tooltip instance;


    private void Awake(){
        //instance = this;
        backgroundRectTransform = transform.Find("background").GetComponent<RectTransform>();
        tooltipText = transform.Find("tiptext").GetComponent<Text>();

        ShowTooltip("Random text");
    }

    private void Update(){
        Vector2 localPoint;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(transform.parent.GetComponent<RectTransform>(),Input.mousePosition,uiCamera, out localPoint);
        transform.localPosition = localPoint;
    }

    private void ShowTooltip(string sToolTip){
        gameObject.SetActive(true);

        tooltipText.text = sToolTip;
        float textPaddingSize = 4f;
        Vector2 backgroundSize = new Vector2(tooltipText.preferredWidth + textPaddingSize*2, tooltipText.preferredHeight + textPaddingSize*2);
        backgroundRectTransform.sizeDelta = backgroundSize;
    }

    private void HideTooltip(){
        gameObject.SetActive(false);
    }
    public static void ShowTooltip_Static(string stoolTip){
        //instance.ShowTooltip(stoolTip);
    }
    public static void HideTooltip_Static(){
        //instance.HideTooltip();
    }
}
