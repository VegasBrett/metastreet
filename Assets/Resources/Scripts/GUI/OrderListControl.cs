using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Alveo3_Client;
using Photon.Pun;

public class OrderListControl : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private GameObject orderTemplate;

    private List<GameObject> lOrders;

    void Start()
    {
        lOrders = new List<GameObject>();
    }

    public void OrderText(Order cOrder)
    {
        if(lOrders.Count >= 3000)
        {
            int iOldestOrder = lOrders.Count-1;
            GameObject tempOrder = lOrders[iOldestOrder];
            Destroy(tempOrder);

        }
        GameObject orderRecord = Instantiate(orderTemplate) as GameObject;
        orderRecord.SetActive(true);
        string sTest = "12";
        //GetComponent<IdItem>().SetID(cOrder.Id.ToString());
        GetComponent<IdItem>().SetID(sTest);
        //orderRecord.transform.SetParent(orderTemplate.transform.partent,false);
    }


}
