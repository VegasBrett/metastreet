using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityUITable;
using Alveo3_Client;


namespace Alveo3_Client{
    public class OrderHistoryManager : MonoBehaviour
    {
        [SerializeField]
        public List<Order> OrderHistory;
        static public OrderHistoryManager Instance;

        private void Awake(){
            Instance = this;
            OrderHistory = new List<Order>(); 
        }
        public OrderHistoryManager(){
            Instance = this;
            OrderHistory = new List<Order>();
            //Initialize the table with blank orders
            Order BlankOrder = new Order();
            /*
            for(int iOrder = 0; iOrder < 100; iOrder++){
                OrderHistory.Add(BlankOrder);
            }
            */
        }
        private void OnDestroy() {
            OrderHistory.Clear();
        }
        public void vClearList(){
            OrderHistory.Clear();
        }
        public void vInsertOrder(Order thisOrder, bool bNewClosedOrder){
/*
            for(int iOrder = 0; iOrder < OrderHistory.Count; iOrder++){
                if(OrderHistory[iOrder].Id < 0){
                    //Update this record to a valid order
                    OrderHistory[iOrder] = thisOrder;
        Debug.Log("OrderHistoryManager vInsertOrder OrderHistory.Count "+OrderHistory.Count+" OID ");
                }
            }
            */
            OrderHistory.Add(thisOrder);
            if(OrderHistory.Count>= 100) {
                TradingPlatform.vSetActivateOrderHistoryTable(true);
                //Debug.Log("OrderHistoryManager vInsertOrder Set History table active");
            }
    //Debug.Log("OrderHistoryManager vInsertOrder OrderHistory.Count "+OrderHistory.Count);
        }
    }
}
