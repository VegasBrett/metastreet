using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IdItem : MonoBehaviour
{
    [SerializeField]
    Text Field;

    public void SetID(string sText){
        Field.text = sText;
    }
}
