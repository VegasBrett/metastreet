using System;
using System.Collections.Generic;
using UnityEngine;
using Alveo3_Client;

namespace Alveo3_Client{
     public class OpenOrderManager2 : MonoBehaviour
    {

        [SerializeField]
        public List<Order> OpenOrders2;

        static public OpenOrderManager2 Instance;

        private void Awake(){
            Instance = this;
            OpenOrders2 = new List<Order>(); 
        }
        public OpenOrderManager2(){
            Instance = this;
            OpenOrders2 = new List<Order>();    
        }
        private void OnDestroy() {
            OpenOrders2.Clear();
        }
        public void vClearList(){
            OpenOrders2.Clear();
        }
        public void vInsertOrder(Order thisOrder){
            OpenOrders2.Add(thisOrder);
    //Debug.Log("OrderHistoryManager vInsertOrder OpenOrders2.Count "+OpenOrders2.Count);
        } 
    }
}
