using System;
using System.Collections.Generic;
using UnityEngine;
using Alveo3_Client;
using SLS.Widgets.Table;
using UnityEngine.EventSystems;

public class CurrentOrderTableManager : MonoBehaviour
{
   static public CurrentOrderTableManager Instance;
    public Table CurrentOrederTable;
    private List<Order> CurrentOrders;

    static int iRecord = 0;
    static bool initial = true;
    private const int ROWS = 3;
    //private List<DataTableData.Population> poplist;

private void Start() {
    Instance = this;
}


    public bool vInitializeOrderTable() {

        // this instantiates default fonts for ALL tables when not explicitely defined on an individual table
        //  ProTip: You don't need to do this if you just set a font for your table in the editor
        MakeDefaults.Set();

        this.CurrentOrederTable = this.GetComponent<Table>();

        this.CurrentOrederTable.ResetTable();
        this.CurrentOrederTable.AddTextColumn("X");
        this.CurrentOrederTable.AddTextColumn("Id");            
        this.CurrentOrederTable.AddTextColumn("Symbol"); 
        this.CurrentOrederTable.AddTextColumn("Quantity");
        this.CurrentOrederTable.AddTextColumn("Side");
        this.CurrentOrederTable.AddTextColumn("Type");
        this.CurrentOrederTable.AddTextColumn("Price");
        this.CurrentOrederTable.AddTextColumn("Open Date");
        this.CurrentOrederTable.AddTextColumn("Fill Date");
        this.CurrentOrederTable.AddTextColumn("Status");
        this.CurrentOrederTable.AddTextColumn("Open Price");
        this.CurrentOrederTable.AddTextColumn("Current Price");
        this.CurrentOrederTable.AddTextColumn("Pips");
        this.CurrentOrederTable.AddTextColumn("Profit");
        this.CurrentOrederTable.AddTextColumn("Open Type");
        this.CurrentOrederTable.AddTextColumn("S/L");
        this.CurrentOrederTable.AddTextColumn("T/P");
        this.CurrentOrederTable.AddTextColumn("Expiration Date");
        this.CurrentOrederTable.AddTextColumn("Trade C.");
        this.CurrentOrederTable.AddTextColumn("TimeInForce");
        this.CurrentOrederTable.AddTextColumn("Trailing Stop");
        this.CurrentOrederTable.AddTextColumn("Comment");

        // Initialize Your Table
        this.CurrentOrederTable.Initialize(this.OnTableSelected);
        this.OnHeaderClick(this.CurrentOrederTable.columns[0], null);

        // Populate Your Rows (obviously this would be real data here)
        for (int iRecord = 0; iRecord < TradingPlatform.lOpenOrders.Count; iRecord++){
            Datum d = Datum.Body(iRecord.ToString());
            Order thisOrder = TradingPlatform.lOpenOrders[iRecord];

            int iID = 0;
            try{
                iID = (int)thisOrder.Id;                
            }catch (Exception e) { 
                Debug.Log("Message AddArg exception "+e);
                return false;
            }
            d.elements.Add("X");
            d.elements[0].color = new Color(1, .0f, 0.0f);
            d.elements[0].backgroundColor = new Color(0.0f, 0, 0);
            d.elements.Add(iID.ToString());
            d.elements.Add(thisOrder.Symbol.ToString());
            d.elements.Add(thisOrder.Quantity.ToString());
            d.elements.Add(thisOrder.Side.ToString()); 
            d.elements.Add(thisOrder.Type.ToString());          
            d.elements.Add(thisOrder.Price.ToString());
            d.elements.Add(thisOrder.OpenDate.ToString());
            d.elements.Add(thisOrder.FillDate.ToString());
            d.elements.Add(thisOrder.Status.ToString());
            d.elements.Add(thisOrder.OpenPrice.ToString());
            d.elements.Add(thisOrder.CurrentPrice.ToString());
            d.elements.Add(thisOrder.Pips.ToString());
            d.elements.Add(thisOrder.Profit.ToString());
            d.elements.Add(thisOrder.OpenType.ToString());
            d.elements.Add(thisOrder.StopLoss.ToString());
            d.elements.Add(thisOrder.TakeProfit.ToString());
            d.elements.Add(thisOrder.ExpirationDate.ToString());
            d.elements.Add(thisOrder.TradeCommission.ToString());
            d.elements.Add(thisOrder.TimeInForce.ToString());
            d.elements.Add(thisOrder.TrailingStop.ToString());
            d.elements.Add(thisOrder.Comment.ToString());
            this.CurrentOrederTable.data.Add(d);
        }
        // Draw Your Table
        this.CurrentOrederTable.StartRenderEngine();
        return true;
    }

    // Handle the row selection however you wish (be careful as column can be null here
    //  if your table doesn't fill the full horizontal rect space and the user clicks an edge)
    private void OnTableSelected(Datum datum, Column column) {
      string cidx = "N/A";
      if(column != null)
        cidx = column.idx.ToString();
      if(datum != null){
          int iRec = Int32.Parse( datum.uid);
          int iOID = (int)TradingPlatform.lOpenOrders[iRec].Id;
        print("You Clicked: " + datum.uid + " Column: " + cidx + " Close Order "+ iOID+" "+DateTime.Now);
        Processor.vCloseOrder(iOID);
      }

      else
        print("Selection Cleared!");
    }

    public void HandleSelectRandomClick() {
      int idx = UnityEngine.Random.Range(0, this.CurrentOrederTable.data.Count - 1);
      this.CurrentOrederTable.SetSelected(this.CurrentOrederTable.data[idx], null, true, true);
    }

    public void HandleClearSelectionClick() {
      this.CurrentOrederTable.lastSelectedDatum = null;
    }
    private void OnHeaderClick(Column column, PointerEventData e) {
        //print("Header You Clicked Column: " + column);
        //if(column == 0){

        //}
    }


}
