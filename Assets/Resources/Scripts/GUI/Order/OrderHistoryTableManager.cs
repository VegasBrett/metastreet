using System;
using System.Collections.Generic;
using UnityEngine;
using Alveo3_Client;
using SLS.Widgets.Table;

public class OrderHistoryTableManager : MonoBehaviour
{

    static public OrderHistoryTableManager Instance;
    public Table table;
    private List<Order> OrderHistory;

    static int iRecord = 0;
    static bool initial = true;
    private const int ROWS = 3;
    //private List<DataTableData.Population> poplist;

private void Start() {
    Instance = this;
}


    public bool vInitializeOrderTable() {

      // this instantiates default fonts for ALL tables when not explicitely defined on an individual table
      //  ProTip: You don't need to do this if you just set a font for your table in the editor
      MakeDefaults.Set();

      this.table = this.GetComponent<Table>();

      this.table.ResetTable();

        this.table.AddTextColumn("Id");            
        this.table.AddTextColumn("Symbol"); 
        this.table.AddTextColumn("Quantity");
        this.table.AddTextColumn("Side");
        this.table.AddTextColumn("Type");
        this.table.AddTextColumn("Price");
        this.table.AddTextColumn("Status");
        this.table.AddTextColumn("Open Price");
        this.table.AddTextColumn("Close Price");
        this.table.AddTextColumn("Pips");
        this.table.AddTextColumn("Open Type");
        this.table.AddTextColumn("Close Type");
        this.table.AddTextColumn("Profit");
        this.table.AddTextColumn("Stop Loss");
        this.table.AddTextColumn("Take Profit");
        this.table.AddTextColumn("Open Date");
        this.table.AddTextColumn("Fill Date");
        this.table.AddTextColumn("Close Date");
        this.table.AddTextColumn("Expiration Date");
        this.table.AddTextColumn("Commission");
        this.table.AddTextColumn("TimeInForce");
        this.table.AddTextColumn("Trailing Stop");
        this.table.AddTextColumn("One Click");
        this.table.AddTextColumn("Comment");

      // Initialize Your Table
      this.table.Initialize(this.OnTableSelected);

      // Populate Your Rows (obviously this would be real data here)

        for (int iRecord = 0; iRecord < TradingPlatform.lHistoricOrders.Count; iRecord++){ //TradingPlatform.lHistoricOrders.Count
            //Debug.Log("vInsertOrder Creating Record "+iRecord);
            Datum d = Datum.Body(iRecord.ToString());
            Order thisOrder = TradingPlatform.lHistoricOrders[iRecord];

            int iID = 0;
            try{
                iID = (int)thisOrder.Id;                
            }catch (Exception e) { 
                Debug.Log("Message AddArg exception "+e);
                return false;
            }
            d.elements.Add(iID.ToString());
            //Debug.Log("vInitializeOrderTable thisOrder.Symbol "+thisOrder.Symbol.ToString());
            d.elements.Add(thisOrder.Symbol.ToString());
            //d.elements[0].color = new Color(0, .5f, 0.8f);
            //d.elements[0].backgroundColor = new Color(0.2f, 0, 0);
            d.elements.Add(thisOrder.Quantity.ToString());
            d.elements.Add(thisOrder.Side.ToString());
            string sType = thisOrder.Type.ToString();  
            d.elements.Add(sType);          
            d.elements.Add(thisOrder.Price.ToString());
            d.elements.Add(thisOrder.Status.ToString());
            d.elements.Add(thisOrder.OpenPrice.ToString());
            d.elements.Add(thisOrder.ClosePrice.ToString());
            d.elements.Add(thisOrder.Pips.ToString());
            d.elements.Add(thisOrder.OpenType.ToString());
            d.elements.Add(thisOrder.CloseType.ToString());
            d.elements.Add(thisOrder.Profit.ToString());
            d.elements.Add(thisOrder.StopLoss.ToString());
            d.elements.Add(thisOrder.TakeProfit.ToString());
            d.elements.Add(thisOrder.OpenDate.ToString());
            d.elements.Add(thisOrder.FillDate.ToString());
            d.elements.Add(thisOrder.CloseDate.ToString());
            d.elements.Add(thisOrder.ExpirationDate.ToString());
            d.elements.Add(thisOrder.TradeCommission.ToString());
            d.elements.Add(thisOrder.TimeInForce.ToString());
            d.elements.Add(thisOrder.TrailingStop.ToString());
            d.elements.Add(thisOrder.OneClick.ToString());
            d.elements.Add(thisOrder.Comment.ToString());
            this.table.data.Add(d);
        }
        // Draw Your Table
        this.table.StartRenderEngine();
        return true;
    }

    // Handle the row selection however you wish (be careful as column can be null here
    //  if your table doesn't fill the full horizontal rect space and the user clicks an edge)
    private void OnTableSelected(Datum datum, Column column) {
      string cidx = "N/A";
      if(column != null)
        cidx = column.idx.ToString();
      if(datum != null)
        print("You Clicked: " + datum.uid + " Column: " + cidx);
      else
        print("Selection Cleared!");
    }

    public void HandleSelectRandomClick() {
      int idx = UnityEngine.Random.Range(0, this.table.data.Count - 1);
      this.table.SetSelected(this.table.data[idx], null, true, true);
    }

    public void HandleClearSelectionClick() {
      this.table.lastSelectedDatum = null;
    }

}
