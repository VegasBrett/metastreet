using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using Alveo3_Client;

namespace Alveo3_Client{
     public class OpenOrderManager : MonoBehaviour
    {
        static public OpenOrderManager Instance;
        static private string sClassName = "OpenOrderManager";

        static  double  faudusd = 10.0;
        static double feurusd = 10.0;
        static double fgbpusd = 10.0;
        static double fnzdusd = 10.0;
        static double fusdcad = 7.5147;
        static double fusdchf = 10.0901;
        static double fusdjpy = 9.3827;

        [SerializeField]
        private List<Order> OpenOrders;

        private void Awake(){
            Instance = this;
            OpenOrders = new List<Order>();
        }
        public OpenOrderManager(){
            Instance = this;
            OpenOrders = new List<Order>();       
        }
        private void OnDestroy() {
            OpenOrders.Clear();
        }
        public void vClearList(){
            OpenOrders.Clear();
        }
        
        public void vInsertOrder(Order thisOrder){
            OpenOrders.Add(thisOrder);
        }

/*
        public void vComputeOrderCurrentValues(){
            //Itterate through each of the open orders compute total profit and lots, pips for each order and profit each order
            double dTotalOpenLots = 0.0;
            double dTotalOpenProfit = 0.0;
            double dTotalOpenPips = 0.0;


            int iOrders = TradingPlatform.lOpenOrders.Count;
            if(iOrders > 0){
                foreach (var cOrder in TradingPlatform.lOpenOrders){
                    double dLots = cOrder.Quantity;
                    dTotalOpenLots += dLots;
                    //Locate the current quote for this symbol
                    Instrument ThisSymbolInstrument = TradingPlatform.vFindIntrument(cOrder.Symbol);
                    double fPriceMult = (double)ThisSymbolInstrument.TickSize * 10.0;
                    double dCommission = (double)ThisSymbolInstrument.Comission/100.0;//Convert from percent to decimal
                    if(TradingPlatform.lCurrentSymbolQuote.Count > 0){
                        foreach (var cSymbol in TradingPlatform.lCurrentSymbolQuote){
                            if(cSymbol.Symbol == cOrder.Symbol){
                                double dAsk = cSymbol.Ask;
                                double dBid = cSymbol.Bid;
                                double dOpenPrice = cOrder.OpenPrice;
                                double dPips = 0.0;
                                if(cOrder.Side == Order.eTradeSide.Buy){
                                    //for buy orders use the current bid price for profit and pips
                                    dPips = dBid - cOrder.OpenPrice * fPriceMult;
                                }else{
                                    //for sell orders use the current ask price for profit and pips
                                    dPips = cOrder.OpenPrice - dAsk * fPriceMult;
                                }
                                dTotalOpenPips += dPips;
                                double dProfit = (dPips - dCommission);
                                break;
                            }
                        }
                    }                    

                }
            }
        }
*/

        static public void vComputeOpenOrderProfit()
        {
            string sMethodName = "vComputeOpenOrderProfit";
            string sErrorDetails = "";
            string sErrorType = "";
            //bool bDelayedQuotes = false;
            do
            {
            double dTotalOpenLots = 0.0;
            double dTotalOpenProfit = 0.0;
            double dTotalOpenPips = 0.0;
                try
                {
                    int iTotalOrders = TradingPlatform.lOpenOrders.Count;
                    //Console.WriteLine("ProcessTrades Total orders " + iTotalOrders);
                    foreach (var cOrder in TradingPlatform.lOpenOrders)
                    {

                        //double fCurrentBalance = 0.0;
                        int iOID = (int)cOrder.Id;
                        string sSymbol = cOrder.Symbol;


                        // Load Instrument specific information
                        double fCommisionCost = 0.0;
                        double fPipSize = 0.0;
                        Instrument ThisSymbolInstrument = TradingPlatform.vFindIntrument(cOrder.Symbol);
                        fCommisionCost = ThisSymbolInstrument.Commission;
                        fPipSize = (double)ThisSymbolInstrument.TickSize * 10.0;

                        double fBid = 0.0;
                        double fAsk = 0.0;
                        double FullLotValue = 10.0;
                        foreach (var cSymbol in TradingPlatform.lCurrentSymbolQuote)
                        {
                            if (cSymbol.Symbol == cOrder.Symbol)
                            {
                                fAsk = cSymbol.Ask;
                                fBid = cSymbol.Bid;
                                break;
                            }
                        }
                        cOrder.Profit = 0.0;
                        cOrder.Pips = 0.0;
                        if (fBid > 0.0 && fAsk > 0.0)
                        {
                            double fOpenPrice = cOrder.OpenPrice;
                            double fLotSize = cOrder.Quantity;
                            int iStatus = (int)cOrder.Status;
                            double fPips = 0.0;
                            //Side = 0 = Buy Side = 1 = Sell 
                            int iSide = (int)cOrder.Side;
                            DateTime dtNow = DateTime.Now;
                            dTotalOpenLots += cOrder.Quantity;
                            if (iStatus == 2)
                            {
                                double fCommission = fLotSize * fCommisionCost * -1.0;
                                cOrder.TradeCommission = Math.Round(fCommission, 2);
                                double fProfit = 0.0;
                                if (iSide == 0)
                                {
                                    //Buy Order
                                    fPips = (fBid - fOpenPrice) / fPipSize;
                                    cOrder.Pips = fPips;
                                    //Compute Order profit
                                    fProfit = (fPips * fLotSize * FullLotValue) + fCommission;
                                    cOrder.Profit = fProfit;
                                    cOrder.CurrentPrice = fBid;

                                }
                                else
                                {
                                    //Sell order
                                    fPips = (fOpenPrice - fAsk) / fPipSize;
                                    fProfit = (fPips * fLotSize * FullLotValue) + fCommission;
                                    cOrder.Profit = fProfit;
                                    cOrder.Pips = fPips;
                                    cOrder.CurrentPrice = fAsk;
                                }
                            }
                            else
                            {
                                cOrder.Profit = 0.0;
                                cOrder.Pips = 0.0;
                            }
                            cOrder.Pips = Math.Round(cOrder.Pips, 1);
                            cOrder.Profit = Math.Round(cOrder.Profit, 2);
                            dTotalOpenPips += cOrder.Pips;
                            dTotalOpenProfit += cOrder.Profit;
                        }
                    }
                    MoneyManagement.vSetUnrealizedPips(dTotalOpenPips);
                    MoneyManagement.vSetOpenLots(dTotalOpenLots);
                    MoneyManagement.vSetOpenProfit(dTotalOpenProfit);

                }
                catch (Exception e)
                {
                    sErrorDetails = string.Format("Outer Exception");
                    sErrorType = e.ToString();
                    TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                }
                Thread.Sleep(100);
            } while (!TradingPlatform.bExit);
        }

        static public void vProcessOrderUpdate(Order cOrderRecord1)
        {
            string sMethodName = "vProcessOrderUpdate";
            cOrderRecord1.Profit = Math.Round(cOrderRecord1.Profit,2);
            try
            {
                if((int)cOrderRecord1.Status < 20){
                    //Check the open orders list if this order is new or an update
                    bool bFoundRecord = false;
                    if(TradingPlatform.lOpenOrders.Count > 0){
                        int IRecords = TradingPlatform.lOpenOrders.Count;
                        for(int iRecord = 0; iRecord < IRecords; iRecord++){
                            if(TradingPlatform.lOpenOrders[iRecord].Id == cOrderRecord1.Id){
                                // found match so update the record
                                TradingPlatform.lOpenOrders[iRecord] = cOrderRecord1;
                                bFoundRecord = true;
                            }
                        }
                        // If order is new added it to the list
                        if(!bFoundRecord){
                            TradingPlatform.lOpenOrders.Add(cOrderRecord1);
                        }
                    } else{
                        //no records so add this record
                        TradingPlatform.lOpenOrders.Add(cOrderRecord1);                         
                    }
                    TradingPlatform.bRefreshOpenOrderTables = true;
                    TradingPlatform.lOpenOrders.Sort();
                    TradingPlatform.lOpenOrders.Reverse();
                    //TradingPlatform.lOpenOrders.Add(cOrderRecord1);
                }else{
                    //Check to determine is new close
                    //Check the open order to determine if this order is in the list and remove it
                    if(TradingPlatform.lOpenOrders.Count > 0){
                        int IRecords = TradingPlatform.lOpenOrders.Count;
                        for(int iRecord = 0; iRecord < IRecords; iRecord++){
                            if(TradingPlatform.lOpenOrders[iRecord].Id == cOrderRecord1.Id){
                                // found match so update the record
                                TradingPlatform.lOpenOrders.RemoveAt(iRecord);
                                TradingPlatform.bRefreshOpenOrderTables = true;
                            }
                        }
                    }
                    bool bFoundHistoryRecord = false;
                    //Check the closed order list
                    if(TradingPlatform.lHistoricOrders.Count > 0){
                        int IRecords = TradingPlatform.lHistoricOrders.Count;
                        for(int iRecord = 0; iRecord < IRecords; iRecord++){
                            //Check if this order is already in the history order
                            if(TradingPlatform.lHistoricOrders[iRecord].Id == cOrderRecord1.Id){
                                // found match so update the record
                                TradingPlatform.lHistoricOrders[iRecord] = cOrderRecord1;
                                TradingPlatform.bRefreshOrderTables = true;
                                bFoundHistoryRecord = true;
                                MoneyManagement.vComputeTodaysCloseOrders();
                            }
                        }
                    }
                    if(!bFoundHistoryRecord){
                        TradingPlatform.lHistoricOrders.Insert(0, cOrderRecord1);
                        TradingPlatform.bRefreshOrderTables = true;                                
                    }
                    //TradingPlatform.lHistoricOrders.Add(cOrderRecord1);
                    TradingPlatform.lHistoricOrders.Sort();
                    TradingPlatform.lHistoricOrders.Reverse();
                    TradingPlatform.bRefreshOrderTables = true;
                }
                //TradingPlatform.vShowOpenOrders();
            }
            catch (Exception e)
            {
                string sErrorDetails = string.Format("Exception TradingOrders.Add");
                string sErrorType = e.ToString();
                Debug.Log(sMethodName + " Error "+sErrorDetails+" "+sErrorType);
                //TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
            }
        }//end vProcessOrderUpdate
    }
}
