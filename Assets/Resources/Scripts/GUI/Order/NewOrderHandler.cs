using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Alveo3_Client;

public class NewOrderHandler : MonoBehaviour
{
    public static NewOrderHandler instance {get; set;}
    
    private double dTP = 0.0f;
    private double dSL = 0.0f;
    private double OneClickSLOffset = 0.0f;
    private double OneClickTPOffset = 0.0f;
    private double OneClickLotSize = 0.01f;
    private double dLotSize = 0.01f;

    private bool bSell = false;
    private bool bBuy = true;
    public GameObject NewOrderGUI;
    public GameObject TP_Text;
    public GameObject SL_Text;
    public GameObject Lots_Text;
    public GameObject sComment_Text;
    public GameObject BuyToggle;
    public GameObject SellToggle;
    public Dropdown SymbolDropdown;
    public GameObject Bid_Text;
    public GameObject Ask_Text;
    double dAsk = 0.0f;
    double dBid = 0.0f;
    static double fPriceMult = 0.00001f; // Used to convert pips to price
    static int iPriceRound = 5;
    Instrument ThisSymbolInstrument;
    
    private void Awake(){
        instance = this;

    }
    // Start is called before the first frame update
    void Start()
    {

        //Load the current chart symbol
        string sActiveSymbol = "";
        if(TradingPlatform.iNumberOfCharts > 0){
            for(int i=0; i < TradingPlatform.iNumberOfCharts; i++){
                sActiveSymbol = TradingPlatform.Charts[i].sChartSymbol;
            }
        }
        var dropdown = transform.GetComponent<Dropdown>();

        int iChartSymbolIndex = 0;
        List<string> Symbols = new List<string>();

        //Add only symbols of the open charts
        for(int i=0; i<TradingPlatform.iNumberOfCharts; i++){
            Symbols.Add(TradingPlatform.Charts[i].sChartSymbol);
         }
         PopulateListSymbol(Symbols);
        //Fill symbols in the dropdown
        SymbolDropdown.onValueChanged.AddListener(delegate { 
            DropdownItemSelected(SymbolDropdown);
        });

        SymbolDropdown.captionText.text = Symbols[iChartSymbolIndex];
        vUpdateSymbolInstrument(Symbols[iChartSymbolIndex]);
        foreach (var cSymbol in TradingPlatform.lCurrentSymbolQuote){
            if(sActiveSymbol == cSymbol.Symbol){
                dAsk = cSymbol.Ask;
                dBid = cSymbol.Bid;
                string sBid_Text = string.Format("{0:N5}",dBid);
                Bid_Text.GetComponent<Text>().text = sBid_Text;
                string sAsk_Text = string.Format("{0:N5}",dAsk);
                Ask_Text.GetComponent<Text>().text = sAsk_Text;
                break;
            }
        }
        bBuy = BuyToggle.GetComponent<Toggle>().isOn;
        bSell = SellToggle.GetComponent<Toggle>().isOn;
        OneClickGUIScript OneClick = new OneClickGUIScript();
        bool bOneClickEnabled = OneClick.bGetOneClick();
Debug.Log("NewOrderHandler Start bOneClickEnabled "+bOneClickEnabled);
        if(bOneClickEnabled){
            OneClickTPOffset = OneClick.fGetTP();
            OneClickSLOffset = OneClick.fGetSL();
            OneClickLotSize = OneClick.fGetLots();
            //Assume Buy order as default
            dSL = dAsk - (OneClickSLOffset * fPriceMult);
            dTP = dAsk + (OneClickTPOffset * fPriceMult);
            dLotSize = OneClickLotSize;
        }
        vSetSLTextField();
        vUpdateTPTextField();
        vUpdateLotsTextField();
    }

    void PopulateListSymbol(List<string> lString)
    {
        SymbolDropdown.AddOptions(lString);
    }
    void DropdownItemSelected(Dropdown dropdown)
    {
        int index = SymbolDropdown.value;
        string sSymbol = SymbolDropdown.options[index].text;
        Debug.Log("Selected symbol "+sSymbol);
        NewChartHandler.setSymbol(sSymbol);
        vUpdateSymbolInstrument(sSymbol);
    }

    void vUpdateSymbolInstrument(string sSymbol){
        ThisSymbolInstrument = TradingPlatform.vFindIntrument(sSymbol);
        fPriceMult = (float)ThisSymbolInstrument.TickSize;
        if(fPriceMult == 0.00000001) iPriceRound = 8;        
        if(fPriceMult == 0.000001) iPriceRound = 6;
        if(fPriceMult == 0.00001) iPriceRound = 5;
        if(fPriceMult == 0.0001) iPriceRound = 4;
        if(fPriceMult == 0.001) iPriceRound = 3;
        if(fPriceMult == 0.01) iPriceRound = 2;
        if(fPriceMult == 0.1) iPriceRound = 1;
        fPriceMult *= 10.0f;  // convert to pips
    }

    public void vLotsPlusButton(){
        dLotSize += 0.01f;
        vUpdateLotsTextField();
    }
    public void vLotsMinusButton(){
        dLotSize -= 0.01f;
        if(dLotSize < 0.01) dLotSize = 0.01f;
        vUpdateLotsTextField();
    }
    private void vUpdateLotsTextField(){
        //The extra digit can cause the need to round to 2 digits, there is no round for float
        double d = dLotSize;
        dLotSize = Math.Round(d, 2);
        string sLots_Text = string.Format("{0:N2}",dLotSize);
        Lots_Text.GetComponent<Text>().text = sLots_Text;
    }

     public void vTPPlusButton(){
        dTP += 0.1 *fPriceMult;
        vUpdateTPTextField();
    }
    public void vTriggerTpMinus(){
        dTP -= 0.1 *fPriceMult ;
        if(dTP < 0.0) dTP = 0.0f;
        vUpdateTPTextField();
    }
    private void vUpdateTPTextField(){
        dTP = Math.Round(dTP, iPriceRound);
        TP_Text.GetComponent<Text>().text = dTP.ToString();
    }

    public void vSLPlusButton(){
        //User can change the value since last button update
        dSL += 0.1f *fPriceMult;
        vSetSLTextField();
    }
    public void vSLMinusButton(){
        //User can change the value since last button update
        dSL -= 0.1f *fPriceMult;
       if(dSL < 0.0) dSL = 0.0f;
        vSetSLTextField();
    }
    private void vSetSLTextField(){
        dSL = Math.Round(dSL, iPriceRound);
        SL_Text.GetComponent<Text>().text = dSL.ToString();
    }
    public void vCancelButton(){
        dSL = 0.0f;
        dTP = 0.0f;
        dLotSize = 0.01f;
        NewOrderGUI.SetActive(false);
    }
    public void vSubmitButton(){
        string sComment = sComment_Text.GetComponent<Text>().text;
    }
    public void vSideToggle(){
        bBuy = BuyToggle.GetComponent<Toggle>().isOn;
        bSell = SellToggle.GetComponent<Toggle>().isOn;
        if(!bSell && !bBuy){
            bBuy = true;
            BuyToggle.GetComponent<Toggle>().isOn = bBuy;
        }
        //bSell = !bSell;
        //SellToggle.GetComponent<Toggle>().isOn = bSell;
    }

    public void vOneClickBuy(){
        string sSymbol = "";
        if(TradingPlatform.lOpenOrders.Count >= 50){
            string sErrorMessage = "Error: Open order limit reached 50 orders";
            MainCallbacks MCB = new MainCallbacks();
            MCB.vErrorMessageBox(sErrorMessage);
            return;
        }
        int iNumberActiveSymbols = TradingPlatform.lCharts.Count;
        if(iNumberActiveSymbols == 1) sSymbol = TradingPlatform.lCharts[0].sGetSymbol();
        double dAsk = 0;
        dAsk = TradingPlatform.dGetAsk(sSymbol);
        double dBid = TradingPlatform.dGetAsk(sSymbol);
 
        OneClickGUIScript OneClick = new OneClickGUIScript();
        bool bOneClickEnabled = OneClick.bGetOneClick();
        OneClickTPOffset = OneClick.fGetTP();
        OneClickSLOffset = OneClick.fGetSL();
        OneClickLotSize = OneClick.fGetLots();
        vUpdateSymbolInstrument(sSymbol);
        dSL = Math.Round(dBid - (OneClickSLOffset * fPriceMult),iPriceRound);
        dTP = Math.Round(dBid + (OneClickTPOffset * fPriceMult),iPriceRound);
        dLotSize = Math.Round(OneClickLotSize,2);

        Order cOpenOrder = new Order();
        TradingPlatform.iMaxOID++;
        cOpenOrder.Id = TradingPlatform.iMaxOID;
        cOpenOrder.OpenType = Order.eTradeType.Market;
        cOpenOrder.Symbol = sSymbol;
        cOpenOrder.Side = Order.eTradeSide.Buy;
        cOpenOrder.Quantity = dLotSize;
        cOpenOrder.StopLoss = dSL;
        cOpenOrder.TakeProfit = dTP;
        cOpenOrder.ExpirationDate = null;
        cOpenOrder.OpenDate = DateTime.UtcNow;
        cOpenOrder.AccountBalanceFilled = Math.Round(MoneyManagement.dGetCurrentBalance(),2);
        cOpenOrder.Status = Order.eOrderStatus.Processing;
        cOpenOrder.OpenPrice = dAsk;
        cOpenOrder.Price = dAsk;
        cOpenOrder.OneClick = true;
        Debug.Log("vOneClickBuy Symbol "+ sSymbol +" cOpenOrder.Id "+cOpenOrder.Id+" dSL "+dSL +" dTP "+dTP+" dLotSize "+dLotSize+" dAsk "+dAsk);
        Processor.vSendNewOrder(cOpenOrder);
    }
    public void vOneClickSell(){
        string sSymbol = "";
        if(TradingPlatform.lOpenOrders.Count >= 50){
            string sErrorMessage = "Error: Open order limit reached 50 orders";
            MainCallbacks MCB = new MainCallbacks();
            MCB.vErrorMessageBox(sErrorMessage);
            return;
        }
        int iNumberActiveSymbols = TradingPlatform.lCharts.Count;
        if(iNumberActiveSymbols == 1) sSymbol = TradingPlatform.lCharts[0].sGetSymbol();
        double dBid = TradingPlatform.dGetAsk(sSymbol);
        double dAsk = 0;
        dAsk = TradingPlatform.dGetAsk(sSymbol);

        OneClickGUIScript OneClick = new OneClickGUIScript();
        bool bOneClickEnabled = OneClick.bGetOneClick();
        OneClickTPOffset = OneClick.fGetTP();
        OneClickSLOffset = OneClick.fGetSL();
        OneClickLotSize = OneClick.fGetLots();
        vUpdateSymbolInstrument(sSymbol);
        dSL = Math.Round(dAsk + (OneClickSLOffset * fPriceMult),iPriceRound);
        dTP = Math.Round(dAsk - (OneClickTPOffset * fPriceMult),iPriceRound);
        dLotSize = Math.Round(OneClickLotSize,2);
        Order cOpenOrder = new Order();
        TradingPlatform.iMaxOID++;
        cOpenOrder.Id = TradingPlatform.iMaxOID;
        cOpenOrder.OpenType = Order.eTradeType.Market;
        cOpenOrder.Symbol = sSymbol;
        cOpenOrder.Side = Order.eTradeSide.Sell;
        cOpenOrder.Quantity = dLotSize;
        cOpenOrder.StopLoss = dSL;
        cOpenOrder.TakeProfit = dTP;
        cOpenOrder.ExpirationDate = null;
        cOpenOrder.OpenDate = DateTime.UtcNow;
        cOpenOrder.AccountBalanceFilled = Math.Round(MoneyManagement.dGetCurrentBalance(),2);
        cOpenOrder.Status = Order.eOrderStatus.Processing;
        cOpenOrder.OpenPrice = dBid;
        cOpenOrder.Price = dBid;
        cOpenOrder.OneClick = true;    
        Debug.Log("vOneClickSell Symbol "+ sSymbol +" cOpenOrder.Id "+cOpenOrder.Id+" dSL "+dSL +" dTP "+dTP+" dLotSize "+dLotSize+" dBig "+dBid);
        Processor.vSendNewOrder(cOpenOrder);
    }
}
