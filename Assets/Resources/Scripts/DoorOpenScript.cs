using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class DoorOpenScript :MonoBehaviourPunCallbacks
{
     [SerializeField]
  string SelectTag;
      [SerializeField]
    GameObject Door;
 
    public float movementSpeed = 0.000010f;
    bool bIsDoorMoving = false;
    bool bIsDoorClosed = true;
    bool bIsDoorOpen = false;
  private float StationWaitTimer = 0f;
    private float allowedWaitTime = 1.0f;
    float minDistance = 0.01f;

    bool bInsideTrigger = false;
    bool bOutsideTrigger = true;

    bool bOpenDoor = false;
    bool bCloseDoor = false;

    [SerializeField]
        Transform DoorOpenStop;
    [SerializeField]
        Transform DoorCloseStop;    
    [SerializeField]
        Transform DoorSensor;    
    [SerializeField]
    Material Transparent;
    [SerializeField]
    Material NormalDoorColor; 

    void Start(){
    }

    void Update()
    {
        if(StationWaitTimer > 0){
            StationWaitTimer -= Time.deltaTime;
            return;
        }
        Door.GetComponent<Renderer>().material = NormalDoorColor;
        //Door.SetActive(true);
      /*
        if(bIsDoorMoving){
            float movementStep = movementSpeed * Time.deltaTime; // distance traveled since last frame
            if(bIsDoorClosed && bInsideTrigger){
                float distanceToOpenStop = CheckDistanceToOpenStopSensor();
                Debug.Log("Open door movementStep "+ movementStep+ " Distance to stop = "+distanceToOpenStop);
                transform.position = Vector3.MoveTowards(transform.position, DoorOpenStop.position,movementStep);
             }
            if(bIsDoorOpen && bOutsideTrigger){
                float distanceToCloseStop = CheckDistanceToCloseStopSensor();
                if(distanceToCloseStop <= .1) bIsDoorMoving = false;
                Debug.Log("Open door movementStep "+ movementStep+ " Distance to stop = "+distanceToCloseStop);
                transform.position = Vector3.MoveTowards(transform.position, DoorCloseStop.position,movementStep);
             }
        }
        */
/*         
        if(Mathf.Abs(fDistance) > minDistance){
                float localMovementSpeed = movementSpeed;
                float movementStep = localMovementSpeed * Time.deltaTime; // distance traveled since last frame
                if(bDoorClose) 
                {
                    Debug.Log("Open door movementStep "+ movementStep+ " Distance to stop = "+fDistance);
                    transform.position = Vector3.MoveTowards(transform.position, DoorOpenStop.position,-movementStep);

                } else
                {
                    Debug.Log("Close door movementStep "+ movementStep+" Distance to stop = "+fDistance);
                    transform.position = Vector3.MoveTowards(transform.position, DoorCloseStop.position,movementStep);
                }
            } else{
                bIsDoorMoving = false;
                if(bDoorClose){
                    bDoorClose = false;
                } else{
                    bDoorClose = true;
                }
            }

        }
        */
    }
    private void OnTriggerEnter (Collider Client)
    {
        if(Client.gameObject.tag == "Player"){

            Debug.Log("Collider Entered "+ Client.gameObject.tag +" Set door to inactive");
            // set door invisisable
            StationWaitTimer = allowedWaitTime;
            //Door.SetActive(false);
            Door.GetComponent<Renderer>().material = Transparent;



            bIsDoorMoving = true;
            bInsideTrigger = true;
            bOutsideTrigger = false;
            
        }
    }
    private void OnTriggerExit (Collider Client)
    {
       if(Client.gameObject.tag == "Player"){
            Debug.Log("Collider Exited "+ Client.gameObject.tag);
        bInsideTrigger = false;
        bOutsideTrigger = true;
        bIsDoorMoving = true;
       }  
    }
    private float CheckDistanceToOpenStopSensor()
    {
        float distanceToOpenStop = Vector3.Distance(DoorSensor.position, DoorOpenStop.position);
        float distance = distanceToOpenStop;
        //if(!bDoorClose) distance = distanceToCloseStop;
        /*
        if(distance <= minDistance)
        {
            //Start a wait timer to allow client walk through door
            StationWaitTimer = allowedWaitTime;
        }
        */
        return distanceToOpenStop;
    }
    private float CheckDistanceToCloseStopSensor()
    {
        float distanceToCloseStop = Vector3.Distance(DoorSensor.position, DoorCloseStop.position);
        return distanceToCloseStop;
    }
}
