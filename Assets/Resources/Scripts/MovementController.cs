using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class MovementController : MonoBehaviourPunCallbacks
{
    [SerializeField]
    float speed = 4f;

    [SerializeField]
    private float rotSensitivity = 0.3f;

    [SerializeField]
    GameObject FPSCamera;

    private Vector3 velocity = Vector3.zero;
    private Vector3 rotation = Vector3.zero;
    private float cameraUpDownRotation = 0f;
    private float currentCameraUpDownRotation = 0f;

    private Rigidbody rb;
    public Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
         if(Input.GetKeyDown(KeyCode.UpArrow)){
            animator.SetBool("IsWalking",true);
           //Debug.Log("UpArrow Pressed "+ animator.GetBool("IsWalking"));
        }
        if(Input.GetKeyUp(KeyCode.UpArrow)){
            animator.SetBool("IsWalking",false);
            //Debug.Log("UpArrow released "+ animator.GetBool("IsWalking"));
        } 
 
         //Calculate movement velocity as a 3d vector
        float _xMovement = Input.GetAxis("Horizontal");
        float _zMovement = Input.GetAxis("Vertical");

        Vector3 _movementHosizontal = transform.right * _xMovement;
        Vector3 _movementVertical = transform.forward * _zMovement;

        //Final movement velocity
        Vector3 _movementVelocity = (_movementHosizontal + _movementVertical).normalized *speed;

        //Apply movement
        Move(_movementVelocity);
        
        //Calculate rotation as a 3d vector
        float _yRotation = Input.GetAxis("Mouse X");
        Vector3 _rotationVector = new Vector3(0,_yRotation,0) * rotSensitivity;

        //Apply rotation
        Rotate(_rotationVector);

        //Calculate look up and down rotation
        float _cameraUpDownRotation = Input.GetAxis("Mouse Y") * rotSensitivity;
        //Debug.Log("Mouse Y "+ Input.GetAxis("Mouse Y") + " _cameraUpDownRotation "+_cameraUpDownRotation);

        //Apply Camera rotation
        RotateCamera(_cameraUpDownRotation);

    }
    private void FixedUpdate()
    {
        //if(!photonView.IsMine) return;
        if(velocity != Vector3.zero)
        {
            rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
        }

        if(rotation != Vector3.zero)
        {
            rb.MoveRotation(rb.rotation * Quaternion.Euler(rotation));
        }

        if(FPSCamera != null)
        {
            //Debug.Log("Mouse Y "+ Input.GetAxis("Mouse Y") + " _cameraUpDownRotation "+_cameraUpDownRotation);
        //Calculate look up and down rotation
            //float _cameraUpDownRotation = Input.GetAxis("Mouse Y") * rotSensitivity;
            currentCameraUpDownRotation -= cameraUpDownRotation;
            currentCameraUpDownRotation = Mathf.Clamp(currentCameraUpDownRotation, -80.0f, 70.0f);
            FPSCamera.transform.localEulerAngles = new Vector3(currentCameraUpDownRotation,0,0);
        }
    }

    void Move(Vector3 MovementVelocity)
    {
        velocity = MovementVelocity;
    }

    void Rotate(Vector3 rotationVector)
    {
        rotation = rotationVector;

    }
    void RotateCamera(float cameraRotation )
    {
        cameraUpDownRotation = cameraRotation;
    }
}
