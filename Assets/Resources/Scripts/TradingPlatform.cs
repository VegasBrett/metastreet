using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CodeMonkey.Utils;
using Alveo3_Client;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;

//using UnityEngine.SceneManagement;

public struct aChart{


    public aChart ( SingleChart chartPointer, string Symbol, string Timeframe, string ID, Instrument Instrument1){
        thisChart = chartPointer;
        sChartSymbol = Symbol;
        sChartTimeframe = Timeframe;
        sID = ID;
        bNewQuote = false;
        bNewBar = false;
        iTimeFrame = 1; // default 1 minute
        TimeOfNextBar = DateTime.MinValue;
        cInstrument = Instrument1;

    }  
    public SingleChart thisChart {get; set;}
    public string sChartSymbol {get; set;}
    public string sChartTimeframe {get; set;}
    public string sID  {get; set;}
    public bool bNewQuote {get; set;}
    public bool bNewBar {get; set;}
    public int iTimeFrame {get; set;}
    public DateTime TimeOfNextBar { get; set;}
    public Instrument cInstrument { get; set;}
}
public class TradingPlatform : MonoBehaviour
{
    // Start is called before the first frame update
    public static TradingPlatform instance{get; set;}
    public enum EventLevel : int
    {
        Fatal = 0,
        Error = 1,
        Warn = 2,
        Info = 3,
        Debug = 4,
        Trace = 5,
    }
    public enum eTimeFrame
    {
        Unknown = -1,
        M1 = 1, // native support
        M5 = 5, // native support
        M15 = 15, // native support
        M30 = 30, // native support
        H1 = 60,
        H2 = 120,
        H4 = 240,
        D1 = 60 * 24, // native support
        D7 = 60 * 24 * 7,
        MN = 60 * 24 * 30,
    }

    [SerializeField]
     private Sprite dotSprite;
     [SerializeField]
    private Sprite PointSprite;

    [SerializeField]
    public GameObject NewChartGUI;

    [SerializeField] private RectTransform graphContainer;
    private RectTransform LowerData;
    private RectTransform labelTemplateXAxis;
    private RectTransform labelTemplateYAxis; 
     private RectTransform labelChartCurrentClosePrice;
     private RectTransform labelTimeToNextBar;
    private RectTransform dashTemplateXHorizontal;
    private RectTransform dashTemplateYVertical;
    private RectTransform LabelTemplateOrder;
    private List<GameObject> gameObjectList;
    private GameObject gameObjectTimeTillBar;
    //private GameObject gameObjectTime;
    private List<GameObject> barZerogameObjectList;
    private GameObject tooltipGameObject;
    private Func<int, string> getAxisLabelX;
    private Func<double, string> getAxisLabelY;


    public static double barWidth = 4.0F;
    public static double graphHeight;
    public static double graphWidth;

    public static double yMax;
    public static double yMin;
    public static double xMax;
    public static double xMin;
    public static double xRange;
    public static double yRange;
    public static DateTime dt1970 = new DateTime(1970, 1, 1, 0, 0, 0);

    public static  aChart[] Charts = new aChart[30];
    public static int iNumberOfCharts = 0;
    public int iMaxCharts = 30;
    public GameObject LoginPanel;
    [SerializeField]
    public GameObject NewChartButton;
    [SerializeField]
    public GameObject FileButton;
    [SerializeField]
    public  GameObject NewOrderButton;
    [SerializeField]
    public GameObject OpenOrderListButton;
    [SerializeField]
    public GameObject ClosedOrderListButton;
    [SerializeField]
    public GameObject WatchListButton;
    [SerializeField]
    public GameObject OneClickTrading;

    //public GameObject EnterTradingButton;
    public GameObject EnterUsernameInput;
    //public GameObject OrderHistoryTable;



    public GameObject GOOneClickBuy;
    public GameObject GOOneClickSell;

    //Lower GUI objects
    public Text CurrentTime;
    public Text GOBalance;
    public Text GOUnrealizedPips;
    public Text GOOpenLots;

    public Text GOMaxLots;
    public Text GOPercentProfit;
    public Text tPipsToday;
    public Text tMargin;
    public Text tFreeMargin;
    public Text tFramesPS;

    //Chart related
    private static GameObject Window_Graph;
    private static GameObject SingleSymbolChart;

    public Text UserNameText;
    public Text AccountIdText;

    //[SerializeField]
     //private Sprite dotSprite;
    // [SerializeField]
    //private Sprite PointSprite;
    private bool bOneClickButtonsVisable = false;

    private static string sProgramName = "MetastreetServerClient";
    public static string sVersion = "3.01.01";
    //private static string sClassName = "MetastreetServerClient";
    public static EventLevel eMinLevel = EventLevel.Debug;
    public const char end_of_file_tag = '^';
    private static List<cMessagesSentQueue> lWaitingReplyMessage = new List<cMessagesSentQueue>();

    static public List<BarWithDT>[] lBars = new List<BarWithDT>[30];

    public class cMessagesSentQueue
    {
        //public Message SentMessage { get; set; }
        public DateTime dtExpired { get; set; }
        public int iAttempts { get; set; }
    }


    TcpClient aos_socket;
    //Socket aos_socket;
 //   string server_address = "";
 //   int server_port = 0;

    public static List<Instrument> lInstruments = new List<Instrument>();
    public static List<Instrument> lActiveInstruments = new List<Instrument>();
    public static List<string> lActiveSymbols = new List<string>();

    public static List<cTradingAccount> lTradingAccounts = new List<cTradingAccount>();

    public static bool bBalanceUpdated = false;

    public static List<Order> lHistoricOrders = new List<Order>();
    public static List<Order> lOpenOrders = new List<Order>();
    //public static List<Order> lTodaysOrders = new List<Order>();
    public static bool bAllOrdersLoad = false;
    public static bool bRefreshOrderTables = false;
    public static bool bRefreshOpenOrderTables = false;
    public static List<string> lSubscribedSymbols = new List<string>();
    public static string sUserName = "";

    public static bool bConnected = false;
    public static long iUID = -1;
    private static DateTime dtLastProcessed;
    public static List<Quote> lCurrentSymbolQuote;
    static public List<Bar> valueList;
    public static List<SingleChart> lCharts;
    //static private bool bBarsAvailable = false;
    static private int iCurrentMinute = -1;
    static private int iCurrentSecond = -1;
    private int iFrames = 0;
    static public int iMaxOID = 0;
    static public bool bActivateOrderHistoryTable = false;
    static private bool bOrderHistoryTableIsActive = false;
    static private bool bOpenOrderBefore = false;
    static public bool bExit = false;
    static private int iActiveAccount = 0;
    static private OneClickGUIScript OneClick = new OneClickGUIScript();

    private static Processor Proc;
    private static MoneyManagement MM;
    private void Awake(){

        instance = this;

        Proc = new Processor();
        MM = new MoneyManagement(); 
        //graphContainer = transform.Find("graphContainer").GetComponent<RectTransform>();
        labelTemplateXAxis = graphContainer.Find("LabelTemplateXaxis").GetComponent<RectTransform>();
        labelTemplateYAxis = graphContainer.Find("LabelTemplateYaxis").GetComponent<RectTransform>();
        LabelTemplateOrder = graphContainer.Find("LabelTemplateOrder").GetComponent<RectTransform>();
        dashTemplateXHorizontal = graphContainer.Find("dashTemplateXHorizontal").GetComponent<RectTransform>();
        dashTemplateYVertical = graphContainer.Find("dashTemplateYVertical").GetComponent<RectTransform>();
        labelChartCurrentClosePrice = graphContainer.Find("labelChartCurrentClosePrice").GetComponent<RectTransform>();
        labelTimeToNextBar = graphContainer.Find("labelTimeToNextBar").GetComponent<RectTransform>();
        //OrderHistoryTable = GameObject.Find("HistoryTable").GetComponent<GameObject>();

        gameObjectList = new List<GameObject>();
        barZerogameObjectList = new List<GameObject>();
        List<Bar> valueList = new List<Bar>();
        //lTodaysOrders = new List<Order>();

        GOBalance = GameObject.Find("Balance").GetComponent<Text>();

        LowerBarControl LBC = new LowerBarControl();
        //LBC.vInitObjects(GOBalance);
        //List<Bar> lBars = new List<Bar>();
        
        //IGraphVisual lineGraphVisual = new LineGraphVisual(graphContainer, dotSprite, Color.green, new Color(1,0,0,.5f));
        //IGraphVisual barGraphVisual = new BarChartVisual(graphContainer, Color.green, 0.3f);
        //ShowGraph(valueList,barGraphVisual, -1);
        
//        bool useBarChart = true;
        /*
        FunctionPeriodic.Create(() => {
            //valueList.Clear();
            if(useBarChart) {
                ShowGraph(valueList,barGraphVisual,-1);
            }
           // useBarChart = !useBarChart;
        },5.0f);
        */

        //ShowTooltip("This is a tooltip",new Vector2(100,100));   
    }

    void Start()
    {
        OneClick = new OneClickGUIScript();

        //EnterUsernameInput.SetActive(true);
        //EnterTradingButton.SetActive(true);


        //tooltipGameObject = graphContainer.Find("tooltip").gameObject;
        //Debug.Log("TradingBarChart Awake");

        lCharts = new List<SingleChart>();
        lCurrentSymbolQuote = new List<Alveo3_Client.Quote>();
        //Debug.Log("TradingPlatform Start player nickname "+PhotonNetwork.NickName);
        if(PhotonNetwork.NickName == "") UserNameText.text = "bconklin"; //set for debuging trading platform
        SetUserName.SetPlayerName(PhotonNetwork.NickName);

        UserNameText.text = PhotonNetwork.NickName;
        if(UserNameText.text == "") UserNameText.text = "bconklin"; //set for debuging trading platform

        StartTradingClient();
        //Debug.Log("Starting Alveo3Client");
        dtLastProcessed = DateTime.Now;
        //Start the continuous socket thread processing
        //IGraphVisual barGraphVisual = new BarChartVisual(graphContainer, Color.green, 0.3f);
        new Thread(() =>
        {
            Processor.InCommingProcess();
        }).Start();

        new Thread(() =>
        {
            OpenOrderManager.vComputeOpenOrderProfit();
        }).Start();

        //SingleSymbolChart.SetActive(true);
    }

    void Update() {
    //float fps = 1 / Time.unscaledDeltaTime;
    //fpsDisplay.text = "" + fps;
        iFrames++;

    }

    void FixedUpdate() {
        if(bExit){
           // Application.Quit();
            SceneManager.LoadScene("TradingFloor"); 
        }
        DateTime dtNow =  DateTime.UtcNow;
        bool bNewMinute = false;
        if(dtNow.Minute != iCurrentMinute){
            bNewMinute = true;
        }
        //Debug.Log("FixedUpdate new Minute "+dtNow+" iCurrentMinute "+iCurrentMinute);

        Processor.vHeartbeatThread();

        if(bAllOrdersLoad){
            vActiveOrderListsButtons();
        }
        if(bBalanceUpdated){

            bBalanceUpdated = false;
        }
        if(lOpenOrders.Count > 0){
            for(int OpenOrder = 0; OpenOrder < lOpenOrders.Count; OpenOrder++){
                if((int)lOpenOrders[OpenOrder].Status > 20){
                    lOpenOrders.RemoveAt(OpenOrder);
                }
            }
        }

        bool bOneClickEnabled = OneClick.bGetOneClick();
        if(!bOneClickButtonsVisable){
            if(bOneClickEnabled){
                GOOneClickBuy.SetActive(true);
                GOOneClickSell.SetActive(true);
                bOneClickButtonsVisable = true;             
            }
        } else{
            if(!bOneClickEnabled){
                GOOneClickBuy.SetActive(false);
                GOOneClickSell.SetActive(false);
                bOneClickButtonsVisable = false;             
            }
        }

        if(bRefreshOrderTables){
            // if bActiveOrdersHistoryButton is true then show the table
            if(MainCallbacks.bActiveOrdersHistoryButton) vShowOrderHistory();
            bRefreshOrderTables = false;
        }
        //if( (dtNow.Second != iCurrentSecond && lOpenOrders.Count > 0) || bOpenOrderBefore){
        bool bShowOpenOrders = false;
        if(dtNow.Second != iCurrentSecond ){
            tFramesPS.text = "Fames "+iFrames;
            iFrames = 0;
            if(bOpenOrderBefore) bShowOpenOrders = true;
            if(lOpenOrders.Count > 0) bShowOpenOrders = true;
        }

        if(bShowOpenOrders){
            if(lOpenOrders.Count > 0 || bOpenOrderBefore) {
                // if bActiveOpenOrdersButton is true then show the table
                if(MainCallbacks.bActiveOpenOrdersButton && (dtNow.Second%4 == 0)){
                    MoneyManagement.vComputeTodaysCloseOrders();
                    vShowOpenOrders();
                }
                //use so that when order is closed the table is updated one time after all or closed
                bOpenOrderBefore = true;
                if(lOpenOrders.Count == 0 ) {
                    bOpenOrderBefore = false;
                    MoneyManagement.vComputeTodaysCloseOrders();
                }
            } else{
                bOpenOrderBefore = false;
            }
        }
        //update the current time and the charts countdown time to next bar
        //LowerBarControl LBC = new LowerBarControl();
        vUpdateTime();
        if(iNumberOfCharts>0){
           for(int IChartIndex = 0; IChartIndex < iNumberOfCharts; IChartIndex++){
                SingleChart cThisChart = Charts[IChartIndex].thisChart;
                if(bNewMinute){
                    for(int iQuoteIndex = 0; iQuoteIndex <lCurrentSymbolQuote.Count; iQuoteIndex++){
                        if(Charts[IChartIndex].sChartSymbol == lCurrentSymbolQuote[iQuoteIndex].Symbol){
                            vUpdateBarZero(lCurrentSymbolQuote[iQuoteIndex]);
                            //vUpdateChartTime(IChartIndex);
                        }
                    }
                }
                if(dtNow.Second != iCurrentSecond ){
                    vUpdateChartTime(IChartIndex);
                }
                if(Charts[IChartIndex].bNewBar || Charts[IChartIndex].bNewQuote) {
                    int iMaxBars = -1;
                    for(int iQuoteIndex = 0; iQuoteIndex <lCurrentSymbolQuote.Count; iQuoteIndex++){
                        if(Charts[IChartIndex].sChartSymbol == lCurrentSymbolQuote[iQuoteIndex].Symbol){
                            //Debug.Log(dtNow+" Call ShowGraph chart symbol "+Charts[IChartIndex].sChartSymbol+" quote symbol "+lCurrentSymbolQuote[iQuoteIndex].Symbol+" chart index "+IChartIndex+" iQuoteIndex "+iQuoteIndex);
                            ShowGraph(IChartIndex, iMaxBars);
                            break;
                        }
                    }
                    if(Charts[IChartIndex].bNewBar) Charts[IChartIndex].bNewBar = false;
                    if(Charts[IChartIndex].bNewQuote) Charts[IChartIndex].bNewQuote = false;

                } 
            }
        }
        iCurrentSecond = dtNow.Second;
        iCurrentMinute = dtNow.Minute;
    }
    static public Instrument vFindIntrument(string sSymbol){
        Instrument cInstrument = null;
        //Search for the matching symbol
        for(int iIndex = 0; iIndex < lInstruments.Count; iIndex++){
            if(lInstruments[iIndex].Symbol == sSymbol){
                cInstrument = lInstruments[iIndex];
                break;
            }
        }
        return cInstrument;
    }

    public void vSetActiveAccount(int iAccount){
        iActiveAccount = iAccount;
        AccountIdText.text = iActiveAccount.ToString();       
    }
    public void StartTradingClient()
    {
        //Debug.Log("StartTradingClient");
        sUserName = SetUserName.sGetUserName();
        bConnected = true;

        System.Random rnd = new System.Random();
        string sNewSession = rnd.Next(100, 10000000).ToString();
        Message cMessage = new Message();
        string sLoginString = cMessage.LoginRequest(sUserName,sNewSession,sVersion);
        Processor.sSend_msg(sLoginString);
        
        LoginPanel.SetActive(false);
        NewChartButton.SetActive(true);
        FileButton.SetActive(true);
        WatchListButton.SetActive(true);
        OneClickTrading.SetActive(true);
    }

    private void vActiveOrderListsButtons(){
        OpenOrderListButton.SetActive(true);
        ClosedOrderListButton.SetActive(true);
        NewOrderButton.SetActive(true);
    }

    public static void vShowOrderHistory(){

        bool bSuccess = OrderHistoryTableManager.Instance.vInitializeOrderTable();
        if(bSuccess) {
            bRefreshOrderTables = false;
            return;
        } else{
            //Issue with object reference attempt to reset the list
            List<Order> HoldOrders = new List<Order>();
            for(int iRecord = 0; iRecord < lHistoricOrders.Count; iRecord++){
                HoldOrders.Add(lHistoricOrders[iRecord]);
            }
            lHistoricOrders.Clear();
            lHistoricOrders = new List<Order>();
            for(int iRecord = 0; iRecord < lHistoricOrders.Count; iRecord++){
                lHistoricOrders.Add(HoldOrders[iRecord]);
            }
            bSuccess = OrderHistoryTableManager.Instance.vInitializeOrderTable();
            if(bSuccess) {
                bRefreshOrderTables = false;
            }         
        }
    }

    public static void vShowOpenOrders(){
        bool bSuccess = CurrentOrderTableManager.Instance.vInitializeOrderTable();
        if(bSuccess) {
            bRefreshOrderTables = false;
            return;
        } else{
            //Issue with object reference attempt to reset the list
            List<Order> HoldOrders = new List<Order>();
            for(int iRecord = 0; iRecord < lOpenOrders.Count; iRecord++){
                HoldOrders.Add(lOpenOrders[iRecord]);
            }
            lOpenOrders.Clear();
            lOpenOrders = new List<Order>();
            for(int iRecord = 0; iRecord < lOpenOrders.Count; iRecord++){
                lOpenOrders.Add(HoldOrders[iRecord]);
            }
            bSuccess = CurrentOrderTableManager.Instance.vInitializeOrderTable();
            if(bSuccess) {
                bRefreshOrderTables = false;
            }         
        }
    }
    public void vShowCurrentOrder(){
        bool bSuccess = OrderHistoryTableManager.Instance.vInitializeOrderTable();
        if(bSuccess) {
            bRefreshOrderTables = false;
        }
    }

    public void vUpdateTime(){
        try{
            //Debug.Log("Enter vUpdateTime");

            //Update Lower bar information
            //LowerBarControl LBC = new LowerBarControl();
            //LBC.vUpdateTime();
 
            double dCurrentBalance = MoneyManagement.dGetCurrentBalance();
            string sBalanceValue = string.Format("{0:N2}",dCurrentBalance);
            GOBalance.text = sBalanceValue;

            DateTime dtNow = DateTime.UtcNow;            
            string sTimeValue = "GMT: " +  string.Format("{0:00}/{1:00}/{2} {3:00}:{4:00}:{5:00}",dtNow.Month,dtNow.Day,dtNow.Year,dtNow.Hour, dtNow.Minute,dtNow.Second);
            CurrentTime.text = sTimeValue;

            string sUnrealizedValue = "";
            double dUnRealizedPips = 0.0;
            try{
                dUnRealizedPips = MoneyManagement.vGetUnrealizedPips();
                sUnrealizedValue = string.Format("Unrealized Pips {0:N1}",dUnRealizedPips);
                GOUnrealizedPips.text = sUnrealizedValue;
            }catch(Exception ex){
                Debug.Log("LowerBarControl sUnrealizedValue "+ dUnRealizedPips);
            }
            try{
                string sPipsToday = string.Format("Pips Today: {0:N1}",MoneyManagement.dGetPipsToday());
                tPipsToday.text = sPipsToday;
            }catch(Exception ex){
                Debug.Log("LowerBarControl sOpenLotsValue "+ex);
            }
           try{
                string sOpenLotsValue = string.Format("{0:N2}",MoneyManagement.dGetOpenLots());
                GOOpenLots.text = sOpenLotsValue;
            }catch(Exception ex){
                Debug.Log("LowerBarControl sOpenLotsValue "+ex);
            }
            try{
                string sMaxLotsValue = string.Format("/{0:N2}",MoneyManagement.dGetMaxPosition());
                GOMaxLots.text = sMaxLotsValue;
            }catch(Exception ex){
                Debug.Log("LowerBarControl sMaxLotsValue "+ex);
            }

            try{
                double dPercentDailyProfit = 0.0;
                dPercentDailyProfit = MoneyManagement.dPercentDailyProfit();
                string sPercentProfitToday = string.Format("Daily Return: {0:N2}%",dPercentDailyProfit);
                //if(dCurrentBalance > 0.0) Debug.Log("sPercentProfitToday "+sPercentProfitToday);
                GOPercentProfit.text = sPercentProfitToday;
            }catch(Exception ex){
                Debug.Log("LowerBarControl sPercentProfitToday "+ex);
            }

            try{
                double dMarginBalance = MoneyManagement.dGetCurrentBalance();
                string sMargin = string.Format("Margin: {0:N2}",dMarginBalance);
                tMargin.text = sMargin;
            }catch(Exception ex){
                Debug.Log("LowerBarControl sMargin "+ex);
            }

            try{
                string sFreeMargin = string.Format("Free Margin: {0:N2}",dCurrentBalance);
                tFreeMargin.text = sFreeMargin;
            }catch(Exception ex){
                Debug.Log("LowerBarControl sFreeMargin "+ex);
            }


        }catch(Exception ex){
            Debug.Log("LowerBarControl vUpdateTime "+ex);
        }
    }



    static public double dGetBid(string sSymbol){
        double dBid = 0.0;
        try{
            for(int iQuoteIndex = 0; iQuoteIndex <lCurrentSymbolQuote.Count; iQuoteIndex++){
                if(sSymbol == lCurrentSymbolQuote[iQuoteIndex].Symbol){
                    dBid = lCurrentSymbolQuote[iQuoteIndex].Bid;
                    break;
                }
            }
        } catch{
            Debug.Log("Catch cQuote not available");
        }
        return dBid;
    }

    static public double dGetAsk(string sSymbol){
        double dAsk = 0.0;
        try{
            for(int iQuoteIndex = 0; iQuoteIndex <lCurrentSymbolQuote.Count; iQuoteIndex++){
                if(sSymbol == lCurrentSymbolQuote[iQuoteIndex].Symbol){
                    dAsk = lCurrentSymbolQuote[iQuoteIndex].Ask;
                   break;
                }
            }
        } catch{
            Debug.Log("Catch cQuote not available");
        }
        return dAsk;
    }

    public static void vSetActivateOrderHistoryTable(bool bValue){
        bActivateOrderHistoryTable = bValue;
    }

    public  static void vAddNewBar(Bar cBar, bool bLastBar, string sID){
        //Fix to correct chart later with sID
        //Assume receive in time order, the newest bar is at index 0 oldest at end

        //if()
        Bar cLocalBar1 = cBar;
        BarWithDT cLocalBar = new BarWithDT();
        cLocalBar.Close = cBar.Close;
        cLocalBar.Open = cBar.Open;
        cLocalBar.High = cBar.High;
        cLocalBar.Low = cBar.Low;
        cLocalBar.Volume = cBar.Volume;
        cLocalBar.Date = cBar.Date/1000;
        DateTime dtTime = dt1970.AddSeconds(cLocalBar.Date);
        cLocalBar.dtDate = dtTime;
        cLocalBar.Date = (long) cLocalBar.Date/1000;
        //Debug.Log("cLocalBar "+cLocalBar.Date);
        for(int iChartIndex = 0; iChartIndex < iNumberOfCharts;iChartIndex++){
            if(Charts[iChartIndex].sID == sID){
                int iCurrentListCount;
                try{
                    iCurrentListCount = lBars[iChartIndex].Count;
                    //Debug.Log("vAddNewBar iCurrentListCount "+iCurrentListCount);
                } catch{
                    lBars[iChartIndex] = new List<BarWithDT>();
                    iCurrentListCount = 0;
                    lBars[iChartIndex].Add(cLocalBar);
                    return;
                }
                bool bDuplicate = false;
                for(int i = 0; i < iCurrentListCount; i++){
                    if(lBars[iChartIndex][i].Date == cBar.Date){
                        bDuplicate = true;
                        break;
                    }
                }
                if(bDuplicate) return;
                //Debug.Log("vAddNewBar iCurrentListCount "+iCurrentListCount);
                try{
                    lBars[iChartIndex].Add(cLocalBar);
                    //Debug.Log("vAddNewBar add bar"+cLocalBar.Close+" "+lBars.Count);
                    lBars[iChartIndex].Sort((left, right) => left.dtDate.CompareTo(right.dtDate));
                } catch (Exception e){
                    Debug.Log("vAddNewBar exception1 "+e);
                }
                //if close to full loaded flag as available
                if(lBars[iChartIndex].Count > 99) {
                    //Debug.Log("TradingPlatform vAddNewBar iCurrentListCount "+iCurrentListCount);
                    Charts[iChartIndex].bNewBar = true;
                }
                lBars[iChartIndex].Sort((left, right) => right.dtDate.CompareTo(left.dtDate));
                //Debug.Log("TradingPlatform vAddNewBar bar 1 "+lBars[iChartIndex][1].dtDate+" "+lBars[iChartIndex][0].dtDate);
                Charts[iChartIndex].TimeOfNextBar = lBars[iChartIndex][0].dtDate.AddMinutes(Charts[iChartIndex].iTimeFrame);
            }
        }
    }

    public void vUpdateBarZero(Quote cQuote){
        // loop through all the active charts
        bool bNewMinute = false;
        DateTime dtNow =  DateTime.UtcNow;
        if(dtNow.Minute != iCurrentMinute){
            bNewMinute = true;
            //iCurrentMinute = dtNow.Minute;
            //Debug.Log("vUpdateBarZero "+dtNow);
        }

        for(int iChartIndex = 0; iChartIndex < iNumberOfCharts;iChartIndex++){
            //If the symbol on the chart is the same as quote symbol
            if(Charts[iChartIndex].sChartSymbol == cQuote.Symbol){
                //if new minute check to determine if new bar has formed
                if(bNewMinute){
                    bool bNewBar = false;
                    int iTF = Charts[iChartIndex].iTimeFrame;
                    if(iTF == 1)bNewBar = true;
                    if(iTF < 60){
                        if((dtNow.Minute % iTF) == 0)bNewBar = true;
                    } else{
                        if(dtNow.Minute == 0){
                            if(iTF == 60){
                                bNewBar = true;
                            }else{
                                //Convert time into minutes of year
                                int iMinutes = dtNow.Month *30*1440 + dtNow.Day * 1440 + dtNow.Hour*60 + dtNow.Minute;
                                if(iMinutes %iTF == 0) bNewBar = true;
                            }
                        }
                    }
                    //if(bNewMinute) Debug.Log("vUpdateBarZero bNewBar"+bNewBar);
                    if(bNewBar){
                        //New bar
                        //Debug.Log("Make new bar "+dtNow);
                        DateTime dtThisBar = new DateTime(dtNow.Year,dtNow.Month,dtNow.Day,dtNow.Hour,dtNow.Minute,0);
                        long unixTimestamp = (Int32)(dtThisBar.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                        BarWithDT cNewBar = new BarWithDT();
                        if(cQuote.Bid == 0.0) {
                            //Debug.Log("cQuote.Bid "+cQuote.Bid +" Ask "+cQuote.Ask);
                            cNewBar.Open = lBars[iChartIndex][0].Close;
                            cNewBar.Close = lBars[iChartIndex][0].Close;
                            cNewBar.Volume = 0;
                            cNewBar.High = lBars[iChartIndex][0].Close;
                            cNewBar.Low = lBars[iChartIndex][0].Close;
                            cNewBar.Date = unixTimestamp;
                            cNewBar.dtDate = dtThisBar;
                        } else{
                            cNewBar.Open = (float)cQuote.Bid;
                            cNewBar.Close = (float)cQuote.Bid;
                            cNewBar.Volume = (float)cQuote.Volume;
                            cNewBar.High = (float)cQuote.Bid;
                            cNewBar.Low = (float)cQuote.Bid;
                            cNewBar.Date = unixTimestamp;
                            cNewBar.dtDate = dtThisBar;
                        }
                        Charts[iChartIndex].bNewBar = true;

                        try{
                            lBars[iChartIndex].Insert(0,cNewBar);
                            int iSecondsDifferenceBar0Bar1 = (int)(lBars[iChartIndex][0].Date - lBars[iChartIndex][1].Date);
                            //Debug.Log("TradingPlatform vUpdateBarZero Seconds bar 1 "+iSecondsDifferenceBar0Bar1);
                        }catch(Exception ex){
                            Debug.Log("TradingPlatform vUpdateBarZero iChartIndex "+iChartIndex+" Exception "+ex);
                        }
                        //Flag as new bar so all bars are replotted and set timetag of next bar for countdown clock
                        Charts[iChartIndex].bNewBar = true;
                        Charts[iChartIndex].TimeOfNextBar = dtThisBar.AddMinutes(iTF);
                   }
                }
                //If not new bar then update bar zero
                if(!Charts[iChartIndex].bNewBar){
                    try{
                        BarWithDT cBarZero = lBars[iChartIndex][0];
                        //Update the close, volume and check for new high or low for this bar
                        cBarZero.Close = (float)cQuote.Bid;
                        cBarZero.Volume += (float)cQuote.Volume;
                        if(cQuote.Bid > 0.0){
                            if(cQuote.Bid > cBarZero.High) cBarZero.High = (float)cQuote.Bid;
                            if(cQuote.Bid < cBarZero.Low) cBarZero.Low = (float)cQuote.Bid;
                            lBars[iChartIndex][0] = cBarZero;
                        }
                    //Flag that the bar zero has been updated so chart is updated
                        Charts[iChartIndex].bNewQuote = true;
                    } catch (Exception ex){
                        Debug.Log("Catch cQuote not available "+ex);
                    }
                }
            }
         }
    }
    private int vSetChartTimeframe(string sTF){
        string sTimeFrame = sTF;
        int iTF = 1;
        switch (sTF)
        {
            case "M1":
                iTF = 1;
                break;
            case "M5":
                iTF = 5;
                break;
            case "M15":
                iTF = 15;
                break;
            case "M30":
                iTF = 30;
                break;
            case "H1":
                iTF = 60;
                break;
            case "H2":
                iTF = 120;
                break;
            case "H4":
                iTF = 480;
                break;
            case "D1":
                iTF = 1440;
                break;
            case "D7":
                iTF = 10080; //60*24*7
                break;
            case "MN":
                iTF = 43200; // 60*24*30
                break;
        }
        //Debug.Log("vSetChartTimeframe sTimeFrame "+sTimeFrame +" iTF "+iTF);
        return iTF;
    }
    public string sGetUserName(){
        return sUserName;
    }
    public void vOpenNewChart(string sChartSymbol, string sChartTimeframe, int iRequestedBars){

        SingleChart NewChart =  new SingleChart( );

        //NewOrderButton.SetActive(true);
        //Initialize the historic bar list
        NewChart.vInitBarList();

        //Creates a new entry for access to a specific chart
        lCharts.Add(NewChart);

        //The chart initial parameters
        NewChart.vSetSymbol(sChartSymbol);
        NewChart.vSetChartTimeframe(sChartTimeframe);
        string sID = NewChart.sGetChartID();
        Instrument lInstrument = vFindIntrument(sChartSymbol);
        aChart thisChart = new aChart(NewChart, sChartSymbol,sChartTimeframe, sID, lInstrument);
        int iTF = vSetChartTimeframe(sChartTimeframe);
        thisChart.iTimeFrame = iTF;
        Charts[iNumberOfCharts] = thisChart;
        iNumberOfCharts++;
       if(iNumberOfCharts > iMaxCharts) {
            iNumberOfCharts--;
            Debug.Log("Reached Maximium charts 30");
        }
        //Debug.Log("vOpenNewChart iNumberOfBars  "+iRequestedBars);
        //Request the bar history
        if(iRequestedBars > 0){
           Processor.vSendHistoryBarRequest(sID,sChartSymbol,sChartTimeframe ,iRequestedBars);
        } else{
            Debug.Log("vOpenNewChart Error Zero Bars requested ");
        }
    }
  
    static public void Logger(EventLevel eErrorLevel, string sClassName, string sFunctionName, string sErrorType, string sError)
    {
            //string sMethodName = "Logger";
        try
        {
            int iErrorLevel = (int)eErrorLevel;
            int iMinLevel = (int)eMinLevel;
            //Console.WriteLine("Logger iErrorLevel {0} iMinLevel {1}", iErrorLevel, iMinLevel);
            if (iErrorLevel <= iMinLevel)
            {
                string sSendRequest = string.Format("{0};{1};{2};{3};{4};{5}", sProgramName, iErrorLevel, sClassName, sFunctionName, sErrorType, sError);
                Debug.Log( DateTime.Now+" "+ sProgramName+" "+ iErrorLevel+" "+ sClassName+" "+ sFunctionName+" "+ sErrorType+" "+ sError);
                /*
                if (iErrorLevel >= 4)
                {
                    //log debug and above to console to save the number of interactions to the sql 
                        Console.WriteLine("{0} {1} {2} {3} {5} {4} {6}", DateTime.Now, sProgramName, iErrorLevel, sClassName, sFunctionName, sErrorType, sError);
                }
                */
            }
        }
        catch (Exception e)
        {
            string sMessage = string.Format("MetastreetServerClient Logger {0}", e);
            Debug.Log(sMessage);
        }
    }


    private void vUpdateChartTime(int iChartIndex){
        DateTime dtNextBar = Charts[iChartIndex].TimeOfNextBar;
        DateTime dtNow = DateTime.UtcNow;
        TimeSpan interval = dtNextBar - dtNow;
        
        string sYValue = "Next Bar: " + interval.ToString(@"dd\.hh\:mm\:ss");
        //Place Time to next bar countdown
        double xPosition = 0.25 * graphWidth;
        double yPosition = 1.02 * graphHeight;
        Destroy(gameObjectTimeTillBar);
        if(yMax > 0){
            RectTransform TimeToNextBarLabel = Instantiate(labelTimeToNextBar);
            TimeToNextBarLabel.SetParent(graphContainer,false);
            TimeToNextBarLabel.gameObject.SetActive(true);
            TimeToNextBarLabel.anchoredPosition = new Vector2((float)xPosition, (float)yPosition);
            TimeToNextBarLabel.GetComponent<Text>().text = sYValue;
            gameObjectTimeTillBar = TimeToNextBarLabel.gameObject;
        }
    }
    private void ShowGraph(int iChartIndex,int xMaxVisibleValues)
    {
        graphContainer = transform.Find("graphContainer").GetComponent<RectTransform>();
        int iMaxVisibleValues = xMaxVisibleValues;
        //if(Charts[iChartIndex].bNewBar) {

            //remove all of the graph game objects include bar zero
            foreach(GameObject gameObject in gameObjectList){
                Destroy(gameObject);
            }
            gameObjectList.Clear();

            //foreach(GameObject gameObject in barZerogameObjectList){
            //    Destroy(gameObject);
            //}
            //barZerogameObjectList.Clear();
        

            if(iMaxVisibleValues <= 0){
                try{
                iMaxVisibleValues = lBars[iChartIndex].Count;
                }catch{
                    return;
                }
            }
            //Get how tall and wide the graphic area is
            graphHeight = graphContainer.sizeDelta.y;
            graphWidth = graphContainer.sizeDelta.x;
            //Determine the y min and y max
            yMax = lBars[iChartIndex][0].High;
            yMin = lBars[iChartIndex][0].Low;
            int iEndPoint = lBars[iChartIndex].Count;
            if(iEndPoint > iMaxVisibleValues ) iEndPoint = iMaxVisibleValues;
            DateTime dtBarZero = lBars[iChartIndex][0].dtDate;
            DateTime dtThisBar = dtBarZero;
            double fMinute = 0.0;
            xMin = fMinute;
            xMax = fMinute;
            for(int i= 0; i<iEndPoint; i++){
                if(lBars[iChartIndex][i].High > yMax) {
                    yMax = lBars[iChartIndex][i].High;
                }
                if(lBars[iChartIndex][i].Low < yMin){
                    yMin = lBars[iChartIndex][i].Low;
                }
                dtThisBar = lBars[iChartIndex][i].dtDate;
                fMinute = (float)dtThisBar.Subtract(dtBarZero).TotalMinutes; 
                if(fMinute < xMin)xMin = fMinute;
                if(fMinute > xMax)xMax = fMinute;
            }
            //xMin -= 5.0f;
            xMax += 5.0f;
            //yMax = yMax + ((yMax - yMin) * 0.1f);
            //yMin = yMin - ((yMax - yMin) * 0.1f);
            yMax = yMax + 0.0001f;
            yMin = yMin - .0001f;
            yRange = yMax - yMin;
            xRange = xMax - xMin;

            //Place symbol and current close
            double xPosition = 0.02 * graphWidth;
            double yPosition = 1.02 * graphHeight;
            RectTransform CurrentProceLabel = Instantiate(labelChartCurrentClosePrice);
            CurrentProceLabel.SetParent(graphContainer,false);
            CurrentProceLabel.gameObject.SetActive(true);
            //float yPostion = normalizedValue * graphHeight;
            CurrentProceLabel.anchoredPosition = new Vector2((float)xPosition, (float)yPosition);
            string sYValue = Charts[iChartIndex].sChartSymbol;
            double dBid = dGetBid(Charts[iChartIndex].sChartSymbol);
            double dAsk = dGetAsk(Charts[iChartIndex].sChartSymbol);            
            string sValue2 = string.Format(" Bid = {0:N5} Ask = {1:N5}",dBid, dAsk);
            //Debug.Log("ShowGraph sValue2 "+sValue2);
            sYValue += sValue2;
            CurrentProceLabel.GetComponent<Text>().text = sYValue;
            gameObjectList.Add(CurrentProceLabel.gameObject);

            MarkAxis(lBars[iChartIndex],15,dtBarZero,dBid,Charts[iChartIndex].sChartSymbol);
            //int iIndex = 0;
            // Set the distance between each point along the x axis 
            double xSize = graphWidth; ///(xMaxVisibleValues+1);

            List<GameObject> HoldGameObjects = new List<GameObject>();    
            //Cycle through all visable data points
            for(int i= 0; i<iEndPoint; i++){
                //Add data point visual
                //string tooltipText = "Here I am";
                //tooltipText = getAxisLabelY(valueList[i]);
                //tooltipText = valueList[i].ToString();
                BarWithDT cBar = lBars[iChartIndex][i];

                IGraphVisual graphVisual = new BarChartVisual(graphContainer, Color.green, 0.3f);
                HoldGameObjects.AddRange(graphVisual.AddGraphVisual(cBar, dtBarZero));
                gameObjectList.AddRange(HoldGameObjects);
                if(i==0){
                    barZerogameObjectList.AddRange(HoldGameObjects);
                }
                HoldGameObjects.Clear();
            }
    }
    
    private void MarkAxis( List<BarWithDT> valueList, int iNumberLabels, DateTime dtBarZero,double dBid,string sSymbol){
        //Create times on X-Axis labels
        double xStep = xRange/iNumberLabels;
        for(int i= 0; i<iNumberLabels; i++){
            double normalizedValue = (float)i / iNumberLabels;
            double xPosition = (i/xMax) * graphWidth;
            //Debug.Log("MarkAxis graphWidth "+graphWidth+ " xRange "+xRange);
            RectTransform xAxisLabel = Instantiate(labelTemplateXAxis);
            xAxisLabel.SetParent(graphContainer,false);
            xAxisLabel.gameObject.SetActive(true);
            xAxisLabel.anchoredPosition = new Vector2((float)(normalizedValue*graphWidth), -20f);
            double fxValue = (xMin + i*xStep);
            DateTime dtTime = dtBarZero.AddMinutes(fxValue);
            //DateTime dtTime = dt1970.AddSeconds(fxValue);
            string sTime = string.Format("{0:00}/{1:00}/{2}\n{3:00}:{4:00}",dtTime.Month,dtTime.Day,dtTime.Year,dtTime.Hour, dtTime.Minute);

            xAxisLabel.GetComponent<Text>().text = sTime;
            gameObjectList.Add(xAxisLabel.gameObject);
            //Create virtical x-axis Line
            Vector2 LineStartGameObject = new Vector2((float)(normalizedValue*graphWidth), -20f);
            Vector2 LineEndGameObject = new Vector2((float)(normalizedValue*graphWidth), (float)graphHeight);
            CreateLine(LineStartGameObject, LineEndGameObject,0);
        }

        //Create price on Y_Axis labels
        for(int iIndex = 0; iIndex <= iNumberLabels; iIndex++){
            RectTransform yAxisLabel = Instantiate(labelTemplateYAxis);
            yAxisLabel.SetParent(graphContainer,false);
            yAxisLabel.gameObject.SetActive(true);
            float normalizedValue = (float)iIndex / iNumberLabels;
            //float yPostion = normalizedValue * graphHeight;
            yAxisLabel.anchoredPosition = new Vector2(0f, (float)(normalizedValue * graphHeight));
            string sYValue = "";
            sYValue = string.Format("{0:N5}", (yMin + (normalizedValue*(yMax - yMin))));
            yAxisLabel.GetComponent<Text>().text = sYValue;
            gameObjectList.Add(yAxisLabel.gameObject);
                    //Create X-Axis Line
            Vector2 LineStartGameObject = new Vector2(0f, (float)(normalizedValue*graphHeight));
            Vector2 LineEndGameObject = new Vector2((float)graphWidth, (float)(normalizedValue*graphHeight));
            CreateLine(LineStartGameObject, LineEndGameObject,0);
                
        }
        //Place the current bid price on the chart
        RectTransform yAxisLabel2 = Instantiate(labelTemplateYAxis);
        yAxisLabel2.SetParent(graphContainer,false);
        yAxisLabel2.gameObject.SetActive(true);
        double normalizedValue2 = (dBid-yMin)/(yMax-yMin);
        yAxisLabel2.anchoredPosition = new Vector2(0f, (float)(normalizedValue2 * graphHeight));
        string sYValue2 = "";
        sYValue2 = string.Format("{0:N5}", dBid);
        yAxisLabel2.GetComponent<Text>().text = sYValue2;
        yAxisLabel2.GetComponent<Text>().color = new Color(1,0.5f,0.0f, .9f);
        gameObjectList.Add(yAxisLabel2.gameObject);
        //Create X-Axis Line
        Vector2 LineStartGameObject2 = new Vector2((float)graphWidth*.99f, (float)(normalizedValue2*graphHeight));
        Vector2 LineEndGameObject2 = new Vector2((float)graphWidth, (float)(normalizedValue2*graphHeight));
        CreateLine(LineStartGameObject2, LineEndGameObject2,1);

        //Add open orders open price, take profit if set, stop loss if set, order number
        if(lOpenOrders.Count > 0){
                foreach (var cOrder in lOpenOrders){
                    if(cOrder.Symbol == sSymbol){
                        double dOpenPrice = cOrder.OpenPrice;
                        //If pending the open price is zero
                        if(dOpenPrice == 0.0) dOpenPrice = cOrder.Price;
                        double dTakeProfit = cOrder.TakeProfit;
                        double dStopLoss = cOrder.StopLoss;
                        int iOID = (int)cOrder.Id;
                        //show open price line
                        if(dOpenPrice > yMin && dOpenPrice < yMax){
                            RectTransform yOpenOrderLabel = Instantiate(LabelTemplateOrder);
                            yOpenOrderLabel.SetParent(graphContainer,false);
                            yOpenOrderLabel.gameObject.SetActive(true);
                            double OpenValue = (dOpenPrice-yMin)/(yMax-yMin);
                            //Apply a 20 pixel y offset to raise label above line
                            yOpenOrderLabel.anchoredPosition = new Vector2(0.0f, (float)((OpenValue * graphHeight)+20f));
                            string sYOpen = "";
                            if((int)cOrder.Side == 0){
                                sYOpen = string.Format("Order {0} - Buy @ {1:N5}",iOID, dOpenPrice);
                            } else{
                                sYOpen = string.Format("Order {0} - Sell @ {1:N5}",iOID, dOpenPrice);
                            }
                            yOpenOrderLabel.GetComponent<Text>().text = sYOpen;
                            yOpenOrderLabel.GetComponent<Text>().color = new Color(0,1f,0f, .9f);
                            gameObjectList.Add(yOpenOrderLabel.gameObject);
                            //Create X-Axis Line
                            Vector2 LineStartOpen = new Vector2((float)0, (float)(OpenValue*graphHeight));
                            Vector2 LineEndOpen = new Vector2((float)graphWidth, (float)(OpenValue*graphHeight));
                            CreateLine(LineStartOpen, LineEndOpen,2);
                        }
                        if(dTakeProfit> 0.0 && dTakeProfit > yMin && dTakeProfit < yMax){
                            //show TakeProfit price line
                            RectTransform yTPOrderLabel = Instantiate(LabelTemplateOrder);
                            yTPOrderLabel.SetParent(graphContainer,false);
                            yTPOrderLabel.gameObject.SetActive(true);
                            double TPValue = (dTakeProfit-yMin)/(yMax-yMin);
                            //Apply a 20 pixel y offset to raise label above line
                            yTPOrderLabel.anchoredPosition = new Vector2(0.0f, (float)((TPValue * graphHeight)+20f));
                            string sYTP = "";
                            sYTP = string.Format("Order {0} - TP @ {1:N5}",iOID, dTakeProfit);
                            yTPOrderLabel.GetComponent<Text>().text = sYTP;
                            yTPOrderLabel.GetComponent<Text>().color = new Color(0,1f,0f, .9f);
                            gameObjectList.Add(yTPOrderLabel.gameObject);
                            //Create X-Axis Line
                            Vector2 LineStartTP = new Vector2((float)0, (float)(TPValue*graphHeight));
                            Vector2 LineEndTP = new Vector2((float)graphWidth, (float)(TPValue*graphHeight));
                            CreateLine(LineStartTP, LineEndTP,2); 
                        }

                        if(dStopLoss> 0.0 && dStopLoss > yMin && dStopLoss < yMax){
                            //show dStopLoss price line
                            RectTransform ySLOrderLabel = Instantiate(LabelTemplateOrder);
                            ySLOrderLabel.SetParent(graphContainer,false);
                            ySLOrderLabel.gameObject.SetActive(true);
                            double SLValue = (dStopLoss-yMin)/(yMax-yMin);
                            //Apply a 20 pixel y offset to raise label above line
                            ySLOrderLabel.anchoredPosition = new Vector2(0.0f, (float)((SLValue * graphHeight)+20f));
                            string sYSL = "";
                            sYSL = string.Format("Order {0} - SL @ {1:N5}",iOID, dStopLoss);
                            ySLOrderLabel.GetComponent<Text>().text = sYSL;
                            ySLOrderLabel.GetComponent<Text>().color = new Color(0,1f,0f, .9f);
                            gameObjectList.Add(ySLOrderLabel.gameObject);
                            //Create X-Axis Line
                            Vector2 LineStartSL = new Vector2((float)0, (float)(SLValue*graphHeight));
                            Vector2 LineEndSL = new Vector2((float)graphWidth, (float)(SLValue*graphHeight));
                            CreateLine(LineStartSL, LineEndSL,2); 
                        }  
                    }                                    

            }
        }


    }

    private void CreateLine(Vector2 dotPositionA, Vector2 dotPositionB,int iLabel) {
        //iLabel = 0 Grid lines, =1 Bid Price, =2 Open Orders
        GameObject gameObject = new GameObject("Line", typeof(Image));
        gameObjectList.Add(gameObject.gameObject);

        gameObject.transform.SetParent(graphContainer, false);
        gameObject.GetComponent<Image>().color = new Color(1,1,1, .5f);
        if(iLabel == 1) gameObject.GetComponent<Image>().color = new Color(1,0.5f,0.0f, .9f);
        if(iLabel == 2) gameObject.GetComponent<Image>().color = new Color(0,1.0f,0.0f, .9f);
       RectTransform rectTransform = gameObject.GetComponent<RectTransform>();
        Vector2 dir = (dotPositionB - dotPositionA).normalized;
        float distance = Vector2.Distance(dotPositionA, dotPositionB);
        rectTransform.anchorMin = new Vector2(0, 0);
        rectTransform.anchorMax = new Vector2(0, 0);
        rectTransform.sizeDelta = new Vector2(distance, 3f);
        rectTransform.anchoredPosition = dotPositionA + dir * distance * .5f;
        rectTransform.localEulerAngles = new Vector3(0, 0, UtilsClass.GetAngleFromVectorFloat(dir));
    }


    private interface IGraphVisual{
        //List<GameObject> AddGraphVisual(Bar cBar, string tooltipText);
        List<GameObject> AddGraphVisual(BarWithDT cBar, DateTime dtBarZero);
    }

    public  class BarChartVisual : IGraphVisual {

        private RectTransform graphContainer;
        Color barColor; 
        float barWidthMultiplier;

        public BarChartVisual(RectTransform graphContainer, Color barColor, float barWidthMultiplier){
            this.graphContainer = graphContainer;
            this.barColor = barColor;
            this.barWidthMultiplier = barWidthMultiplier;

        }
        //public List<GameObject> AddGraphVisual(Bar cBar, string tooltipText){
        public List<GameObject> AddGraphVisual(BarWithDT cBar,DateTime dtBarZero){
            GameObject barGameObject1 = CreateHighLowBar(cBar, dtBarZero);
            GameObject barGameObject = CreateBarOpenClose(cBar, dtBarZero);
            //barGameObject.AddComponent<Button_UI>().MouseOverOnceFunc += () =>{
            //     ShowTooltip_Static(tooltipText, graphPosition);
            //};
            return new List<GameObject> {barGameObject,barGameObject1};
        }


        public  GameObject CreateHighLowBar(BarWithDT cBar,DateTime dtBarZero)
        {
            bool bBarUp = false;
            float fFirstPoint = cBar.Close;
            float fSecondPoint = cBar.Open;
            if(cBar.Close > cBar.Open) {
                bBarUp = true;
            }
            DateTime dtThisBar = cBar.dtDate;
            //DateTime dt2000 = new DateTime(2000,01,01,00,00,00);
            var  dMinute= dtThisBar.Subtract(dtBarZero).TotalMinutes; 
            float fMinute = (float)dMinute;

            double normalizedXValue = (fMinute - xMin) /xRange;
            double xPosition = normalizedXValue * graphWidth + 1;
            //Debug.Log("(float)cBar.Date "+cBar.Date/60.0f+ " ((float)cBar.Date/60.0f - xMin)" +((float)cBar.Date/60.0f - xMin)+" xPosition "+xPosition);
            //if(bBarUp) gameObject.GetComponent<Image>().color = Color.green; 

            //For trading parts use open to close or low to high for anchor and sizeDelta
            //For High Low bar reduce the barWidth so is narrower
            //Set the anchor at the bottom of the graph
            //xPosition is current time normalisted between the max and min
            //form the line for Open to Close
            double normalizedyOpenValue;
            double yPositionOpen;
            double fNormalizedDeltaValue;
            double fInScaleDeltaValue;
            if(bBarUp){
                normalizedyOpenValue = (cBar.Open - yMin) /yRange;
                yPositionOpen = normalizedyOpenValue * graphHeight;
                fNormalizedDeltaValue = (cBar.Close-cBar.Open) /yRange;
                fInScaleDeltaValue = fNormalizedDeltaValue * graphHeight; 
            } else{
                normalizedyOpenValue = (cBar.Close - yMin) /yRange;
                yPositionOpen = normalizedyOpenValue * graphHeight;
                fNormalizedDeltaValue = (cBar.Open-cBar.Close) /yRange;
                fInScaleDeltaValue = fNormalizedDeltaValue * graphHeight; 
            } 

            //form the line for low to high this is a second component
            GameObject gameObject = new GameObject("HighLowBar", typeof(Image));
            gameObject.transform.SetParent(graphContainer,false);
            gameObject.GetComponent<Image>().color = Color.red; //barColor;
            if(bBarUp) gameObject.GetComponent<Image>().color = Color.green; 
            //normalizedXValue = ((float)cBar.Date/60.0f - xMin) /xRange;
            //xPosition = normalizedXValue * graphWidth+2;
            if(bBarUp) gameObject.GetComponent<Image>().color = Color.green;
            RectTransform rectTransform = gameObject.GetComponent<RectTransform>();
            double normalizedyLowValue = (cBar.Low - yMin) /yRange;
            double yPositionLow = normalizedyLowValue * graphHeight;
            fNormalizedDeltaValue = (cBar.High-cBar.Low) /yRange;
            fInScaleDeltaValue = fNormalizedDeltaValue * graphHeight; 

            // the starting point of the line is the time position,bar low
            rectTransform.anchoredPosition = new Vector2((float)xPosition,(float)yPositionLow);
            //draw from the now draw a line with x being how wide the bar and hight to the bar high price
            rectTransform.sizeDelta = new Vector2(1,(float)fInScaleDeltaValue);
            rectTransform.anchorMin = new Vector2(0,0);
            rectTransform.anchorMax = new Vector2(0,0);
            rectTransform.pivot = new Vector2(.0f,0f);
 
            return gameObject;
        }

        public  GameObject CreateBarOpenClose(BarWithDT cBar, DateTime dtBarZero)
        {
            //The object at the graph point
            GameObject gameObject = new GameObject("OpenCloseBar", typeof(Image));
            gameObject.transform.SetParent(graphContainer,false);
            gameObject.GetComponent<Image>().color = Color.red; //barColor;
            RectTransform rectTransform = gameObject.GetComponent<RectTransform>();
            bool bBarUp = false;
            float fFirstPoint = cBar.Close;
            float fSecondPoint = cBar.Open;
            if(cBar.Close > cBar.Open) {
                bBarUp = true;
            }

            DateTime dtThisBar = cBar.dtDate;
            var  dMinute= dtThisBar.Subtract(dtBarZero).TotalMinutes;
            //Debug.Log("CreateBarOpenClose dtThisBar "+dtThisBar+" dt2000 "+dt2000 + " dMinute "+dMinute +" xMin "+xMin+" xRange "+xRange);
            double fMinute = (float)dMinute;
            double normalizedXValue = (fMinute - xMin) /xRange;
            double xPosition = normalizedXValue * graphWidth;
            //if(xPosition >= 1500) Debug.Log("(float)cBar.Date "+cBar.Date/60.0f+" Date- xMin "+(cBar.Date/60.0f - xMin)+ " xMax "+ xMax+" xPosition "+xPosition);
            if(bBarUp) gameObject.GetComponent<Image>().color = Color.green; 

            //For trading parts use open to close or low to high for anchor and sizeDelta
            //Set the anchor at the bottom of the graph
            //xPosition is current time normalisted between the max and min

            //form the line for Open to Close 
            double normalizedyOpenValue;
            double yPositionOpen;
            double fNormalizedDeltaValue;
            double fInScaleDeltaValue;
            if(bBarUp){
                normalizedyOpenValue = (cBar.Open - yMin) /yRange;
                yPositionOpen = normalizedyOpenValue * graphHeight;
                fNormalizedDeltaValue = (cBar.Close-cBar.Open) /yRange;
                fInScaleDeltaValue = fNormalizedDeltaValue * graphHeight; 
            } else{
                normalizedyOpenValue = (cBar.Close - yMin) /yRange;
                yPositionOpen = normalizedyOpenValue * graphHeight;
                fNormalizedDeltaValue = (cBar.Open-cBar.Close) /yRange;
                fInScaleDeltaValue = fNormalizedDeltaValue * graphHeight; 
            } 
           // the starting point of the line is the time position,bar low
            rectTransform.anchoredPosition = new Vector2((float)xPosition,(float)yPositionOpen);
            //draw from the now draw a line with x being how wide the bar and hight to the bar high price 
            rectTransform.sizeDelta = new Vector2(3,(float)fInScaleDeltaValue);
            rectTransform.anchorMin = new Vector2(0,0);
            rectTransform.anchorMax = new Vector2(0,0);
            rectTransform.pivot = new Vector2(0f,0f);
 
            return gameObject;

        }  
    }

}

