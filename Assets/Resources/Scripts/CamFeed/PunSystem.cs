using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

//used to decode send cam or Main camera to other clients
public class PunSystem : MonoBehaviour
{
    public PhotonView M_PhotonView; //interact between client and photon server
    public GameViewDecoder gameViewDecoder; // used to convert byte array to screen
    public GameObject LocalName;
    public string localName;
    //public GameObject Track;
    //public Toggle WebCamToggle;
    void Start()
    {
        localName = PlayerPrefs.GetString("UserName");
        //LocalName = localName.ToString();
        Debug.Log("My Username = "+localName);
    }
    public void SendMessage(byte[] _byteData, string message)
    {
            M_PhotonView.RPC("RPC_SendMessage", RpcTarget.Others, _byteData, message);
    }
/*
    public void WebCam()
    {
        if(WebCamToggle.isOn == true) 
        {
            Track.SetActive(false);
        }
        if(WebCamToggle.isOn == false) 
        {
            Track.SetActive(true);
        }
    }
    */
    [PunRPC]
    private void RPC_SendMessage(byte[] _byteData, string message)
    {
        bool bContainUser = false;
        if(message.Contains(localName)) bContainUser = true;
        if(!bContainUser)
        {
            if(message.Contains("VideoShare"))
            {
                gameViewDecoder.Action_ProcessImageData(_byteData);
            }
        }
    }



}
