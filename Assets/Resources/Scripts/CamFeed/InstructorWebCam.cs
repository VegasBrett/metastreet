using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstructorWebCam : MonoBehaviour
{
   [SerializeField]
    private UnityEngine.UI.RawImage rawImage;

     private void Start() {
        WebCamDevice[] devices = WebCamTexture.devices;

        for(int i = 0; i<devices.Length; i++)
        {
            print("Webcams available " + devices[i].name);
        }

        WebCamTexture tex = new WebCamTexture(devices[0].name);

        RawImage m_RawImage;
        m_RawImage = GetComponent<RawImage>();
        m_RawImage.texture = tex;
        tex.Play();
    }

}
