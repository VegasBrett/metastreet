//using System.Security.Cryptography;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class MyFPSController : MonoBehaviour
{
    //float speed = 5.0f;
    float xRotRate = 2f;
    float yRotRate = 2f;
    float MinX = -85f;
    float MaxX = 90.0f;

    Rigidbody RB;
    CapsuleCollider capsule;

    public GameObject cam;
    //public GameObject MiniMapCam;
    //private Vector3 offset;            //Private variable to store the offset distance between the player and camera

    Quaternion cameraRot;
    Quaternion characterRot;
    public Animator animator;

    bool bCursonIsLocked = true;
    bool bLockCursor = true; 

    // Start is called before the first frame update
    void Start()
    {
        RB = this.GetComponent<Rigidbody>();
        capsule = this.GetComponent<CapsuleCollider>();

        cameraRot = cam.transform.localRotation;
        characterRot = this.transform.localRotation;
        cameraRot *= Quaternion.Euler(0,0,0);
        cameraRot = ClampRotationAroundXAxis(cameraRot);
        cam.transform.localRotation = cameraRot;

        //MiniMapCam = GameObject.Find("PlayerMinimapCam");
        //offset = MiniMapCam.transform.position - transform.position;

 }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.UpArrow)){
            animator.SetBool("IsWalking",true);
           // Debug.Log("UpArrow Pressed "+ animator.GetBool("IsWalking"));
        }
        if(Input.GetKeyUp(KeyCode.UpArrow)){
            animator.SetBool("IsWalking",false);
            //Debug.Log("UpArrow released "+ animator.GetBool("IsWalking"));
        } 
        if(Input.GetKeyDown(KeyCode.Escape)){
            ExitGame();
        }
        
    }

    void ExitGame(){
        Application.Quit();
    }
    private void FixedUpdate() {
        //Turn eyes left or right with the motion of the mouse
        float yRot = Input.GetAxis("Mouse X") * xRotRate;
        //Turn head up or down with mouse up down
        float xRot = Input.GetAxis("Mouse Y") * yRotRate;
        cameraRot *= Quaternion.Euler(-xRot,0,0);
        characterRot *= Quaternion.Euler(0,yRot,0);

        cameraRot = ClampRotationAroundXAxis(cameraRot);
        // apply the rotations
        this.transform.localRotation = characterRot;
        cam.transform.localRotation = cameraRot;

    /*
        //Jump if the spacebar is down and on the ground
        if(Input.GetKeyDown(KeyCode.Space) &&bIsGrounded()){
            RB.AddForce(0,300,0);
        }
        if(Input.GetKeyDown(KeyCode.UpArrow)){
            animator.SetBool("IsWalking",true);
           // Debug.Log("UpArrow Pressed "+ animator.GetBool("IsWalking"));
        }
        if(Input.GetKeyUp(KeyCode.UpArrow)){
            animator.SetBool("IsWalking",false);
            //Debug.Log("UpArrow released "+ animator.GetBool("IsWalking"));
        } 

        if(Input.GetKeyDown(KeyCode.U)){
             animator.SetBool("UpStairs",true);
             animator.SetBool("IsWalking",true);
       }
        if(Input.GetKeyUp(KeyCode.U)) {
            animator.SetBool("IsWalking",false);
            animator.SetBool("UpStairs",false);
        }
       // move forward with up/down arrow keys
        float x = Input.GetAxis("Horizontal") * speed*Time.deltaTime;
        // move left or right with left right keys
        float z = Input.GetAxis("Vertical") *speed*Time.deltaTime;
        //Perform actual movement control
        transform.position += cam.transform.forward* z + cam.transform.right * x; //new Vector3(x,0,z);
        */
       // MiniMapCam.transform.position = transform.position + offset;
        //UpdateCursorLock();
 
    }

    Quaternion ClampRotationAroundXAxis(Quaternion q){
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);
        angleX = Mathf.Clamp(angleX,MinX,MaxX);
        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

        return q;
    }

    bool bIsGrounded(){
        RaycastHit hitInfo;
        if(Physics.SphereCast(transform.position,capsule.radius,Vector3.down, out hitInfo,
            (capsule.height/2f) - capsule.radius + 0.1f ))
            {
                return true;
            }
            return false;

    }

    public void SetCursorLock(bool bValue){

        bLockCursor = bValue;
        if(!bLockCursor)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public void UpdateCursorLock()
    {
       if(bLockCursor) {
           InternalLockUpdate();
       }
    }
    public void InternalLockUpdate()
    {
        if(Input.GetKeyUp(KeyCode.Escape))
            bCursonIsLocked = false;
        else if (Input.GetMouseButtonUp(0))
            bCursonIsLocked = true;
        
        if(bCursonIsLocked)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else if (!bCursonIsLocked)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public override bool Equals(object obj)
    {
        return obj is MyFPSController controller &&
               base.Equals(obj) &&
               EqualityComparer<Animator>.Default.Equals(animator, controller.animator);
    }
}