﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Vehicle : MonoBehaviour
{
    public List<Transform> waypoints = new List<Transform>();
    private Transform targetWaypoint;
    private int currentWaypointIndex = 0;
    private float minDistance = 0.4f; // close enough to the waypoint distance
    private int lastWaypointIndex; //
    public float movementSpeed = 50.0f;
    private float rotationSpeed = 2.0f;
    private float StationWaitTimer = 0f;
    private float allowedWaitTime = 8.0f;



    // Start is called before the first frame update
    void Start()
    {
        targetWaypoint = waypoints[currentWaypointIndex];
        StationWaitTimer = 0;
  }

    // Update is called once per frame
    void Update()
    {
        if(StationWaitTimer > 0){
            StationWaitTimer -= Time.deltaTime;
            return;
        }
        float localMovementSpeed = movementSpeed;
        float fDistanceRemaining = CheckDistanceToWaypoint();
        if(fDistanceRemaining <= 70) localMovementSpeed *= .8f;
        if(fDistanceRemaining <= 40) localMovementSpeed *= .6f;
        if(fDistanceRemaining <= 20) localMovementSpeed *= .4f;
        if(fDistanceRemaining <= 2) localMovementSpeed *= .2f;
        float movementStep = localMovementSpeed * Time.deltaTime; // distance traveled since last frame

        transform.position = Vector3.MoveTowards(transform.position, targetWaypoint.position,movementStep);

        if(fDistanceRemaining > 6.0f ){
            float rotationStep = rotationSpeed * Time.deltaTime; // direction change rate since last frame
            Vector3 directionToStation  = targetWaypoint.position - transform.position;
            Quaternion rotationToStation = Quaternion.LookRotation(directionToStation);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotationToStation, rotationStep);
        }
        //shoot a raycast relatively downwards
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -transform.up, out hit, 100)) 
        {
            Vector3 pos = hit.point; //get the position where the ray hit the ground
            //shoot a raycast up from that position towards the object
            Ray upRay = new Ray(pos, transform.position - pos); 
 
            //get a point (vector3) in that ray 8 units from its origin
            float distanceAboveGround = 3.0f;
            //if(fDistanceRemaining < 40.0f)distanceAboveGround = 0.5f;
            if(fDistanceRemaining < 30.0f)distanceAboveGround = 0.4f;
            if(fDistanceRemaining < 20.0f)distanceAboveGround = 0.2f;
            if(fDistanceRemaining < 2.0f)distanceAboveGround = 0.08f;
            Vector3 upDist = upRay.GetPoint(distanceAboveGround); 
            //smoothly interpolate its position
            transform.position = Vector3.Lerp(transform.position, upDist, 1.0f); 
        }       
    }

    float CheckDistanceToWaypoint()
    {
        float distance = Vector3.Distance(transform.position, targetWaypoint.position);

        if(distance <= minDistance)
        {
            //Start a wait timer to allow passangers off and on
            StationWaitTimer = allowedWaitTime;
            UpdateTargetWaypoint();
        }
        //Debug.Log("Distance to travel to next station "+distance);
        return distance;
    }

     private void OnTriggerEnter(Collider coll) {
       if (coll.tag == "Player"){
           coll.transform.parent = this.transform;
            //Debug.Log("Vehicle Player on");
       }

    }
     private void OnTriggerExit(Collider coll) {
       if (coll.tag == "Player"){
           coll.transform.parent = null;
            //Debug.Log("Vehicle Player off");
       }
       
    }
    void UpdateTargetWaypoint()
    {
        currentWaypointIndex++;
        if(currentWaypointIndex >= waypoints.Count){
            currentWaypointIndex = 0;
        }
        targetWaypoint = waypoints[currentWaypointIndex];

    }

}
