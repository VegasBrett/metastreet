using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;

public class MonitorButtons : MonoBehaviour
{

    [SerializeField]
    public GameObject TradingButton;
    [SerializeField]
    public GameObject WatchVideoButton;
    //[SerializeField]
    //public GameObject TradingFloorButton;

    public void vMonitorEntered(){
        TradingButton.SetActive(true);
        WatchVideoButton.SetActive(true);
    }
    public void vMonitorExit(){
        TradingButton.SetActive(false);
        WatchVideoButton.SetActive(false);
    }

    private void OnTriggerEnter(Collider collider) {
        Debug.Log("Monitor collision enter "+collider.name);
        vMonitorEntered();
    }
    private void OnTriggerExit(Collider collider){
        Debug.Log("Monitor collision exit "+collider.name);
        vMonitorExit();
    }
    private void OnTriggerStay(Collider collider) {
        Debug.Log("Monitor collision stay "+collider.name);
    }

    
    public void TradingPlatformScene(){
        //SceneManager.LoadScene()
        SceneManager.LoadScene("TradingFloor");
    }

    public void ViewVideos(){
        SceneManager.LoadScene("WatchVidios");        
    }
    
    public void TradingFloor(){
         SceneManager.LoadScene("TradingFloor");  
    }
    
    public void Exit(){
        Application.Quit();
    }
}
