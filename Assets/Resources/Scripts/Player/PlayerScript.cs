﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerScript : MonoBehaviourPunCallbacks
{
        public TextMesh playerNameText;
        public GameObject floatingInfo;
        public GameObject MiniMapCam;

        private Material playerMaterialClone;

       // [SyncVar(hook = nameof(OnNameChange))]
        public string playerName;
       // [SyncVar(hook = nameof(OnColorChange))]
        public Color playerColor = Color.white;

        public GameObject LectureRoom1;
        private Vector3 offset;            //Private variable to store the offset distance between the player and camera
    //private SceneScript sceneScript;


        void OnNameChange(string _Old, string _New)
        {
            playerNameText.text = playerName;
        }
        
        void OnColorChange(Color _Old, Color _New)
        {
            playerNameText.color = _New;
            //playerMaterialClone =new Material(GetComponent<Renderer>().material);
            //playerMaterialClone.color = _New;
            //GetComponent<Renderer>().material = playerMaterialClone;
        }
        /*
        public override void OnStartLocalPlayer()
        {
             base. OnStartLocalPlayer();
                    //sceneScript.playerScript = this;
            Camera.main.transform.SetParent(transform);
            Camera.main.transform.localPosition = new Vector3(0,0,0);
                
            floatingInfo.transform.localPosition = new Vector3(0, -0.3f, 0.6f);
            floatingInfo.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);

            string name = "Player" + Random.Range(100, 999);
            Color color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
            CmdSetupPlayer(name, color);
            MiniMapCam = GameObject.Find("PlayerMinimapCam");
            offset = MiniMapCam.transform.position - transform.position;
        }

        [Command]
        public void CmdSetupPlayer(string _name, Color _col)
        {
        //player info sent to server, then server updates sync vars which handles it on all clients
            playerName = _name;
            playerColor = _col;
            //sceneScript.statusText = $"{playerName} joined.";
        }
*/
 /*
        // Update is called once per frame
        void Update()
        {
            if(!isLocalPlayer || !hasAuthority) {
                // make non-local players run this
               // floatingInfo.transform.LookAt(Camera.main.transform);
                return;
            }
            float moveX = Input.GetAxis("Horizontal") * Time.deltaTime* 110.0f;
            float moveZ  = Input.GetAxis("Vertical") * Time.deltaTime* 4.0f;

            transform.Rotate(0,moveX,0);
            transform.Translate(0,0,moveZ);
            MiniMapCam.transform.position = transform.position + offset;

        }
        */
}
