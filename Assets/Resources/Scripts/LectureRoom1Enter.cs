using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class LectureRoom1Enter :MonoBehaviourPunCallbacks
{
    GameObject LectureRoom;

     [SerializeField]
  string SelectTag;
    //  [SerializeField]
    //GameObject Door;
    public GameObject[] VisableList;

    void Start(){
        VisableList =GameObject.FindGameObjectsWithTag(SelectTag);

        foreach (GameObject visable in VisableList){
            try{
            visable.GetComponentInChildren<MeshRenderer>().enabled = false;
            } catch(Exception e){
                Debug.Log("Exception object name " + visable.name );
            }
        }

    }

    void Update()
    {

    }
    private void OnTriggerEnter (Collider Client)
    {
        if(Client.gameObject.tag == "Player"){

            Debug.Log("Collider Entered "+ Client.gameObject.tag);
            foreach (GameObject visable in VisableList){
                visable.GetComponentInChildren<MeshRenderer>().enabled = true;
            }  
        }
    }
    private void OnTriggerExit (Collider Client)
    {
       if(Client.gameObject.tag == "Player"){
            Debug.Log("Collider Entered "+ Client.gameObject.tag);
            foreach (GameObject visable in VisableList){
                visable.GetComponentInChildren<MeshRenderer>().enabled = false;
            }
       }  
    }
}
