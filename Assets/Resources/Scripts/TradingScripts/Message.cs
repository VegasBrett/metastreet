using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
//using Common.Enums;
using Alveo3_Client;
using UnityEngine;
using Photon.Pun;

namespace Alveo3_Client
{
    public class Message 
    {
        public enum eTimeFrame
        {
            Unknown = -1,
            M1 = 1, // native support
            M5 = 5, // native support
            M15 = 15, // native support
            M30 = 30, // native support
            H1 = 60,
            H2 = 120,
            H4 = 240,
            D1 = 60 * 24, // native support
            D7 = 60 * 24 * 7,
            MN = 60 * 24 * 30,
        }

        public enum Code : long
        {
            Error = -1,
            Action = 100,
            Login = 115,
            UpdateOrder = 116,
            NewOrder = 117,
            ModifyOrder = 118,
            CloseOrder = 119,
            Subscribe = 120,
            Unsubscribe = 121,
            NewQuote = 123,
            Historical = 124,
            Account = 200,
            AccountDailyBalance = 201,
            RequestOrderLoad = 220,
            SymbolList = 411,
            Heartbeat = 999,
            Exit = 1000
        }

        [JsonProperty]
        internal int Type { get; private set; }

        [JsonProperty]
        internal Dictionary<string, object> Args { get; private set; }

        public Message() { }

        private Message(Code type)
        {
            Type = (int)type;
            Args = new Dictionary<string, object>();
        }

        private Message AddArg(string key, object val)
        {
            try
            {
                //Debug.Log("AddArg "+key +" Object "+ val);
                Args.Add(key, val);
            }
            catch (Exception e) { 
                Debug.Log("Message AddArg exception "+e);
            }

            return this;
        }

        /// <summary>
        ///     Encrypt data being sent with order
        /// </summary>
        public string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Converts the request to a JSON string
        /// </summary>
        /// <returns>the JSON representation of the request</returns>
        public override string ToString()
        {
            try

            {
                //TestMetastreetServerClient cTestMetastreetServerClient = new TestMetastreetServerClient();
                if (Type != (int)Code.Heartbeat && Type != (int)Code.Login)
                {
                    string umid = Guid.NewGuid().ToString();
                    AddArg("UMID", umid);
                    string sHashInput = "";
                    sHashInput = "apiary" + TradingPlatform.iUID + "alveo" + umid;
                    AddArg("Hash", CalculateMD5Hash(sHashInput));
                }

                return JsonConvert.SerializeObject(this);
            }
            catch (Exception e) { 
                Debug.Log("Message ToString exception "+e);
            }

            return string.Empty;
        }
        public static string FullJasonMessage(Code eCode,string sArgs)
        {
            string sMessage = string.Format("{0}\"Type\":{1},\"Args\":{2}{3}",'{',(int)eCode,sArgs,'}');
 
            return sMessage;

        }
        public static Message FromJsonString(string json)
        {
            return JsonConvert.DeserializeObject<Message>(json);
        }

        public static string HeartbeatRequest(long timestamp)
        {
            cHeartbeatWithTime cRequest = new cHeartbeatWithTime();
            cRequest.Timestamp = timestamp;
            var jasonString = JsonConvert.SerializeObject(cRequest);
            string sHeartbeatRequest = FullJasonMessage(Code.Heartbeat ,jasonString);
            return sHeartbeatRequest;
        }

        public static string HeartbeatRequest()
        {
            cHeartbeat cRequest = new cHeartbeat();
            var jasonString = JsonConvert.SerializeObject(cRequest);
            string sHeartbeatRequest = FullJasonMessage(Code.Heartbeat ,jasonString);
            return sHeartbeatRequest;
        }

        public  Message LoginRequest(long uid, string session, string sPlatformVersion)
        {
            return new Message(Code.Login).AddArg("UID", uid).AddArg("Session", session).AddArg("Version", sPlatformVersion);
        }

        public string LoginRequest( string sName, string session, string sPlatformVersion)
        {
            Login cLogin = new Login();
            cLogin.Name = sName;
            cLogin.Version = sPlatformVersion;
            cLogin.Session = session;
            var jasonLoginString = JsonConvert.SerializeObject(cLogin);
            string sLoginString = FullJasonMessage(Code.Login,jasonLoginString);
            //Debug.Log("sLoginString = "+sLoginString);
            return sLoginString;
        }
        public string SymbolListRequest(long iUID)
        {
            string umid = Guid.NewGuid().ToString();
            string sHashInput = "apiary" + iUID + "alveo" + umid;
            cSymbolListRequest cRequest = new cSymbolListRequest(iUID,umid,sHashInput);
            var jasonString = JsonConvert.SerializeObject(cRequest);
            string sSymbolRequest = FullJasonMessage(Code.SymbolList ,jasonString);
            //Debug.Log("sSymbolRequest message "+sSymbolRequest);
            return sSymbolRequest;
        }

        public string AccountsRequest(long iUID)
        {
           AccountRequest cRequest = new AccountRequest();
            cRequest.iUID = iUID;
            var jasonString = JsonConvert.SerializeObject(cRequest);
            string sAccountRequest = FullJasonMessage(Code.Account ,jasonString);
            //Debug.Log("sSymbolRequest message "+sAccountRequest);
           return sAccountRequest;
        }

        public string AccountBalanceRequest() {
            AccountBalanceRequest cRequest = new AccountBalanceRequest();
            var jasonString = JsonConvert.SerializeObject(cRequest);
            string sBalanceRequest = FullJasonMessage(Code.AccountDailyBalance ,jasonString);
            return  sBalanceRequest;           
        }
        
        public string OrdersRequest(long iUID, int iOrderLimit)
        {
            //string umid = Guid.NewGuid().ToString();
           // string sHashInput = "apiary" + iUID + "alveo" + umid;
            cOrdersRequest cRequest = new cOrdersRequest();
            cRequest.iUID = iUID;
            cRequest.OrderLimit = iOrderLimit;
            var jasonString = JsonConvert.SerializeObject(cRequest);

            string sOrdersRequest = FullJasonMessage(Code.RequestOrderLoad ,jasonString);
            //Debug.Log("sOrdersRequest message "+sOrdersRequest+"\n "+jasonString);
            return sOrdersRequest;
        }

        public static string SubscribeRequest(long iUID,string symbol)
        {
             cSymbolRequest cRequest = new cSymbolRequest();
            //cRequest.iUID = iUID;
            cRequest.Symbol = symbol;
            var jasonString = JsonConvert.SerializeObject(cRequest);
            string sSymbolRequest = FullJasonMessage(Code.Subscribe, jasonString);
            //Debug.Log("sSymbolRequest message " + sSymbolRequest);
            return sSymbolRequest;
         }

        public string UnsubscribeRequest(string symbol)
        {
            cSymbolUnSubscribe cRequest = new cSymbolUnSubscribe();
            cRequest.Symbol = symbol;
            var jasonString = JsonConvert.SerializeObject(cRequest);
            string sSymbolUnSubscribe = FullJasonMessage(Code.Unsubscribe, jasonString);
            //Debug.Log("sSymbolUnSubscribe message " + sSymbolUnSubscribe);
            return sSymbolUnSubscribe;
           //return new Message(Code.Unsubscribe).AddArg("Symbol", symbol);
        }

        public string HistoricalRequest(string id, string symbol, string tf, int bars)
        {

            cBarHistory cRequest = new cBarHistory();
            cRequest.Symbol = symbol;
            cRequest.ID = id;
            cRequest.TimeFrame = tf;
            cRequest.NumBars = bars;
            var jasonString = JsonConvert.SerializeObject(cRequest);
            string sBarRequest = FullJasonMessage(Code.Historical, jasonString);
            //Debug.Log("sSymbolRequest message " + sSymbolRequest);
            return sBarRequest;
        }

        public Message UpdateOrderRequest(Order order)
        {
            return new Message(Code.UpdateOrder).AddArg("Order", order);
        }

        static public Message NewOrderRequest(Order order)
        {
            return new Message(Code.NewOrder).AddArg("Order", order);
        }

        static public Message ModifyOrderRequest(Order order)
        {
            return new Message(Code.ModifyOrder).AddArg("Order", order);
        }

        static public Message CloseOrderRequest(Alveo3_Client.Order order)
        {
            return new Message(Code.CloseOrder).AddArg("Order", order);
        }
        public string ExitRequest()
        {
            cExitRequest cRequest = new cExitRequest();
            var jasonString = JsonConvert.SerializeObject(cRequest);
            string sExitRequest = FullJasonMessage(Code.Exit ,jasonString);
            //Debug.Log("Messages sExitRequest message "+sExitRequest);
            return sExitRequest;
        }    }

    public class AccountRequest
    {
        public AccountRequest()
        {
        }
        public long iUID;
        //public string UMID;
        //public string Hash;

    }
    public class AccountBalanceRequest
    {
        public AccountBalanceRequest()
        {
        }
        //public long iUID;
        //public string UMID;
        //public string Hash;

    }
    public class cBarHistory
    {
        public cBarHistory() 
        {
            
        }
        public string ID;
        public string Symbol;
        public string TimeFrame;
        public int NumBars;
    }
    public class Login
    {
        public string Name;
        public string Version;
        public string Session;
    }
    class cSymbolListRequest
    {
        public cSymbolListRequest(long iUID,string sUMID, string sHash)
        {
            lUID = iUID;
            UMID = sUMID;
            Hash = sHash;
        }
        private long lUID;
        private string UMID;
        private string Hash;
    }

    class cHeartbeat
    {

    }
    class cHeartbeatWithTime
    {
        public long Timestamp;
    }
    class cOrdersRequest
    {
        public cOrdersRequest()
        {

        }
        public long iUID;
        public int OrderLimit;
    }

    class cSymbolRequest
    {
        public cSymbolRequest()
        {
        }
        public string Symbol;
    }
    class cSymbolUnSubscribe
    {
        public cSymbolUnSubscribe()
        {
        }
        public string Symbol;
    }
    class cExitRequest
    {
        public cExitRequest()
        {
        }
    }


}
