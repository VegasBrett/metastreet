using System;
using System.Collections.Generic;
using UnityEngine;

namespace Alveo3_Client{
    [System.Serializable]
    public class Order
    {
        // Start is called before the first frame update
            public enum eEmotion
            {
                None = -1,
                Greed,
                Fear,
                Focus,
                Hope
            }
            public enum eTimeFrame
            {
                Unknown = -1,
                M1 = 1, // native support
                M5 = 5, // native support
                M15 = 15, // native support
                M30 = 30, // native support
                H1 = 60,
                H2 = 120,
                H4 = 240,
                D1 = 60 * 24, // native support
                D7 = 60 * 24 * 7,
                MN = 60 * 24 * 30,
            }
            public enum eSender
            {
                Unknown = -1,
                Chart = 0,
                Button,
                MarketWatch,
                DepthOfMarket,
                CodeBase
            }
            public enum eTradeSide
            {
                Unknown = -1,
                Buy = 0,
                Sell
            }
            public enum eTradeType
            {
                Unknown = -1,
                Market = 0,
                Limit,
                Stop
            }
            public enum eOrderStatus
            {
                Unknown = -1,
                PendingNew = 0,
                New,
                Filled,
                Processing,
                Deleted = 32,
                Closed = 33,
                Rejected = 34
            }
            public enum eCloseType
            {
                Unknown = -1,
                Expiration = 0,
                Market,
                TakeProfit,
                StopLoss,
                Risk
            }
            public enum eTimeInForce
            {
                Unknown = -1,
                Day = 0,// Day
                GTC, // Good Till Cancel
                GTD // Good Till Date
            }
/*
            public Order()
            {
                Id = 10000;
                //ExternId = -1;
                //CustomId = string.Empty;
                AccountBalanceFilled = 100000.0;
                Symbol = "EUR/USD";
                Price = 100.001;
                Pips = 100.0;
                Quantity = 0.01;
                StopLoss = 00.951;
                TakeProfit = 100.021;
                Side = eTradeSide.Buy;
                Type = eTradeType.Market;
                OpenType = eTradeType.Market;
                CloseType = eCloseType.Market;
                Status = eOrderStatus.Filled;
                TimeInForce = eTimeInForce.Unknown;
                ExpirationDate = null;
                OpenDate = DateTime.MinValue;
                FillDate = null;
                CloseDate = DateTime.MinValue;
                OneClick = false;
                //Sender = eSender.Unknown;
                OpenPrice = 100.001;
                ClosePrice = 0.0;
                //ClosePriceLow = 0.0;
                //ClosePriceHigh = 0.0;
                //OpenBid = 0.0;
                //OpenAsk = 0.0;
                //CloseBid = 0.0;
                //CloseAsk = 0.0;
                //FilledAsk = 0.0;
                //FilledBid = 0.0;
                //Emoticon = eEmotion.None;
            }
*/
            public Int64 Id { get; set; }
            //public long ExternId { get; set; }
            //public string CustomId { get; set; }
            public string Symbol { get; set; }
            public double AccountBalanceFilled { get; set; }
            public double Pips { get; set; }
            public double Price { get; set; }
            public double Quantity { get; set; }
            public eTradeSide Side { get; set; }
            public eTradeType Type { get; set; }
            public eTradeType OpenType { get; set; }
            public eCloseType CloseType { get; set; }
            public eOrderStatus Status { get; set; }
            public eTimeInForce TimeInForce { get; set; }
            public DateTime? ExpirationDate { get; set; }
            public DateTime OpenDate { get; set; }
            public DateTime? FillDate { get; set; }
            public DateTime CloseDate { get; set; }
            public double TradeCommission { get; set; }
           // public decimal AgentCommission { get; set; }
            //public decimal Swap { get; set; }
            public string Comment { get; set; }
            public double OpenPrice { get; set; }
            public double ClosePrice { get; set; }
            public double CurrentPrice { get; set; }
            //public double ClosePriceHigh { get; set; }
            //public double ClosePriceLow { get; set; }
            public double Profit { get; set; }
            public double StopLoss { get; set; }
            public double TakeProfit { get; set; }
            public bool TrailingStop { get; set; }
            public bool OneClick { get; set; }
           // public eSender Sender { get; set; }
            //public double OpenBid { get; set; }
            //public double OpenAsk { get; set; }
            //public double CloseBid { get; set; }
            //public double CloseAsk { get; set; }
            //public double FilledBid { get; set; }
            //public double FilledAsk { get; set; }
            //public eEmotion Emoticon { get; set; }

            //public static string Chart = "chart";
            //public static string WatchList = "watch-list";
           // public static string OrderButton = "order-button";
            //public static string DoM = "depth-of-market";
            public void Method()
            {
                Debug.Log("Method called on " + Id);
            }
            public event Action Updated;

            public void Update()
            {
                Updated?.Invoke();
            }

            /// <summary>
            ///     Update order parameters according to other order passed in parameter
            /// </summary>
            public virtual void Copy(Order trade)
            {
                if (trade == null)
                    throw new ArgumentNullException("trade");

                Id = trade.Id;
                //ExternId = trade.ExternId;
                //CustomId = trade.CustomId;
                AccountBalanceFilled = trade.AccountBalanceFilled;
                Symbol = trade.Symbol;
                Price = trade.Price;
                Quantity = trade.Quantity;
                Side = trade.Side;
                Type = trade.Type;
                OpenType = trade.OpenType;
                CloseType = trade.CloseType;
                Status = trade.Status;
                //OpenDate = trade.OpenDate.DeepCloneBySerialization();
                OpenDate = trade.OpenDate;
                //FillDate = trade.FillDate == null ? null : trade.FillDate.DeepCloneBySerialization();
                FillDate = trade.FillDate == null ? null : trade.FillDate;
                //CloseDate = trade.CloseDate.DeepCloneBySerialization();
                CloseDate = trade.CloseDate;
                TimeInForce = trade.TimeInForce;
                //ExpirationDate = trade.ExpirationDate == null ? null : trade.ExpirationDate.DeepCloneBySerialization();
                ExpirationDate = trade.ExpirationDate == null ? null : trade.ExpirationDate;
                Comment = trade.Comment;
                //Swap = trade.Swap;
                //AgentCommission = trade.AgentCommission;
                TradeCommission = trade.TradeCommission;
                ClosePrice = trade.ClosePrice;
                OpenPrice = trade.OpenPrice;
                //ClosePriceHigh = trade.ClosePriceHigh;
                //ClosePriceLow = trade.ClosePriceLow;
                Pips = trade.Pips;
                Profit = trade.Profit;
                StopLoss = trade.StopLoss;
                TrailingStop = trade.TrailingStop;
                TakeProfit = trade.TakeProfit;
                OneClick = trade.OneClick;
                //Sender = trade.Sender;
               // OpenBid = trade.OpenBid;
                //OpenAsk = trade.OpenAsk;
                //CloseBid = trade.CloseBid;
                //CloseAsk = trade.CloseAsk;
                //FilledBid = trade.FilledBid;
                //FilledAsk = trade.FilledAsk;
                //Emoticon = trade.Emoticon;
            }

            /// <summary>
            ///     Determinate if order is not closed or rejected
            /// </summary>
            public bool IsActiveTrade()
            {
                return Status == eOrderStatus.PendingNew || Status == eOrderStatus.New || Status == eOrderStatus.Filled || Status == eOrderStatus.Processing;
            }

    }
}

