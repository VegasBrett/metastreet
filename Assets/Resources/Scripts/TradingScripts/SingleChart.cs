using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alveo3_Client;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using CodeMonkey.Utils;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using Photon.Pun;
using Photon.Realtime;

public class SingleChart
{
    public static SingleChart instance;
    private static TradingPlatform TBC;

    private RectTransform graphContainer;
    private RectTransform labelTemplateXAxis;
    private RectTransform labelTemplateYAxis; 
     private RectTransform dashTemplateXHorizontal;
    private RectTransform dashTemplateYVertical;
    private List<GameObject> gameObjectList;
    private GameObject tooltipGameObject;
    private Func<int, string> getAxisLabelX;
    private Func<float, string> getAxisLabelY;

    static string sSymbol = "";
    //static Alveo3Client.eTimeFrame eTimeFrame;
    static string sTimeFrame;
    int iTF;
    static public List<Bar> lBars;
    
    static int iQuoteListIndex = -1;

    int iCurrentMinute = -1;
    int iCurrentHour = -1;
    int iTimeFrameMinutes = -1;
    bool bNewQuote = true;
    static string sID = "";

    public int dataCurrentSize;

    static bool bBarsAvalable = false;


    Color barColor; 

    DateTime dtLastQuoteTime;
    
    public static float barWidth = 4.0F;
    public static float graphHeight;
    public static float graphWidth;

    public static float yMax;
    public static float yMin;
    public static float xMax;
    public static float xMin;
    public static float xRange;
    public static float yRange;
    public static DateTime dt1970 = new DateTime(1970, 1, 1, 0, 0, 0);
    bool bBarsAvailable = false;


    public  SingleChart() {

        instance = this;
        //TBC = ;
        //graphContainer = GraphContainer;
        //labelTemplateXAxis = graphContainer.Find("LabelTemplateXaxis").GetComponent<RectTransform>();
        //labelTemplateYAxis = graphContainer.Find("LabelTemplateYaxis").GetComponent<RectTransform>();
        //dashTemplateXHorizontal = graphContainer.Find("dashTemplateXHorizontal").GetComponent<RectTransform>();
        //dashTemplateYVertical = graphContainer.Find("dashTemplateYVertical").GetComponent<RectTransform>();
        //IGraphVisual barGraphVisual = new BarChartVisual(graphContainer, Color.green, 0.3f);
    }

    public void vSetSymbol(string Symbol){
        sSymbol = Symbol;
    }
    public void vSetChartTimeframe(string sTF){
        sTimeFrame = sTF;
        switch (sTF)
        {
            case "M1":
                iTF = 1;
                break;
            case "M5":
                iTF = 5;
                break;
            case "M15":
                iTF = 15;
                break;
            case "M30":
                iTF = 30;
                break;
            case "H1":
                iTF = 60;
                break;
            case "H2":
                iTF = 120;
                break;
            case "H4":
                iTF = 480;
                break;
            case "D1":
                iTF = 1440;
                break;
            case "D7":
                iTF = 10080; //60*24*7
                break;
            case "MN":
                iTF = 43200; // 60*24*30
                break;
        }
        iTimeFrameMinutes = iTF;
        //Debug.Log("vSetChartTimeframe sTimeFrame "+sTimeFrame +" iTF "+iTF);
     }

    public string sGetSymbol(){
        return sSymbol;
    }

    public string sGetChartID(){
        return sID;
    }
    public string getChartTimeFrame(){
        return sTimeFrame;
    }
    
    public static  bool bBarsAvail(){
        return bBarsAvalable;
    }

    public void FixedUpdate(){

    }
    public void vNewQuote(Quote cQuote, double dTimeSeconds){
        DateTime dtNow = DateTime.UtcNow;
       int iMinute = dtNow.Minute;
        //Check if new bar is forming
        if(iMinute != iCurrentMinute){
            int iMinutesOfDay = dtNow.Hour*60 + dtNow.Minute;
            //Debug.Log("vUpdateCurrentBar iTF "+iTF+" iMinutesOfDay "+iMinutesOfDay);
            vUpdateCurrentBar();
            if((iMinutesOfDay) < 1440){
                int iTimeMod = iMinutesOfDay % iMinutesOfDay;
                if(iTimeMod == 0){
                    //New bar
                    Bar iNewBar = new Bar();
                    iNewBar.Open = (float)cQuote.Bid;
                    iNewBar.Close = (float)cQuote.Bid;
                    iNewBar.High = (float)cQuote.Bid;
                    iNewBar.Low = (float)cQuote.Bid;
                    iNewBar.Volume = (float)cQuote.Volume;
                    iNewBar.Date =(long) dTimeSeconds;
                }
            }
            iCurrentMinute = iMinute;
            if(iCurrentHour != dtNow.Hour){
                iCurrentHour = dtNow.Hour;
            }
        }
    }

    public void vInitBarList(){
        lBars = new List<Bar>();
    }

    public static Quote cFindQuote(){
        Quote  cLastQuote = new Quote();
        //Check if this is the first time through to set the index in quote list 
        if(iQuoteListIndex < 0){
            for(int iIndex = 0; iIndex < TradingPlatform.lCurrentSymbolQuote.Count; iIndex++ )
            {
                if(TradingPlatform.lCurrentSymbolQuote[iIndex].Symbol == sSymbol)
                {
                    iQuoteListIndex = iIndex;
                    cLastQuote = TradingPlatform.lCurrentSymbolQuote[iQuoteListIndex];
                } else{
                    // already set check for new quote
                    if(TradingPlatform.lCurrentSymbolQuote[iQuoteListIndex].Time > cLastQuote.Time){
                        cLastQuote = TradingPlatform.lCurrentSymbolQuote[iQuoteListIndex];
                    }

                }

            }
        }
       return cLastQuote;
    }

    private void vUpdateCurrentBar()
    {

        DateTime dtNow = DateTime.Now;
        //bNewQuote = false;
        //CandleNew CN = new CandleNew();
        Alveo3_Client.Quote cLastQuote = cFindQuote();
        DateTime dtQuoteTime = cLastQuote.Time;
        int iMinute = dtNow.Minute;
        //Check if new bar is forming
        if(iMinute != iCurrentMinute){
            int iMinutesOfDay = dtNow.Hour*60 + dtNow.Minute;
            int iTF = iTimeFrameMinutes;
            //Debug.Log("vUpdateCurrentBar iTF "+iTF);
            if(iTF < 60){
                int iTimeMod = iMinute % iTF;
                if(iTimeMod == 0){
                    //New bar
                    Bar iNewBar = new Bar();
                    iNewBar.Open = (float)cLastQuote.Bid;
                    iNewBar.Close = (float)cLastQuote.Bid;
                    iNewBar.High = (float)cLastQuote.Bid;
                    iNewBar.Low = (float)cLastQuote.Bid;
                    iNewBar.Volume = (float)cLastQuote.Volume;
                    dtLastQuoteTime = dtQuoteTime;
                    SingleChart.lBars.Insert(0,iNewBar);
                    float xMin = (float)iNewBar.Date;
                    float xMax = xMin + 60*iTimeFrameMinutes;
                    //CreateCandle( xMax, xMin, iNewBar.High, iNewBar.Low, 
                    //    iNewBar.Open, iNewBar.Close, xMin, iNewBar.Close);
                }
            }
            iCurrentMinute = iMinute;
            if(iCurrentHour != dtNow.Hour){
                iCurrentHour = dtNow.Hour;
            }
        } else
        {
            if(dtQuoteTime > dtLastQuoteTime){
                Bar sBar = lBars[0];
                sBar.Close = (float)cLastQuote.Bid;
                sBar.Volume += (float)cLastQuote.Volume;
                if(cLastQuote.Bid > SingleChart.lBars[0].High) sBar.High = (float)cLastQuote.Bid;
                if(cLastQuote.Bid < SingleChart.lBars[0].Low) sBar.Low = (float)cLastQuote.Bid;
                //UpdateCurrentPrice(SingleChart.lBars[0].Close);
                dtLastQuoteTime = dtQuoteTime;
                SingleChart.lBars[0] = sBar;
                //TradingBarChart.vUpdateCurrentBar(SingleChart.lBars);
            }
        }
    }

    public void vAddNewBar(Bar cBar, bool bLastBar, string sID){
        //Fix to correct chart later with sID
        //Assume receive in time order, the newest bar is at index 0 oldest at end
        Bar cLocalBar = cBar;
        //Debug.Log("cLocalBar "+cLocalBar.Date);
        int iCurrentListCount;
        try{
            iCurrentListCount = lBars.Count;
            //Debug.Log("vAddNewBar iCurrentListCount "+iCurrentListCount);
        } catch{
            lBars = new List<Bar>();
            iCurrentListCount = 0;
            lBars.Add(cLocalBar);
            return;
        }
        bool bDuplicate = false;
        for(int i = 0; i < iCurrentListCount; i++){
            if(lBars[i].Date == cBar.Date){
                bDuplicate = true;
                break;
            }
        }
        if(bDuplicate) return;
        //Debug.Log("vAddNewBar iCurrentListCount "+iCurrentListCount);
        try{
            lBars.Add(cLocalBar);
            //Debug.Log("vAddNewBar add bar"+cLocalBar.Close+" "+lBars.Count);
            lBars.Sort((left, right) => left.Date.CompareTo(right.Date));
        } catch (Exception e){
            Debug.Log("vAddNewBar exception1 "+e);
        }
        if(lBars.Count > 95) {
            //Debug.Log("TradingPlatform vAddNewBar iCurrentListCount "+iCurrentListCount);
            bBarsAvailable = true;
        }
    }

    public static void vUpdateBarZero(Quote cQuote){
        Bar cBarZero = lBars[0];
        cBarZero.Close = (float)cQuote.Bid;
        cBarZero.Volume += (float)cQuote.Volume;
        if(cQuote.Bid > cBarZero.High) cBarZero.High = (float)cQuote.Bid;
        if(cQuote.Bid < cBarZero.Low) cBarZero.Low = (float)cQuote.Bid;
        lBars[0] = cBarZero;
    }
}

