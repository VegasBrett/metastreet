using System.Collections;
using System.Collections.Generic;
using Alveo3_Client;
using Photon.Pun;
using UnityEngine;

public class SetUserName  : MonoBehaviourPunCallbacks
{
    static string sUserName = "";
    public static void SetPlayerName(string sUserName1)
    {
        if(string.IsNullOrEmpty(sUserName1))
        {
            Debug.Log("PlayerNameInputManager User Name is empty");
            return;
        }
//Debug.Log("Username is " + sUserName);
        sUserName = sUserName1;
    }

    public static string sGetUserName() {
        return sUserName;
    } 

}