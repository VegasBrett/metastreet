using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
//using MySql.Data.MySqlClient;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.IO;
//using System.Reflection.Metadata;
//using System.Reflection;using System.Collections;
using Photon.Pun;
using UnityEngine;

namespace Alveo3_Client
{

    public class Processor  //: MonoBehaviour
    {
 
        static Processor instance {get; set;}
        private static string sClassName = "Processor";

        [SerializeField]
        static public GameObject LoginPanel;
        [SerializeField]
        static public GameObject NewChartButton;
        [SerializeField]
        static public GameObject FileButton;
        [SerializeField]
        static public GameObject NewOrderButton;
        [SerializeField]
        static public GameObject OpenOrderListButton;
        [SerializeField]
        static public GameObject ClosedOrderListButton;
        [SerializeField]
        static public GameObject WatchListButton;

        static public GameObject EnterTradingButton;
        static public GameObject EnterUsernameInput;



        static bool bConnected = false;
        static bool ContinueProcess = true;
        static bool bOrderHistorySent = false;
        static TcpClient aos_socket;
        string server_address;
        int server_port;
        static Thread ProcessorThread;
        static int iOrdersLoaded = 0;
        static int iHeartbeatMinute = -1;
        static DateTime dtLastHeartbeatSent;
        static DateTime dtLoginStartTime;
        static int iLoginPhase = -1;
        static int iTickProcessThisMinute = 0;
        static int iBeatsSentThisMinute = 0;
        static int iBeatsReceivedThisMinute = 0;
        static int iProcessCycles = 0;
        static int iProcessMessageStarted = 0;
        static int iProcessMessageExit = 0;
        static int iHeartbeatSecond = -1;

        static int iRequestedBars = 0;
        static int iReceivedBars = 0;
        static int iHistoricOrdersRequest = 0;


         static private DateTime dt1970 = new DateTime(1970, 1, 1, 0, 0, 0);

        static private TradingPlatform TBT;

        private void Awake(){
            //TBT = new TradingPlatform();
            instance = this;
        }

        public Processor()
        {
            //this.Socket = ClientSocket;
            //Define the server address for testing
            //server_address = "aos.avo.tq.apiaryfund.com";
            server_address = "aos2.apiaryfund.net";
            //server_address = "10.8.0.5";
            server_port = 5280;
            bool bIsConnected = false;
//Debug.Log("Processor aos_socket "+aos_socket+" "+ bIsConnected);
            tcpConnectServer(server_address, server_port);
            if(aos_socket == null) {
                Debug.Log("Processor aos_socket = null");               
            } else{
                ContinueProcess = true;
                //ProcessorThread = new Thread(new ThreadStart(Process));
                //ProcessorThread.Priority = ThreadPriority.Normal;
                //ProcessorThread.Start();
                bIsConnected = SocketConnected(aos_socket);
                //Debug.Log("Processor aos_socket "+aos_socket+" "+ bIsConnected);
                TradingPlatform.lInstruments = new List<Instrument>();
                TradingPlatform.lActiveInstruments = new List<Instrument>();
                TradingPlatform.lTradingAccounts = new List<cTradingAccount>();
                dtLastHeartbeatSent = DateTime.Now;               
            }
            //Debug.Log("");
        }

        
        static void tcpConnectServer(string sServer, int iServerPort)
        {
            string sMethodName = "vConnectLPQuoteServer";
            Console.WriteLine("{0} sServer {1} iServerPort {2}", sMethodName, sServer, iServerPort);
            string sErrorDetails = string.Format("sServer {0} iServerPort {1}", sServer, iServerPort);
            string sErrorType = "Debug";
            //Logger(EventLevel.Debug, sClassName, sMethodName, sErrorType, sErrorDetails);
            bool bSuccess = false;
            int iAttempts = 0;
            int iMaxAttempts = 10;
            aos_socket = new TcpClient();
            try
            {
                do
                {
                    try
                    {
                        //Connect with Quote server via socket
                        Console.WriteLine("vConnectQuoteServer sServer {0} iServerPort {1}", sServer, iServerPort);
                        aos_socket = new TcpClient(sServer, iServerPort);
                        bSuccess = true;

                        return;
                    }
                    catch (Exception e)
                    {
                        iAttempts++;
                        sErrorDetails = string.Format("Failed Quote Socket Connection Attempt {0}", iAttempts);
                        sErrorType = e.ToString();
                        //Logger(EventLevel.Warn, sClassName, sMethodName, sErrorType, sErrorDetails);
                        Thread.Sleep(500);
                    }
                } while (!bSuccess && iAttempts < iMaxAttempts);
                if (iAttempts > iMaxAttempts) return;
            }
            catch (SocketException e)
            {
                sErrorDetails = string.Format("SocketException");
                sErrorType = e.ToString();
                //Logger(EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
            }
            catch (Exception e)
            {
                sErrorDetails = string.Format("{0} Exception",sMethodName);
                sErrorType = e.ToString();
                //Logger(EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
            }
            return;
        }

        static bool SocketConnected(TcpClient s)
        {
            bool part1 = s.Connected;
            return part1;
            //bool part2 = (s.Available == 0);
        }


        public static void vHeartbeatThread()
        {

            DateTime dtNow = DateTime.Now;
            if(dtNow.Second != iHeartbeatSecond && iLoginPhase > 0){ 
                vSendHeartbeat();
                iHeartbeatSecond = dtNow.Second;
            }     
        }

        public static void InCommingProcess()
        {
                // Process incoming socket packets

                string sMethodName = "Process";
                // Incoming data from the client.
                char end_of_file_tag = '^';
                string data = null;
                //Alveo3Client SC = new Alveo3Client();
                //Message Mess = new Message();
                // Data buffer for incoming data.
                byte[] bytes;
                int iCurrentMinute = -1;

                string sErrorDetails2 = string.Format("Process()");
                //string sErrorType2 = "Debug";
                //TestMetastreetServerClient.Logger(TestMetastreetServerClient.EventLevel.Debug, sClassName, sMethodName, sErrorType2, sErrorDetails2);
                //Debug.Log( sMethodName+" "+ sErrorDetails2);
                bool bIsConnected = SocketConnected(aos_socket);
                //Debug.Log("Process aos_socket "+aos_socket+" "+ bIsConnected);

                if (!bIsConnected) {
                    //bConnected = false;
                    Debug.Log("Process socket not connected");
                } else{
                    bConnected = true;
                }

                if (bConnected)
                {
                    sErrorDetails2 = string.Format("Process Connected");
                    //Debug.Log( sMethodName+" "+ sErrorDetails2);
                   
                    NetworkStream networkStream = aos_socket.GetStream();
                    aos_socket.ReceiveTimeout = 9000; // 10000 miliseconds
                    int at = 0;
                    //int iEOFLen = end_of_file_tag.Length;
                    char separatingString = end_of_file_tag;

                    while (ContinueProcess)
                    {
                        DateTime dtNowUTC = DateTime.UtcNow;
                        //Debug.Log("Start Process " + dtNowUTC);
                       //Debug output for heartbeats and ticks proceesed each minute
                       if(dtNowUTC.Minute != iCurrentMinute){
                            //if(iTickProcessThisMinute != 0 && iBeatsReceivedThisMinute != 0){                            
                                //Debug.Log(" Tick Received "+iTickProcessThisMinute+" Beats rec "+iBeatsReceivedThisMinute);
                                iTickProcessThisMinute = 0;
                                iBeatsReceivedThisMinute = 0;
                                iBeatsSentThisMinute = 0;
                                iProcessCycles = 0;
                                iProcessMessageExit = 0;
                                iProcessMessageStarted = 0;
                            //} else{
                            //    Debug.Log("No incoming heats or ticks");
                            //    iBeatsSentThisMinute = 0;
                            //}
                            iCurrentMinute = dtNowUTC.Minute;
                        }
                        iProcessCycles++;

                        //Debug.Log("Process Loop");
                        bIsConnected = SocketConnected(aos_socket);
                        if(!bIsConnected)
                        {
                            ContinueProcess = false;
                            Debug.Log("Socket Disconnected InCommingProcess");
                        }

                        if(iLoginPhase < 4){
                            if(iLoginPhase == 3 && dtLoginStartTime.AddSeconds(2) < DateTime.Now){
                                if(!bOrderHistorySent){

                                    //vSendAccountBalanceRequest();
                                    vSendOrderHistoryRequest();
                                    bOrderHistorySent = true;
                                }
                                iLoginPhase = 4;
                                //TradingPlatform.vActivateTradingButtons();
                            }
                            if(iLoginPhase == 2 && dtLoginStartTime.AddSeconds(2) < DateTime.Now){
                                vSendAccountListRequest();
                                iLoginPhase = 3;
                            }
                            if(iLoginPhase == 1) Debug.Log("dtLoginStartTime "+dtLoginStartTime + " DateTime.Now "+ DateTime.Now);
                            if(iLoginPhase == 1 && dtLoginStartTime.AddMilliseconds(30) < DateTime.Now){
                                vSendSymbolRequest();
                                iLoginPhase = 2;
                            }
                        }

                        bytes = new byte[aos_socket.ReceiveBufferSize];
                        try
                        {
                            int BytesRead = networkStream.Read(bytes, 0, (int)aos_socket.ReceiveBufferSize);
                            if(BytesRead >60000){
                                Debug.Log("Warning high number of byte read in buffer "+BytesRead);
                            }
                            if (BytesRead > 0)
                            {
                                data = Encoding.ASCII.GetString(bytes, 0, BytesRead);
                                //Debug.Log("Received message "+ data);
                                    at = data.IndexOf(end_of_file_tag, 0, data.Length);
                                    int iEnd = at;
                                    int iLen2 = iEnd;
                                    //Split this record and add to list
                                    string[] sMessages = data.Split(separatingString);

                                    foreach (var sMessage1 in sMessages)
                                    {
                                        string sErrorDetails = "";
                                        string sErrorType = "";
                                        string sErrorDetails3 = string.Format("sMessage {0}", sMessage1);
                                        //Debug.Log(sErrorDetails3);
                                        bool bMessageError = false;
                                        if(sMessage1.Length ==0){
                                            sErrorDetails = string.Format("Error message zero length ");
                                            bMessageError = true;
                                        }
                                        //Debug.Log("sMessage1.Length "+sMessage1.Length+" sMessage1 "+sMessage1);
                                        if(sMessage1.Length <= 2) break;
                                        //string sMessage = DecryptAndClean(sMessage1);
                                        string sMessage = sMessage1;
                                        //Debug.Log("Decrypted "+sMessage);
                                      
                                        if (!sMessage.Contains("999") && !sMessage.Contains("411") && 
                                          !sMessage.Contains("200") && !sMessage.Contains("221") && !sMessage.Contains("120") 
                                            && !sMessage.Contains("123") && !sMessage.Contains("124"))
                                        {
                                            sErrorDetails = string.Format("sMessage {0}", sMessage);
                                            sErrorType = "Debug";
                                            //Debug.Log( "Received "+sMethodName+" "+ sErrorDetails);
                                        }
                                         //Debug.Log( sMethodName+" "+ sErrorDetails3);
                                        if (!sMessage.Contains("Type") && !bMessageError)
                                        {
                                            bMessageError = true;
                                            sErrorDetails = string.Format("Error message does not contain Type sMessage {0} {1}",sMessage,sMessage.Length);
                                            sErrorType = "Error";
                                            //Debug.Log("Error "+ sErrorDetails );
                                         }
                                        if (!sMessage.Contains("}}") && !bMessageError)
                                        {
                                            bMessageError = true;
                                            sErrorDetails = string.Format("Error message does not contain EOF {0}",sErrorDetails3);
                                            sErrorType = "Error";
                                        }


                                        if(!bMessageError)
                                        {
                                             string sReply = "";
                                            Message aMessage = null;
                                            try
                                            {
                                                aMessage = Message.FromJsonString(sMessage);

                                                try{
                                                    //new Thread(() =>
                                                    //{
                                                        sReply = ProcessMessage(aMessage);

                                                    //}).Start();
                                                }
                                                catch (Exception e)
                                                {
                                                    sErrorDetails = string.Format("Error Creating thread ProcessMessage");
                                                    sErrorType = e.ToString();
                                                    TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                                                }
                                               
                                                Byte[] reply = System.Text.Encoding.ASCII.GetBytes(sReply);
                                            }
                                            catch (Exception e)
                                            {
                                                sErrorDetails = string.Format("Exception-1 sMessage {0} Data {1}", sMessage, data);
                                                sErrorType = e.ToString();
                                                TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                                            }

                                        }
                                    }
                                
                                //if (data == "quit") break;
                            }
                        }
                        catch (IOException) { } // Timeout
                        catch (SocketException)
                        {
                            Debug.Log("Connection is broken! Exit thread");
                            bConnected = false;
                            ContinueProcess = false;
                            bIsConnected = false;
                            vSendExitMessage();
                        }
                        //Debug.Log("Exit Process");
                        System.GC.Collect(0);
                        Thread.Sleep(1);
                    }//End while

                }
                //Debug.Log(sClassName+" "+sMethodName+" End Loop");
                return ;
            }  // Process()

        public static string DecryptAndClean(string sEncrypted)
        {
            string sMessage = AesOperation.DecryptString( sEncrypted);
            //Debug.Log("Decryptand clean sMessage "+sMessage);
            string sCleaned = sMessage.Replace(@"\", @"");
            return sCleaned;
        }

        static public void vOpenNewChart(string sChartSymbol,string sChartTimeframe,int iNumberOfBars){
            //Debug.Log("Processor vOpenNewChart iNumberOfBars "+iNumberOfBars);
            TradingPlatform.instance.vOpenNewChart(sChartSymbol, sChartTimeframe, iNumberOfBars);
            iReceivedBars = 0;
        }
#region Sends
            static private void vSendHeartbeat()
            {
                //Debug.Log("Send Heartbeat");
                bool bSendTime = false;
                string sMethodName = "vSendHeartbeat";
                try
                {
                    DateTime dtNow = DateTime.Now;
                    if(iHeartbeatMinute != dtNow.Minute){
                        bSendTime = true;
                        iHeartbeatMinute = dtNow.Minute;
                    }
                    if (bSendTime)
                    {
                        TimeSpan timeSpan = DateTime.Now - TradingPlatform.dt1970;
                        long lCurrentTimeUnix = (long)timeSpan.TotalMilliseconds;
                        string sHeartbeat = Message.HeartbeatRequest(lCurrentTimeUnix);
                        //string sSendMessage = JsonConvert.SerializeObject(mHeartbeat);
                        //sSendMessage = sSendMessage + end_of_file_tag;
                        sSend_msg(sHeartbeat);
                    } else{
                        string sHearbeat = Message.HeartbeatRequest();
                        sSend_msg(sHearbeat);
                    }
                     iBeatsSentThisMinute++;                  
                    dtLastHeartbeatSent = dtNow;
                }
                catch (Exception e)
                {
                    string sErrorDetails = string.Format("Outer Exception");
                    string sErrorType = e.ToString();
                    TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                }
                return;

            }

            static void vSendOrderHistoryRequest(){
                if(!bOrderHistorySent){
                    Message cMessage3 = new Message();
                    iHistoricOrdersRequest = 1500;
                    string sOrdersRequest = cMessage3.OrdersRequest(TradingPlatform.iUID,iHistoricOrdersRequest);
                    OrderHistoryManager OHM = new OrderHistoryManager();
                    OpenOrderManager OOM = new OpenOrderManager();              
                    OHM.vClearList();
                    OOM.vClearList();
                    sSend_msg(sOrdersRequest);
                    bOrderHistorySent = true;
                    TradingPlatform.bAllOrdersLoad = false;
                }
            }

            static void vSendAccountListRequest(){
                Message cMessage2 = new Message();
                //Request the current trading accounts
                string sAccountsRequest = cMessage2.AccountsRequest(TradingPlatform.iUID);
                sSend_msg(sAccountsRequest);
            }

            static void vSendAccountBalanceRequest(){
                Message cMessage2 = new Message();
                //Request the current active trading account balance
                string sAccountsRequest = cMessage2.AccountBalanceRequest();
                sSend_msg(sAccountsRequest);

            }
            static void vSendSymbolRequest(){
                Message cMessage = new Message();
                string sSymbolRequest = cMessage.SymbolListRequest(TradingPlatform.iUID);
                sSend_msg(sSymbolRequest);
            }
            static public void vSendExitMessage(){
                Message cMessage = new Message();
                string sExitRequest = cMessage.ExitRequest();
                sSend_msg(sExitRequest);
                //Allow 1 second for the message to be processed by the server and then exit
                Thread.Sleep(1000);
                TradingPlatform.bExit = true;
            }

 
            static public void vSendHistoryBarRequest(string sID,string sSymbol,string sTimeFrame ,int INumberOfBars){
                
                //string sTimeFrame = eTimeframe.ToString();
                if(INumberOfBars < 100 ) {
                //Debug.Log("vSendHistoryBarRequest INumberOfBars "+INumberOfBars);
                    INumberOfBars = 100;
                }
                //Debug.Log("vSendHistoryBarRequest ID"+sID+ " Symbol "+sSymbol+" iTimeframe "+ sTimeFrame +" Bars "+INumberOfBars);
                Message cMessage = new Message();
                string sBarRequest = cMessage.HistoricalRequest(sID, sSymbol,  sTimeFrame, INumberOfBars);
                sSend_msg(sBarRequest);
                //SingleChart.vInitBarList();

            }

            static public void vSendSubscribeMessage(string sSymbol)
            {
                
                //string sMethodName = "vSendSubscribeMessage";
                try{
                    string sSubscribe = Message.SubscribeRequest(TradingPlatform.iUID,sSymbol);
                    sSend_msg(sSubscribe);
                    //Add a new entry to the list of quotes for this symbol
                    Quote BlankQuote = new Quote();
                    BlankQuote.Symbol = sSymbol;
                    int iSubscribeCount = TradingPlatform.lCurrentSymbolQuote.Count;
                        
                    if (iSubscribeCount == 0)
                    {
                        try{
                            TradingPlatform.lCurrentSymbolQuote.Add(BlankQuote);
                        }
                        catch(Exception ex){
                                Debug.Log("Exception Init symbol quote "+ex.ToString());
                        }
                    }else
                    {
                        try{
                            bool bSymbolFound = false;
                            for (int iIndex = 0; iIndex < iSubscribeCount; iIndex++) {
                                if (TradingPlatform.lCurrentSymbolQuote[iIndex].Symbol == sSymbol)
                                {
                                    bSymbolFound = true;
                                    break;
                                }
                                if(!bSymbolFound) TradingPlatform.lCurrentSymbolQuote.Add(BlankQuote);
                            }
                        }
                        catch(Exception ex){
                            Debug.Log("Exception Init symbol quote "+ex.ToString());
                        }
                    }
                }
                catch(Exception ex){
                    string sErrorDetails = string.Format("Exception-1");
                    string sErrorType = ex.ToString();
                    Debug.Log("vSendSubscribeMessage "+sErrorType);
                    //TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
               }
            }

            static public void vSendNewOrder(Order cOrder){
                Message mOrder = Message.NewOrderRequest(cOrder);
                string sOrder = mOrder.ToString();
                sSend_msg(sOrder);
            }
            static public void vCloseOrder(int iOID){
                for(int iOpenOrderRec = 0; iOpenOrderRec < TradingPlatform.lOpenOrders.Count; iOpenOrderRec++ ){
                    if(TradingPlatform.lOpenOrders[iOpenOrderRec].Id == iOID){
                        Order cOrder = TradingPlatform.lOpenOrders[iOpenOrderRec];
                        cOrder.CloseType = Order.eCloseType.Market;
                        Message mOrder = Message.CloseOrderRequest(cOrder);
                        string sOrder = mOrder.ToString();
                        sSend_msg(sOrder);
                        break;
                    }

                }
            }     

            static public void vSendUnsubscribeMessage(string sSymbol){
                Message cMessage = new Message();
                string sSymbolRequest = cMessage.UnsubscribeRequest(sSymbol);
                sSend_msg(sSymbolRequest);
               
            }
#endregion Sends

            static public string ProcessMessage(Message sMessage)
            {
                
                //Error = -1,
                //Action = 100,
                //Login = 115,
                //UpdateOrder = 116,
                //NewOrder = 117,
                //ModifyOrder = 118,
                //CloseOrder = 119,
                //Subscribe = 120,
                //Unsubscribe = 121,
                //NewQuote = 123,
                //Historical = 124,
                //SymbolList = 411,
                //Heartbeat = 999

                iProcessMessageStarted++;
                try{
                    if(sMessage.Type < 100){
                        Debug.Log("Invalid message type "+sMessage);
                    }
                }
                catch{
                    return "";
                }
                
                string sMethodName = "ProcessMessage";
                string sReply = "";
                try
                {
                    string sErrorDetails = "";
                    string sErrorType = "";
                //Debug.Log("ProcessMessage sMessage "+sMessage);
                    if (sMessage.Type != 200 && sMessage.Type != 124 && sMessage.Type != 411)
                    {
                        sErrorDetails = string.Format("Message Type {0}", sMessage.Type);
                        sErrorType = "Debug";
                        //TestMetastreetServerClient.Logger(TestMetastreetServerClient.EventLevel.Debug, sClassName, sMethodName, sErrorType, sErrorDetails);
                    }
                    //if (sMessage.Type != 999) Console.WriteLine("ProcessMessage Message Type {0}", sMessage.Type);
                    //Determine the message type to send to correct function to process
                    switch (sMessage.Type)
                    {
                        case 100:
                            //Action
                            break;
                        case 115:
                            //Login
                            bool bSuccess = bProcessLoginResponse(sMessage);
                            break;
                        case 116:
                            Debug.Log("ProcessMessage Received order 116 message "+sMessage);
                            vProcessOrderUpdate(sMessage);
                            break;
                        case 117:
                        case 118:
                        case 119:
                            //116 Update Order
                            //117 New Order
                            //118 Modify Order
                            //119 Close Order
                            //sReply = sProcessOrders(sMessage);
                            break;
                        case 120:
                        //Debug.Log("Quote "+sMessage);
                            new Thread(() =>
                            {
                                sReply = sProcessSubscribe(sMessage);
                            }).Start();
                            //dtShutDownSocket = DateTime.Now.AddHours(1);
                            break;

                        case 121:
                            //Subscribe and unsubscribe symbols
                            //vProcessUnSubscribe(sMessage);
                            //dtShutDownSocket = DateTime.Now.AddHours(1);
                            break;
                        case 122:
                            break;
                        case 123:
                            //Process received quote
                            try{
                                new Thread(() =>
                                {
                                    //Debug.Log("process new quote "+sMessage);
                                    vProcessNewQuote(sMessage);
                                }).Start();
                            }
                            catch (Exception e)
                            {
                                Debug.Log("Error "+e);
                                //sErrorDetails = string.Format("Error Creating thread vProcessNewQuote");
                                //sErrorType = e.ToString();
                                //Alveo3Client.Logger(Alveo3Client.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                            }
                            break;
                        case 124:
                            //122 OldHistorical
                            //124 Historical
                            //new Thread(() =>
                            //{
                                //Debug.Log("process new Bars");
                                sProcessHistoryBarResponse(sMessage);
                            //}).Start();
                            break;

                        case 200:
                            // trading accounts
                             new Thread(() =>
                            {
                                vProcessAccount(sMessage);

                            }).Start();
                           //vProcessAccount(sMessage);
                            break;
                        case 201:
                        //Account daily balance
                            vProcessDailyBalance(sMessage);
                            break;
                        case 221:
                            //Receive Orders for list
                            try{
                                new Thread(() =>
                                {
                                    //Processor Proc = new Processor();
                                    vProcessHistoricOrder(sMessage);
                                }).Start();
                            }
                            catch (Exception e)
                            {
                                sErrorDetails = string.Format("Error Creating thread vProcessHistoricOrder");
                                sErrorType = e.ToString();
                                TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                            }
                            //vProcessHistoricOrder(sMessage);

                            break;
                        case 411:
                            //SymbolList
                            try{
                                //new Thread(() =>
                               // {
                                    vProcessSymbolList(sMessage);
                                //}).Start();
                            }
                            catch (Exception e)
                            {
                                sErrorDetails = string.Format("Error Creating thread vProcessSymbolList");
                                sErrorType = e.ToString();
                                TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                            }
                            break;

                        case 999:
                            //Heartbeat
                            //Debug.Log("Added processor back in");
                            vProcessHeartbeat(sMessage);
                            break;
                        default:
                            // unhandled message type 
                            // create a type -1 return message type error
                            sErrorDetails = string.Format("Switch invalid type {0}", sMessage.Type);
                            sErrorType = "Invalid field";
                            TradingPlatform.Logger(TradingPlatform.EventLevel.Warn, sClassName, sMethodName, sErrorType, sErrorDetails);
                            break;
                    }
                }
                catch (Exception e)
                {
                    string sErrorDetails = string.Format("Exception sMessage {0}", sMessage);
                    string sErrorType = e.ToString();
                    Debug.Log("Exception in received message "+sMessage);
                    TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                }
                //GC.Collect(0);
                iProcessMessageExit++;
                return (sReply);
            }

            static private void vProcessOrderUpdate(Message sMessage){
               string sMethodName = "sProcessOrderUpdate";
                string sOrderRecord = "";
                string ssMessage = sMessage.ToString();
                try
                {
                    var container = (JContainer)sMessage.Args["Order"];
                    sOrderRecord = container.ToString();
                    //Debug.Log("Order Update sOrderRecord "+sOrderRecord);
                    Order cOrderRecord1 = JsonConvert.DeserializeObject<Order>(sOrderRecord);
                    Debug.Log("Order Update OID "+cOrderRecord1.Id);
                    OpenOrderManager.vProcessOrderUpdate(cOrderRecord1);
                    try{
                        if((int)cOrderRecord1.Id > TradingPlatform.iMaxOID) TradingPlatform.iMaxOID = (int)cOrderRecord1.Id;
                    }
                    catch (Exception e)
                    {
                        string sErrorDetails = string.Format("Exception failed if cOrderRecord1.Id {0} TradingPlatform.iMaxOID {1}", cOrderRecord1.Id,TradingPlatform.iMaxOID);
                        string sErrorType = e.ToString();
                        Debug.Log(sMethodName + " Error "+sErrorDetails+" "+sErrorType);
                        //TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                    }
                    bool bFoundRecord = false;
                    if(TradingPlatform.lOpenOrders.Count > 0){
                        for(int iRec =0; iRec < TradingPlatform.lOpenOrders.Count; iRec++){
                            try{
                                if(TradingPlatform.lOpenOrders[iRec].Id ==  cOrderRecord1.Id) {
                                    TradingPlatform.lOpenOrders[iRec] = cOrderRecord1;
                                    if((int)cOrderRecord1.Status > 20){
                                        Debug.Log("Order close remove");
                                        TradingPlatform.lOpenOrders.RemoveAt(iRec);
                                        TradingPlatform.lHistoricOrders.Insert(0,cOrderRecord1);
                                    }
                                    bFoundRecord = true;
                                    break;
                                }
                            }
                            catch (Exception e)
                            {
                                string sErrorDetails = string.Format("Exception failed if TradingPlatform.lOpenOrders[iRec].Id {0} TradingPlatform.iMaxOID {1}",
                                    TradingPlatform.lOpenOrders[iRec].Id,cOrderRecord1.Id);
                                string sErrorType = e.ToString();
                                Debug.Log(sMethodName + " Error "+sErrorDetails+" "+sErrorType);
                                //TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                            }

                        }
                        Debug.Log(sMethodName+ " bFoundRecord "+bFoundRecord);
                        //if(!bFoundRecord) TradingPlatform.lOpenOrders.Add(cOrderRecord1);
                        if(!bFoundRecord) TradingPlatform.lOpenOrders.Add(cOrderRecord1);

                    } else{
                        TradingPlatform.lOpenOrders.Add(cOrderRecord1);
                        //TradingPlatform.lTodaysOrders.Add(cOrderRecord1);
                    }
                }
                catch (Exception e)
                {
                    string sErrorDetails = string.Format("Exception sMessage {0}", sMessage);
                    string sErrorType = e.ToString();
                    Debug.Log(sMethodName + " Error "+sErrorDetails+" "+sErrorType);
                    //TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                }
            }
            static private void sProcessHistoryBarResponse(Message sMessage)
            {
                //string sMethodName = "sProcessHistoryBarResponse";
                string sSymbol1 = (string)sMessage.Args["Symbol"];
                string sID = (string)sMessage.Args["ID"];
                bool bLast = false;
                string ssMessage = sMessage.ToString();
                var container = (JContainer)sMessage.Args["Bar"];
                iReceivedBars++;
                //if(iReceivedBars >= iRequestedBars) bLast = true;
                string sBar = container.ToString();
                Bar bar = JsonConvert.DeserializeObject<Bar>(sBar);
                if(bar.Open != -1) {
                    TradingPlatform.vAddNewBar(bar, bLast, sID);
                } else{
                    bLast = true;
                }
            }
            static private void vProcessSymbolList(Message sMessage)
            {
                string sMethodName = "vProcessSymbolList";
                //Debug.Log("vProcessSymbolList Message "+sMessage);
                try
                {
                    var container = (JContainer)sMessage.Args["Sym"];
                    string sInstrument = container.ToString();
                    //Debug.Log("vProcessSymbolList sInstrument "+sInstrument);
                    dynamic jOrder = JObject.Parse(sInstrument);
                    Instrument cInstrument1 = new Instrument();
                    cInstrument1 = JsonConvert.DeserializeObject<Instrument>(sInstrument);
                    //Debug.Log("vProcessSymbolList cInstrument1 Comission "+cInstrument1.Commission);
                    TradingPlatform.lInstruments.Add(cInstrument1);
                    //TradingPlatform.lInstruments.Sort();
                    if(cInstrument1.Enabled){
                        int iCount = TradingPlatform.lActiveSymbols.Count();
                        string sCurrentSymbol = cInstrument1.Symbol;
                        //Check if this symbol is already on the list
                        bool bFoundSymbol = false;
                        for(int iSymbolIndex = 0; iSymbolIndex < iCount; iSymbolIndex++){
                            if(sCurrentSymbol == TradingPlatform.lActiveSymbols[iSymbolIndex]){
                                bFoundSymbol = true;
                                break;
                            }
                        }
                        if(!bFoundSymbol){
                            TradingPlatform.lActiveSymbols.Add(sCurrentSymbol);
                            TradingPlatform.lActiveSymbols.Sort((left, right) => left.CompareTo(right));
                            TradingPlatform.lActiveInstruments.Add(cInstrument1);
                            try{
                                Quote BlankQuote = new Quote();
                                BlankQuote.Symbol = sCurrentSymbol;
                                bool bSymbolFound = false;
                                int iQuoteCount = TradingPlatform.lCurrentSymbolQuote.Count;
                                for (int iIndex = 0; iIndex < iQuoteCount; iIndex++) {
                                    if (TradingPlatform.lCurrentSymbolQuote[iIndex].Symbol == sCurrentSymbol)
                                    {
                                        bSymbolFound = true;
                                        break;
                                    }
                                }
                                if(!bSymbolFound){
                                    TradingPlatform.lCurrentSymbolQuote.Add(BlankQuote);
                                }
                                    //TradingPlatform.lCurrentSymbolQuote.Sort((left, right) => left.CompareTo(right));
                                //}
                            }
                            catch(Exception ex){
                                Debug.Log("Exception Init symbol quote "+ex.ToString());
                            }
                        }
                    }

                    //Debug.Log(cInstrument1.Symbol +" "+ cInstrument1.Enabled+" "+TradingPlatform.lInstruments.Count+1);
                }
                catch (Exception e)
                {
                    string sErrorDetails = string.Format("Exception sMessage {0}", sMessage);
                    string sErrorType = e.ToString();
                    Debug.Log(sMethodName+ " Error "+sErrorDetails);
                    //TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                }
            }
            static private void vProcessAccount(Message sMessage)
            {
                string sMethodName = "vProcessAccount";
                string sTradingAccount = "";
                //Debug.Log("vProcessAccount "+sMessage);
                try
                {
                    var container = (JContainer)sMessage.Args["Account"];
                    sTradingAccount = container.ToString();
                    cTradingAccount cTradingAccount1 = JsonConvert.DeserializeObject<cTradingAccount>(sTradingAccount);

                    //Debug.Log("Account id {0} Active {1} Name {2}", cTradingAccount1.iAID, cTradingAccount1.bActive, cTradingAccount1.sName);
                    TradingPlatform.lTradingAccounts.Add(cTradingAccount1);
                    /*
                    if(cTradingAccount1.bActive ){
                        Debug.Log("Update active account");
                        TradingPlatform TP = new TradingPlatform();
                        TP.vSetActiveAccount(cTradingAccount1.iAID);
                    }
                    */
                }
                catch (Exception e)
                {
                    string sErrorDetails = string.Format("Exception sMessage {0}", sMessage);
                    string sErrorType = e.ToString();
                    TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                }

            }
            static private void vProcessDailyBalance(Message sMessage){
                //{"Type":201,"Args":{"AccountDailyBalance":{"AID":287506,"AccountBalance":9499.53,"dtEffectiveDate":"2021-09-15T00:00:00Z"},"UMID":"2e37437e-503b-42cd-97e8-eaf842075a1a","Hash":"E997E43A3842C9D12CF63FD8648DC709"}}

                string sMethodName = "vProcessDailyBalance";
                //Debug.Log("vProcessDailyBalance "+sMessage);
                string sAccountBalance = "";
                try{
                    var container = (JContainer)sMessage.Args["AccountDailyBalance"];
                    sAccountBalance = container.ToString();
                    DailyBalanceClass cAccountBalance = JsonConvert.DeserializeObject<DailyBalanceClass>(sAccountBalance);
                    //Debug.Log("Account id "+cAccountBalance.AID +" Balance "+ cAccountBalance.AccountBalance+" Date " + cAccountBalance.dtEffectiveDate );
                    MoneyManagement.cCurrentAccountOpenBalance = cAccountBalance;
                    MoneyManagement.vSetCurrentBalance(cAccountBalance.AccountBalance);
                    MoneyManagement.vSetMaxPosition();
                    // if(cAccountBalance.AID ){
                    //Debug.Log("Update active account");
                    //TradingPlatform TP = new TradingPlatform();
                    //TP.vSetActiveAccount(cAccountBalance.AID);
                    //}
                }
                catch (Exception e)
                {
                    string sErrorDetails = string.Format("Exception sMessage {0}", sMessage);
                    string sErrorType = e.ToString();
                    TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                }

            }
            static private void vProcessHeartbeat(Message aMessage){
                string sMethodName = "vProcessHeartbeat";
                //Message aMessage = Message.FromJsonString(sMessage);
                //Debug.Log("Process Received heartbeat");
               
                try
                {
                    DateTime dtNow = DateTime.Now;
                    //string sHeartbeat = sMessage;
                    //Debug.Log("vProcessHeartbeat "+sHeartbeat+" "+dtNow);

                    //if(dtLastHeartbeatSent.AddMilliseconds(100) < dtNow)
                    //{
                        //vSendHeartbeat();
                        //dtLastHeartbeatSent = dtNow;
                    //}

                    iBeatsReceivedThisMinute++;
               }
                catch (Exception e)
                {
                    string sErrorDetails = string.Format("Exception sMessage {0}", aMessage);
                    string sErrorType = e.ToString();
                    Debug.Log(TradingPlatform.EventLevel.Error+" " +sClassName+" "+ sMethodName+" "+sErrorType+" "+ sErrorDetails);
                }

                 //Debug.Log("Exit "+sMethodName);
                   
            }
            static private void vProcessHistoricOrder(Message sMessage)
            {
                string sMethodName = "vProcessHistoricOrder";
                string sOrderRecord = "";
                bool bLastOrder = false;
                try
                {
                    var container = (JContainer)sMessage.Args["Order"];
                    sOrderRecord = container.ToString();
                    Order cOrderRecord1 = JsonConvert.DeserializeObject<Order>(sOrderRecord);
                    cOrderRecord1.Profit = Math.Round(cOrderRecord1.Profit,2);
                    if((int)cOrderRecord1.Id > TradingPlatform.iMaxOID) TradingPlatform.iMaxOID = (int)cOrderRecord1.Id;
                    //Check if order id = -1 
                    if(cOrderRecord1.Id == -1){
                        //Last order received
                        try{
                            iOrdersLoaded = iHistoricOrdersRequest;
                            bLastOrder = true;
                            MoneyManagement.vComputeTodaysCloseOrders();
                        }
                        catch (Exception e)
                        {
                            string sErrorDetails = string.Format("Exception cOrderRecord1.Id == -1");
                            string sErrorType = e.ToString();
                            Debug.Log(sMethodName + " Error "+sErrorDetails+" "+sErrorType);
                            //TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                        }

                    }
                    iOrdersLoaded++;
                    double dPercentOrdersReceived = (double)iOrdersLoaded/iHistoricOrdersRequest;
                    //if( (iHistoricOrdersRequest - iOrdersLoaded) <= 3)  Debug.Log("Request Orders received "+dPercentOrdersReceived*100.0+"% " +iOrdersLoaded);
                    if(dPercentOrdersReceived >= 1.00) {
                        //Debug.Log("All Request Orders received "+dPercentOrdersReceived);
                        TradingPlatform.bAllOrdersLoad = true;
                        TradingPlatform.bRefreshOrderTables = true;
                        //MoneyManagement.vComputeTodaysCloseOrders();
                    }
                    if(iOrdersLoaded > iHistoricOrdersRequest || bLastOrder){
                        //Ignore the extra orders, used to insure enough orders ar loaded
                        return;
                    }
                    try
                    {
                        if((int)cOrderRecord1.Status < 20){
                            //TradingPlatform.lTodaysOrders.Add(cOrderRecord1);
                            TradingPlatform.lOpenOrders.Add(cOrderRecord1);
                        }else{
                            DateTime dtNow = DateTime.UtcNow;
                            TradingPlatform.lHistoricOrders.Add(cOrderRecord1);
                            /*
                            if(cOrderRecord1.CloseDate.Day == dtNow.Day ){
                                if(cOrderRecord1.CloseDate.Month == dtNow.Month ){
                                    if(cOrderRecord1.CloseDate.Year == dtNow.Year ){
                                        try{
                                            TradingPlatform.lTodaysOrders.Add(cOrderRecord1);
                                        }
                                        catch (Exception e)
                                        {
                                            string sErrorDetails = string.Format("Exception vComputeTodaysCloseOrders");
                                            string sErrorType = e.ToString();
                                            Debug.Log(sMethodName + " Error "+sErrorDetails+" "+sErrorType);
                                            //TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                                        }
                                    }
                                }
                            }
                            */
                        }
                    }
                    catch (Exception e)
                    {
                        string sErrorDetails = string.Format("Exception TradingOrders.Add");
                        string sErrorType = e.ToString();
                        Debug.Log(sMethodName + " Error "+sErrorDetails+" "+sErrorType);
                        //TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                    }
                }
                catch (Exception e)
                {
                    string sErrorDetails = string.Format("Exception sMessage {0}", sMessage);
                    string sErrorType = e.ToString();
                    Debug.Log(sMethodName + " Error "+sErrorDetails+"\n"+sErrorType);
                    //TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                }

            }


            static private string sProcessSubscribe(Message sMessage)
            {
                try{
                    //{ "Type":120,"Args":{ "Quote":{ "Symbol":"EUR/USD","Price":1.17096,"Volume":250.0,"Ask":1.17098,"Bid":1.17096,
                    //                "Date":1629293858391,"Sent":0},"UMID":"ee15190e-3fd4-4619-b381-eeab2ff52cd8",
                    //                "Hash":"A9D73AFE6261BE8D185AFF20E17F1F56"} }
                    var container = (JContainer)sMessage.Args["Quote"];
                    string sQuote = container.ToString();
                    Quote xQuote = new Quote();
                    xQuote = JsonConvert.DeserializeObject<Quote>(sQuote);
                    if(TradingPlatform.lCurrentSymbolQuote.Count == 0){
                        TradingPlatform.lCurrentSymbolQuote.Add(xQuote);
                    }
                }
                catch (Exception e)
                {
                    string sErrorDetails = string.Format("Exception sMessage {0}", sMessage);
                    string sErrorType = e.ToString();
                    Debug.Log("sProcessSubscribe Error "+sErrorDetails+" "+sErrorType);
                    //TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                }
                //Debug.Log("sProcessSubscribe xQuote "+xQuote.Symbol+" bid "+xQuote.Bid+" ask "+ xQuote.Ask);
                return "";

            }
            static private void vProcessNewQuote(Message sMessage)
            {
                var container = (JContainer)sMessage.Args["Quote"];
                string sQuote = container.ToString();
                Quote xQuote = new Quote();
                xQuote = JsonConvert.DeserializeObject<Quote>(sQuote);
                xQuote.Time = dt1970.AddSeconds(xQuote.Date / 1000.0);
                double dTimeSeconds = xQuote.Date/1000.0;
                iTickProcessThisMinute++;

                if(TradingPlatform.lCurrentSymbolQuote.Count > 0){

                    for(int iQuote=0; iQuote < TradingPlatform.lCurrentSymbolQuote.Count; iQuote++){
                        if(TradingPlatform.lCurrentSymbolQuote[iQuote].Symbol == xQuote.Symbol && xQuote.Ask>0.1 && xQuote.Bid> 0.1){
                            //if( xQuote.Symbol=="EUR/USD" )Debug.Log("vProcessNewQuote Symbol "+xQuote.Symbol+" Bid "+xQuote.Bid);
                            TradingPlatform.lCurrentSymbolQuote[iQuote].Ask = xQuote.Ask;
                            TradingPlatform.lCurrentSymbolQuote[iQuote].Bid = xQuote.Bid;
                            TradingPlatform.lCurrentSymbolQuote[iQuote].Volume = xQuote.Volume;
                            TradingPlatform.lCurrentSymbolQuote[iQuote].Sent = xQuote.Sent;
                            TradingPlatform.lCurrentSymbolQuote[iQuote].Date = xQuote.Date;
                            TradingPlatform.lCurrentSymbolQuote[iQuote].Price = xQuote.Price;
                            //Debug.Log("xQuote.Symbol "+xQuote.Symbol+" TradingPlatform.lCurrentSymbolQuote[iQuote].Symbol "+TradingPlatform.lCurrentSymbolQuote[iQuote].Symbol+" iQuote "+iQuote);
                            break;
                        }
                    }
                }
                
                int iNumberCharts = TradingPlatform.lCharts.Count;
                if(iNumberCharts>0){
                    TradingPlatform.instance.vUpdateBarZero(xQuote);
                }
            }

            static private bool bProcessLoginResponse(Message sMessage)
            {
                //Debug.Log("bProcessLoginResponse sMessage "+sMessage);
                bool bSuccess = false;
                bSuccess = (bool)sMessage.Args["Success"];
                TradingPlatform.iUID = (long)sMessage.Args["UID"];

                if (bSuccess)
                {
                    iLoginPhase = 1;
                    dtLastHeartbeatSent = DateTime.Now;
                    dtLoginStartTime = dtLastHeartbeatSent;
                    vSendHeartbeat();
                    //Debug.Log("bProcessLoginResponse sMessage "+sMessage+ " bSuccess "+bSuccess);
                }
                return bSuccess;
            }

           public static void sSend_msg(string sMsg)
            {
                //Send order message to the Alveo client
                string sMethodName = "Msend_msg";
                string msg = "";
                try
                {
                    if (aos_socket != null)
                    {
                        NetworkStream stream = aos_socket.GetStream();
                        if (stream.CanWrite)
                        {
                            string sEncrypted = AesOperation.EncryptString(sMsg);
                            msg = sEncrypted + TradingPlatform.end_of_file_tag;

                            if (msg.Length > 5)
                            {
                                string sMessage = msg;
                                //Debug.Log("ssend_msg "+ DateTime.UtcNow+" "+ msg);
                                Byte[] data = System.Text.Encoding.ASCII.GetBytes(sMessage);
                                stream.Write(data, 0, data.Length);
                                stream.Flush();
                                return;
                                //}
                            }
                        }
                        else
                        {
                            string sErrorDetails = string.Format("Client Disconnected");
                            string sErrorType = "Connection";
                            Debug.Log("sSend_msg "+sErrorDetails);
                            TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                        }
                        return;
                    }
                }
                catch (IOException) { } // Timeout
                catch (SocketException)
                {
                    //Console.WriteLine("Conection is broken!");
                    string sErrorDetails = string.Format("Conection is broken! Cannot send_msg");
                    string sErrorType = "Broken pipe";
                    TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                }
                catch (Exception e)
                {

                    string sErrorType = e.ToString();
                    if (sErrorType.Contains("disposed object") || sErrorType.Contains("non-connected"))
                    {
                        string sErrorDetails = string.Format("Outer Exception Client Disconnected Cannot send_msg");
                        sErrorType = "Broken pipe";
                        //MetastreetTradeSeverCore.Logger(MetastreetTradeSeverCore.EventLevel.Warn, sClassName, sMethodName, sErrorType, sErrorDetails);
                        //ContinueProcess = false;
                    }
                    else
                    {
                        string sErrorDetails = string.Format("Outer Exception Cannot send_msg {0}", msg);
                        TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                        //bConnected = false;
                        //ContinueProcess = false;
                        //}
                        return;
                    }
                }
            }
            public static void Msend_msg(Message mMsg)
            {
                //Send order message to the Alveo client
                string sMethodName = "Msend_msg";
                string msg = "";
                try
                {
                    if (aos_socket != null)
                    {
                        NetworkStream stream = aos_socket.GetStream();
                        if (stream.CanWrite)
                        {
                            msg = JsonConvert.SerializeObject(mMsg);
                            string sEncrypted = AesOperation.EncryptString(msg);
                           
                            msg = sEncrypted  + TradingPlatform.end_of_file_tag;
                            if (msg.Length > 5)
                            {
                                string sMessage = msg;
                                //Console.WriteLine("ssend_msg {0}    {1}", DateTime.UtcNow, msg);
                                Byte[] data = System.Text.Encoding.ASCII.GetBytes(sMessage);
                                stream.Write(data, 0, data.Length);
                                stream.Flush();
                                return;
                                //}
                            }
                        }
                        else
                        {
                            string sErrorDetails = string.Format("Client Disconnected");
                            string sErrorType = "Connection";
                            TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                        }
                        return;
                    }
                }
                catch (IOException) { } // Timeout
                catch (SocketException)
                {
                    //Console.WriteLine("Conection is broken!");
                    string sErrorDetails = string.Format("Conection is broken! Cannot send_msg");
                    string sErrorType = "Broken pipe";
                    TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                }
                catch (Exception e)
                {

                    string sErrorType = e.ToString();
                    if (sErrorType.Contains("disposed object") || sErrorType.Contains("non-connected"))
                    {
                        string sErrorDetails = string.Format("Outer Exception Client Disconnected Cannot send_msg");
                        sErrorType = "Broken pipe";
                        //MetastreetTradeSeverCore.Logger(MetastreetTradeSeverCore.EventLevel.Warn, sClassName, sMethodName, sErrorType, sErrorDetails);
                        //ContinueProcess = false;
                    }
                    else
                    {
                        string sErrorDetails = string.Format("Outer Exception Cannot send_msg {0}", msg);
                        TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                        //bConnected = false;
                        //ContinueProcess = false;
                        //}
                        return;
                    }
                }
            }
    }
}