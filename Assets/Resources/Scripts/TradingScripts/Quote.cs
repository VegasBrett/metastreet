using System;
using UnityEngine;
using Newtonsoft.Json;
namespace Alveo3_Client
{
    public class Quote 
    {
            [JsonProperty("Price")]
            public double Price { get; set; }

            [JsonProperty("Volume")]
            public double Volume { get; set; }

            [JsonProperty("Symbol")]
            public string Symbol { get; set; }

            [JsonProperty("Ask")]
            public double Ask { get; set; }

            [JsonProperty("Bid")]
            public double Bid { get; set; }

            [JsonProperty("Date")]
            public Int64 Date { get; set; }

            [JsonProperty("Sent")]
            public Int64 Sent { get; set; }
            [JsonProperty("Time")]
            public DateTime Time { get; set; }
    }
}
