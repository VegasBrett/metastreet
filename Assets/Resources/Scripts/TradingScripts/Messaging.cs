using Newtonsoft.Json;
using UnityEngine;

namespace Alveo3_Client
{
        public class Messaging
        {
                [JsonProperty("message")]
                public string message { get; set; }

                [JsonProperty("messageLink")]
                public string messageLink { get; set; }

                [JsonProperty("elevate")]
                public int elevate { get; set; }

                [JsonProperty("maintenance")]
                public int maintenance { get; set; }
        }
}
