using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alveo3_Client
{
    public class cTradingAccount
    {
            //Data fields used for each trading account
            public cTradingAccount()
            {
                sDataFeed = "";
                iAID = 0;
                sExecutionType = "simulated";
                iLid = 0;
                lLeverage = 0;
                dRisk = 0.0;
                dDailyRisk = 0.0;
                dMinLots = 0.0;
                dMaxLots = 0.0;
                dMaxPosition = 0.0;
                iMaxOpenTrades = 0;
                dMargin = 0.0;
                dMarginLevel = 0.0;
                sAccountType = "simulated";
                sExecutionType = "simulated";
                bActive = false;
                bPending = false;
                bBlock = false;
                bArchived = false;
                sName = "";
                dRequoteInterval = 240000.0;
                dTradeInterval = 1000.0;
                dMinTradeLength = 0.0;
                bPermitHedging = true;
                iOrderHistory = 30;
                sComment = "";
            }
            public int iAID { get; set; }
            public int iLid { get; set; } //This is the internal label for the account purpose. This value corresponds to alveo_account_labels
            public long lLeverage { get; set; } //How much leverage the user is permitted to use
            public double dRisk { get; set; } // No single trade is permitted to lose more than n percent of the account value in a day; each days account value is calculated at midnight and recorded in alveo_balance_day
            public double dDailyRisk { get; set; } //The account net loss for the day cannot be more than n percent of the account value; each days account value is calculated at midnight and recorded in alveo_balance_day
            public double dMinLots { get; set; } // This is the value that restricts the min quantity for any given trade
            public double dMaxLots { get; set; } //This is the value that restricts the max quantity for any given trade
            public double dMaxPosition { get; set; } // This is the percentage of StartingBalance that define total position size allowed. e.g. 0.0002 would be 2 lots on a $10000 account
            public int iMaxOpenTrades { get; set; } //The maximum number of open trades that the user is permitted
            public double dMargin { get; set; }
            public double dMarginLevel { get; set; }
            public string sDataFeed { get; set; } //Available choices are Tupelo, Avocado, Clover; Clover is for the bulk of the simulated accounts; Avocado is for live accounts; Tupelo was intended for direct execution types;
            public string sAccountType { get; set; } // Simulated accounts will never, ever be sent to market; Live accounts may possibly be sent to market - provided the execution type is direct;
            public string sExecutionType { get; set; } //A direct execution type instructs the Ops server to send the order to market; otherwise, the Ops server simulates sending the order to market;
            public bool bActive { get; set; } //Identifies the current, active trading account; Alveo does not support multiple concurrent logins between different accounts; this field is part of the problem. 0=Not Active, 1=Active
            public bool bPending { get; set; } // Some accounts can only be used after a Beeline milestone has been reached. 0=Can be used now, 1=Cannot be used yet
            public bool bBlock { get; set; } //Sometimes we need to prevent a trader from using a particular account; 0=All is Well, 1=Account is suspended and cannot be used for trading at this time;
            public bool bArchived { get; set; } //Some accounts need to be permanently shut down; 0=All is well, 1=This account has been permanently retired
            public string sName { get; set; } //This is the user defined name for the account
            public double dRequoteInterval { get; set; } // The value that is used to evaluate whether or not an order is rejected for requote
            public double dTradeInterval { get; set; } // This is the interval that we allow trades to be placed
            public double dMinTradeLength { get; set; } // This is the minimum amount of time that a trade must be open before it can be closed
            public bool bPermitHedging { get; set; } //Hedging is enabled by default (user-alveo.php and anonymous signups); IB cannot use hedging
            public int iOrderHistory { get; set; } //How many days worth of orders should Alveo pull down when the user logs in
            public string sComment { get; set; } //internal comments

    }
}
