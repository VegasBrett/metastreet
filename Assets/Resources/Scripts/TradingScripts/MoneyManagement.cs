using System;
using Alveo3_Client;
using UnityEngine;

public class MoneyManagement //: MonoBehaviour
{
    
    static public MoneyManagement Instance;
    static private double dMaxPosition = 0.0;
    static private double dOpenPosition = 0.0;
    static private double dOpenProfit = 0.0;
    public static DailyBalanceClass cCurrentAccountOpenBalance = new DailyBalanceClass();
    private static double dCurrentBalance = 0.0;
    private static double dDailyOpenBalance = 0.0;
    private static double UnrealizedPips = 0.0;
    private static double dPipsToday = 0.0;
    private static double dCloseOrdersTodayBalance = 0.0;


    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

    static public double dGetMaxPosition(){
        return dMaxPosition;
    }

    static public void vSetUnrealizedPips(double dPips){
        UnrealizedPips = dPips;
    }

    static public double vGetUnrealizedPips(){
        return UnrealizedPips;
    }
    static public void vSetMaxPosition() {
        dMaxPosition = dCurrentBalance/5000.00;
    }
    static public void vSetOpenLots(double dLots) {
        dOpenPosition = dLots;
    }
    static public void vSetOpenProfit(double dProfit){
        dOpenProfit = dProfit;
    }
    static public double dGetOpenLots() {
        return dOpenPosition;
    }
    static public double dGetPipsToday(){
        return dPipsToday;
    } 
    static public void vComputeTodaysCloseOrders(){
        double dTotalClosedProfit = 0.0;
        dPipsToday = 0.0;
        Debug.Log("Money Management vComputeTodaysCloseOrders Start\n");
        DateTime dtNow = DateTime.UtcNow;
        int iCloseOrders = TradingPlatform.lHistoricOrders.Count;
        for(int iRecord = 0; iRecord < iCloseOrders; iRecord++){
            Order cOrderRecord1 = TradingPlatform.lHistoricOrders[iRecord];
            if((int)cOrderRecord1.Status > 20 && cOrderRecord1.Id > 0){
                try{
                    DateTime dtCloseDay = TradingPlatform.lHistoricOrders[iRecord].CloseDate;
                    if(dtCloseDay.Day == dtNow.Day ){
                        if(dtCloseDay.Month == dtNow.Month ){
                            if(dtCloseDay.Year == dtNow.Year ){
                                Debug.Log("Money Management vComputeTodaysCloseOrders cOrderRecord1.Profit "+cOrderRecord1.Profit + " OID "+cOrderRecord1.Id);
                                dPipsToday += cOrderRecord1.Pips;
                                dTotalClosedProfit += cOrderRecord1.Profit;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    string sErrorDetails = string.Format("Exception cOrderRecord1"+cOrderRecord1);
                    string sErrorType = e.ToString();
                    Debug.Log("MoneyManagemet.vComputeTodaysCloseOrders Error "+sErrorDetails+"\n"+sErrorType);
                    //TradingPlatform.Logger(TradingPlatform.EventLevel.Error, sClassName, sMethodName, sErrorType, sErrorDetails);
                }

            }
        }
        Debug.Log("Money Management vComputeTodaysCloseOrders dTotalClosedProfit "+dTotalClosedProfit);
        vSetCloseOrdersTodayBalance(dTotalClosedProfit);
    }
    static public void vSetCloseOrdersTodayBalance(double dClosedBalance)
    {
        dCloseOrdersTodayBalance = dClosedBalance;
    }
    static public void vSetCurrentBalance(double dDailyBalance){
        dDailyOpenBalance = dDailyBalance;
        dCurrentBalance = dDailyBalance;
    }
    static public double dGetCurrentBalance()
    {
        //vComputeTodaysCloseOrders();
        double dRealTimeBalance = dCurrentBalance + dCloseOrdersTodayBalance + dOpenProfit;
        //Debug.Log("MoneyManagement dCurrentBalance "+dCurrentBalance+ " dCloseOrdersTodayBalance "+dCloseOrdersTodayBalance+" dOpenProfit "+dOpenProfit);
        return dRealTimeBalance;
    }
    static public double dPercentDailyProfit(){
        double dPercentProfit = 0.0;
        double dCurrentBalance = dGetCurrentBalance();
        if(dDailyOpenBalance > 0.0){
            dPercentProfit = ((dCurrentBalance - dDailyOpenBalance)/dDailyOpenBalance) * 100.0;
        }
        return dPercentProfit;
    }
}
