using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace Alveo3_Client
{
    public class Instrument 
    {
            public Instrument()
            {
                Enabled = false;
                Symbol = "";
                ContractSize = 0m;
                Commission = 0.0;
                TickSize = 0m;
            }

            public string Symbol { get; set; }
            public decimal ContractSize { get; set; }

            public double Commission { get; set; }
            public decimal TickSize { get; set; }
            public bool Enabled { get; set; }
            public int Digits
            {
                get { return BitConverter.GetBytes(decimal.GetBits(TickSize)[3])[2]; }
            }
    }
}
