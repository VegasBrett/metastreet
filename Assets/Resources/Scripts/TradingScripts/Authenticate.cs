using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alveo3_Client
{
    public class Authenticate
    {
            public string userName { get; set; }
            public string password { get; set; }
            public string version { get; set; }
    }
}
