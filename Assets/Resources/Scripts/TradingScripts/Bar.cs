using System;
using UnityEngine;

namespace Alveo3_Client
{
    public struct Bar 
    {
        //public string Symbol { get; set; }
        public float Open { get; set; }
        public float Close { get; set; }
        public float High { get; set; }
        public float Low { get; set; }
        //public int iTimeFrame { get; set; }
        public long Date { get; set; }
        public float Volume { get; set; }
        public bool bTail  { get; set; }
    }

        public struct BarWithDT 
    {
        //public string Symbol { get; set; }
        public float Open { get; set; }
        public float Close { get; set; }
        public float High { get; set; }
        public float Low { get; set; }
        //public int iTimeFrame { get; set; }
        public long Date { get; set; }
        public DateTime dtDate { get; set; }
        public float Volume { get; set; }
        public bool bTail  { get; set; }
    }
}
