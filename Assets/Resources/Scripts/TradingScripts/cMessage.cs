using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alveo3_Client
{
    public class cMessage
    {
            public int Type { get; set; }
            public string Args { get; set; }
    }
}
