/*
using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using TMPro;
using UnityEngine;

public class MyNetworkPlayer : NetworkBehaviour
{
    // MyNetworkManager LocalNetworkManager = new MyNetworkManager();
    [SerializeField] private TMP_Text displayNameText = null;
    [SerializeField] private Renderer displayColorRenderer = null;
    
    [SyncVar(hook= "HandleDisplayNameUpdate")]
    [SerializeField]
    private string displayName = "Missing Name";
    private int myPlayerID;

    //[SyncVar(hook = "HandleDisplayColorUpdate")]
    //[SerializeField]
    //private Color MyDisplayColor = new Color(0f,0f,0f);

    #region Server
    [Server]
    public void SetDisplayName( string newDisplayName){
        displayName = newDisplayName;
    }

    [Server]
    public void SetDisplayColor(Color newDisplayColor){
        //MyDisplayColor = newDisplayColor;
    }

    [Command]
    private void CmdSetDisplayName(string newDisplayName)
    {
        bool bValidName = bValidateDisplayName(newDisplayName);
        if(!bValidName) return;
        RpcLogNewName(newDisplayName);
        SetDisplayName(newDisplayName);
    }

    private bool bValidateDisplayName(string sDisplayName)
    {
        bool bValid = true;
        int iNameLength = sDisplayName.Length;
        if(iNameLength < 3 || iNameLength > 30) bValid = false;

        return bValid;
    }
    #endregion

    #region Client
    private void HandleDisplayColorUpdate(Color oldColor, Color newColor){
        //Debug.Log("new color = "+newColor.ToString());
        displayColorRenderer.material.SetColor("_Color", newColor);
    }

    public override void OnStartClient()
    {
        //UnityEngine.Debug.Log("isClientOnly "+isClientOnly+" hasAuthority "+hasAuthority);
        base.OnStartClient();
        if(NetworkServer.active) {return;}
        if(!hasAuthority) {return;}
        //LocalNetworkManager.AddPlayToList(this);
       // myPlayerID = connectionToClient.connectionId;
    }

    public override void OnStopClient()
    {
        //base.OnStopClient();
        if(!isClientOnly) {return;}
        //LocalNetworkManager.RemovePlayToList(this);
         if(!hasAuthority) {return;}

     
    }
    private void HandleDisplayNameUpdate(string oldName, string newName){
        displayNameText.text = newName;
    }

    [ContextMenu("SetMyName")]
    public void SetMyName()
    {
        CmdSetDisplayName("My New Name");
    }

    [ClientRpc]
    private void RpcLogNewName(string newDisplayName)
    {
        UnityEngine.Debug.Log("New Display Name = "+newDisplayName);
    }

    public void ClientExit(){
       UnityEngine.Debug.Log("ClientExit isClientOnly "+isClientOnly+" hasAuthority "+hasAuthority);
         UnityEngine.Debug.Log("Exit button pushed");
        if(!isClientOnly || !hasAuthority) {return;}
        UnityEngine.Debug.Log("Exit is able to exit");
        OnStopClient();
    }
    #endregion
}
*/
