using UnityEngine;
using Photon.Pun;

public class PlayerNameInputManager : MonoBehaviour
{
    public void SetPlayerName(string playerName)
    {
        if(string.IsNullOrEmpty(playerName))
        {
            Debug.Log("PlayerNameInputManager Playername is empty");
            return;
        }
            //Debug.Log("Playername is " + playerName);
            PhotonNetwork.NickName = playerName;

    }

}
