using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonitorCollisionBox : MonoBehaviour
{
    public void vMonitorEntered(){
        //TradingButton.SetActive(true);
        //WatchVideoButton.SetActive(true);
    }
    public void vMonitorExit(){
        //TradingButton.SetActive(false);
        //WatchVideoButton.SetActive(false);
    }

    public void OnTriggerEnter(Collider other) {
        Debug.Log("Monitor collision enter");
        //vMonitorEntered();
    }
    public void OnTriggerExit(Collider other){
        Debug.Log("Monitor collision exit");
        //vMonitorExit();
    }
    private void OnTriggerStay(Collider collider) {
        Debug.Log("Monitor collision stay "+collider.name);
    }
}
