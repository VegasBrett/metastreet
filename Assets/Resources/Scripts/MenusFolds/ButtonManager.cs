using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    [SerializeField]
    public GameObject TradingButton;
    [SerializeField]
    public GameObject WatchVideoButton;
    [SerializeField]
    public GameObject TradingFloorButton;

    public void vOpenTradingPlatformScene(){
        SceneManager.LoadScene("TradingPanel");
        TradingButton.SetActive(false);
        WatchVideoButton.SetActive(true);
        TradingFloorButton.SetActive(true);
    }

    public void vOpenVideoScene(){
        SceneManager.LoadScene("WatchVideos");
        TradingButton.SetActive(true);
        WatchVideoButton.SetActive(false);
        TradingFloorButton.SetActive(true);
    }

    public void vOpenTradingFloorScene(){
        SceneManager.LoadScene("TradingFloor");
        TradingButton.SetActive(true);
        WatchVideoButton.SetActive(true);
        TradingFloorButton.SetActive(false);
    }

    public void vExit(){
        Debug.Log("Exit");
        Application.Quit();
    }

    public void vMonitorEntered(){
        TradingButton.SetActive(true);
        WatchVideoButton.SetActive(true);
    }
    public void vMonitorExit(){
        TradingButton.SetActive(false);
        WatchVideoButton.SetActive(false);
    }
}
