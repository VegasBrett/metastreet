﻿/*
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.SceneManagement;

public class LobbyMenu : MonoBehaviour
{

    [SerializeField] private GameObject lobbyUI = null;

    private void Start(){
        MyNetworkManager.ClientOnConnected += HandleClientConnected;

    }
    private void OnDestroy()
    {
        MyNetworkManager.ClientOnConnected -= HandleClientConnected;

    }

    private void HandleClientConnected(){
        lobbyUI.SetActive(true);
    }

    public void LeaveLobby()
    {
        //Check if host
        if(NetworkServer.active && NetworkClient.isConnected)
        {
            NetworkManager.singleton.StopHost();

        }
        else{
            NetworkManager.singleton.StopClient();
            SceneManager.LoadScene(0);
        }
    }
}
*/
