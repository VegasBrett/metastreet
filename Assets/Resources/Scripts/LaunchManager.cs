using System;
using System.Collections;
//using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;

public class LaunchManager : MonoBehaviourPunCallbacks
{
public GameObject EnterGamePanel;
public GameObject ConnectionStatusPanel;
public GameObject LobbyPanel;


    #region Unity Methods
    // Start is called before the first frame update
    void Start()
    {
        EnterGamePanel.SetActive(true);
        ConnectionStatusPanel.SetActive(false);
        LobbyPanel.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void  Awake() {
        {
            PhotonNetwork.AutomaticallySyncScene = true;
        }
    }

    #endregion

#region PublicMethods

    public  void ConnectToPhotonServer()
    {
        if(!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.ConnectUsingSettings();
            ConnectionStatusPanel.SetActive(true);
            EnterGamePanel.SetActive(false);
        }
         
    }

    public void joinRandomRoom()
    {
        PhotonNetwork.JoinRandomRoom();
    }
#endregion

#region PrivateMethods

    private void CreateAndJoinRoom()
    {
//        string randomRoomName = "Room" + UnityEngine.Random.Range(0,10);
        string randomRoomName = "AkreweRoom";
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.IsOpen = true;
        roomOptions.IsVisible = true;
        roomOptions.MaxPlayers = 20;
        PhotonNetwork.CreateRoom(randomRoomName,roomOptions);
    }
#endregion

#region Photon Callbacks
    public override void OnConnectedToMaster()
    {
        //Debug.Log(PhotonNetwork.NickName+ " Connected to Photon server");
        LobbyPanel.SetActive(true);
        ConnectionStatusPanel.SetActive(false);
    }

    public override void OnConnected()
    {
        //base.OnConnected();
        //Debug.Log("Connected to internet");
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        base.OnJoinRandomFailed(returnCode, message);
        //Debug.Log(message);
        CreateAndJoinRoom();
    }

    public override void OnJoinedRoom()
    {
        //base.OnJoinedRoom();
        //Debug.Log(PhotonNetwork.NickName + " joined to " + PhotonNetwork.CurrentRoom.Name);
        PhotonNetwork.LoadLevel(1);
        //PhotonNetwork.CurrentRoom.IsOpen = false;
        //PhotonNetwork.CurrentRoom.IsVisible = false;
        //PhotonNetwork.LoadLevel(2);    
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        //base.OnPlayerEnteredRoom(newPlayer);
        //Debug.Log(newPlayer.NickName + " joined room "+ PhotonNetwork.CurrentRoom.Name +" players in room "+ PhotonNetwork.CurrentRoom.PlayerCount);
    }
    #endregion
}
