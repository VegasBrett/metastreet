using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using UnityEngine;

public class Doorcontroller: MonoBehaviour
{
    public Animator _doorAnim;

    private void OnTriggerEnter(Collider other){
        Debug.Log("OnTriggerEnter");
        _doorAnim.SetBool("opening",true);
    }

    private void OnTriggerExit(Collider other){
        Debug.Log("OnTriggerExit");
        _doorAnim.SetBool("opening",false);
    }
}

